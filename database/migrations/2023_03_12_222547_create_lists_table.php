<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('name_ro')->nullable();
            $table->string('name_en')->nullable();
            $table->text('description')->nullable();
            $table->text('description_ro')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_short')->nullable();
            $table->text('description_short_ro')->nullable();
            $table->text('description_short_en')->nullable();
            $table->integer('parent_id')->default(0)->index();
            $table->boolean('enabled')->default(true);
            $table->integer('views')->default(0);
            $table->integer('sort')->default(0);
            $table->string('slug', 100)->unique();
            $table->text('params')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lists');
    }
};
