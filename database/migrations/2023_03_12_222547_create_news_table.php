<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('name_ro');
            $table->string('name_en');
            $table->text('description')->nullable();
            $table->text('description_ro')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_short')->nullable();
            $table->text('description_short_ro')->nullable();
            $table->text('description_short_en')->nullable();
            $table->boolean('enabled')->default(true);
            $table->boolean('top')->default(false);
            $table->integer('views')->default(0);
            $table->integer('sort')->default(0);
            $table->string('slug')->nullable()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
};
