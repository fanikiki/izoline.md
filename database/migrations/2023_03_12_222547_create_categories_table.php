<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('name_ro', 255);
            $table->string('name_en', 255);
            $table->text('description');
            $table->text('description_ro');
            $table->text('description_en');
            $table->text('description_short');
            $table->text('description_short_ro');
            $table->text('description_short_en');
            $table->boolean('enabled')->default(true);
            $table->boolean('show_menu');
            $table->integer('views')->default(0);
            $table->integer('sort')->default(0);
            $table->string('slug', 255)->nullable()->default('NULL')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
