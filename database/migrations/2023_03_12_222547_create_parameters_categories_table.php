<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parameter_id')->index('parameters_categories_parameter_id_foreign');
            $table->unsignedInteger('category_id')->index('parameters_categories_category_id_foreign');
            $table->integer('sort');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameters_categories');
    }
};
