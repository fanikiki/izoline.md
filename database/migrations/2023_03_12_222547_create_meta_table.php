<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meta_description', 250);
            $table->string('meta_description_ro', 250);
            $table->string('meta_description_en', 250);
            $table->string('meta_keywords', 250);
            $table->string('meta_keywords_ro', 250);
            $table->string('meta_keywords_en', 250);
            $table->string('title', 250);
            $table->string('title_ro', 250);
            $table->string('title_en', 250);
            $table->integer('table_id')->index();
            $table->string('table_type', 64)->index('meta_table_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta');
    }
};
