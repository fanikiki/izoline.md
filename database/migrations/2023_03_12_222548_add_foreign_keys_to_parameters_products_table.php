<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parameters_products', function (Blueprint $table) {
            $table->foreign(['parameter_id'])->references(['id'])->on('parameters')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['product_id'])->references(['id'])->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameters_products', function (Blueprint $table) {
            $table->dropForeign('parameters_products_parameter_id_foreign');
            $table->dropForeign('parameters_products_product_id_foreign');
        });
    }
};
