<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use App\Traits\CommonTrait;
use App\Traits\MetaTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Manufacturers extends Model
{
    use CommonTrait, MetaTrait;

    protected $dateFormat = 'Y-m-d H:i:s';
    protected $guarded = ['id'];
    protected $with = ['photos'];

    public function mainphoto($index = 0)
    {
        $fileName = 'no_photo.png';

        if (isset($this->photos[$index])) {

            if(File::exists(public_path('uploaded/' . $this->photos[$index]->source))) {
                $fileName = $this->photos[$index]->source;
            }

            if(Controller::hasWebp()){
                if(File::exists(public_path('uploaded/' . File::name($fileName) . '.webp'))){
                    $fileName = File::name($fileName) . '.webp';
                }
            }

            return config('photos.images_url') . $fileName;
        }

        return $fileName;
    }

    public function scopeWithPhotos($query)
    {
        return $query->enabled()->has('photos')->with('photos')->orderBy('name');
    }

    public function getNameAttribute()
    {
        return $this->attributes['name'];
    }
}
