<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParametersProducts extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function parameter()
    {
        return $this->hasOne('App\Models\Parameters', 'id', 'parameters_id');
    }
}
