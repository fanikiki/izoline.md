<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use App\Traits\CommonTrait;
use App\Traits\MetaTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class Product extends Model
{
    use CommonTrait, MetaTrait;

    protected $with = ['photos', 'category', 'brand'];

    protected $casts = [
        'params' => 'collection',
        'faq' => 'collection',
        'faq_ro' => 'collection'
    ];

    protected $guarded = ['id'];

    const AVAILABILITY_NOT = 0;
    const AVAILABILITY_LOW = 1;
    const AVAILABILITY_MEDIUM = 2;
    const AVAILABILITY_FULL = 3;

    const TYPE_LIST = 1;
    const TYPE_ROLL = 2;
    const TYPE_PIECE = 3;
    const TYPE_LIST_XPS = 4;

    public function scopeEnabled($query)
    {
        return $query->where('enabled', 1);
    }

    public function category()
    {
        return $this->belongsToMany('App\Models\Categories', 'products_categories');
    }

    public function recommended()
    {
        return $this->belongsToMany('App\Models\Product', 'products_recommended', 'product_id', 'recommended_id');
    }

    public function brand()
    {
        return $this->hasOne('App\Models\Manufacturers', 'id', 'manufacturer_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function mainphoto($index = 0)
    {
        $fileName = 'no_photo.png';

        if (isset($this->photos[$index])) {

            if(File::exists(public_path('uploaded/' . $this->photos[$index]->source))) {
                $fileName = $this->photos[$index]->source;
            }

            if(Controller::hasWebp()){
                if(File::exists(public_path('uploaded/' . File::name($fileName) . '.webp'))){
                    $fileName = File::name($fileName) . '.webp';
                }
            }

            return config('photos.images_url') . $fileName;
        }

        return $fileName;
    }

    public function mainphotothumb($index = 0)
    {
        $fileName = 'no_photo.png';

        if (isset($this->photos[$index])) {

            if(File::exists(public_path('uploaded/thumbs/' . $this->photos[$index]->source))) {
                $fileName = $this->photos[$index]->source;
            }

            if(Controller::hasWebp()){
                if(File::exists(public_path('uploaded/thumbs/' . File::name($fileName) . '.webp'))){
                    $fileName = File::name($fileName) . '.webp';
                }
            }

            return config('photos.thumbs_url') . $fileName;
        }

        return $fileName;
    }

    public function list_thickness()
    {
        return $this->belongsToMany('App\Models\Product', 'products_list_thickness', 'product_id')
            ->withPivot(['thickness', 'surface', 'weight', 'num_in_box', 'price', 'price_cubic']);
    }

    public function getPlaceholderTextAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['placeholder_text_' . $locale])) {
            return $this->attributes['placeholder_text_' . $locale];
        }

        return $this->attributes['placeholder_text'];
    }

    public function setPlaceholderTextAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['placeholder_text'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if (!in_array($key, array_keys(config('app.locales')))) continue;
                if ($key == config('app.base_locale')) {
                    $this->attributes['placeholder_text'] = $value;
                    continue;
                }
                $this->attributes['placeholder_text_' . $key] = $value;
            }
        }
    }

    public function setParamsTextAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['params_text'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if (!in_array($key, array_keys(config('app.locales')))) continue;
                if ($key == config('app.base_locale')) {
                    $this->attributes['params_text'] = $value;
                    continue;
                }
                $this->attributes['params_text_' . $key] = $value;
            }
        }
    }

    public function getParamsTextAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['params_text_' . $locale])) {
            return $this->attributes['params_text_' . $locale];
        }
        return $this->attributes['params_text'];
    }

    public function getFaqAttribute($value)
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['faq_' . $locale])) {
            return $this->castAttribute('faq_' . $locale, $this->attributes['faq_' . $locale]);
        }

        return $this->castAttribute('faq', $value);
    }

    public function getCurrencyPrice($thickness = null)
    {
        switch ($this->product_type) {
            case self::TYPE_LIST:
                if($this->list_thickness->isNotEmpty()){
                    return $this->getPriceThickness($thickness);
                }
                return $this->getPrice('list');
            case self::TYPE_ROLL:
                return $this->getPrice('roll');
            case self::TYPE_PIECE:
                return $this->getPrice('piece');
            case self::TYPE_LIST_XPS:
                if($this->list_thickness->isNotEmpty()){
                    return $this->getPriceThickness($thickness);
                }
                break;
            default:
                break;
        }

        return "";
    }

    public function getCurrencyPriceValute()
    {
        return ' ' . trans('common.lei') . ' / ' . trans('common.measure_type_' . $this->measure_type);
    }

    public function getCurrencyTypePrice()
    {
        switch ($this->product_type) {
            case self::TYPE_LIST:
                return $this->getPrice('cubic') . ' ' . trans('common.lei') . ' / ' . trans('common.price_cubic');
            case self::TYPE_ROLL:
                return $this->getPrice('square') . ' ' . trans('common.lei') . ' / ' . trans('common.price_square');
            case self::TYPE_PIECE:
                return $this->getPrice('piece') . ' ' . trans('common.lei') . ' / ' . trans('common.price_piece');
            case self::TYPE_LIST_XPS:
                return ($this->getThicknessList()[0]['price_cubic'] ?? 0) . ' ' . trans('common.lei') . ' / ' . trans('common.price_cubic');
            default:
                break;
        }

        return "";
    }

    public function isRollProduct()
    {
        return $this->product_type == 2;
    }

    public function isPieceProduct()
    {
        return $this->product_type == 3;
    }

    public function isListProduct()
    {
        return $this->product_type == 1;
    }

    public function isListXpsProduct()
    {
        return $this->product_type == 4;
    }

    public function isRoundByBox()
    {
        return $this->round_by == 4;
    }

    public function isRoundByList()
    {
        return $this->round_by == 2;
    }

    public function getSelectedSurface()
    {
        return $this->list_thickness[0]->pivot['surface'];
    }

    public function getSelectedPrice()
    {
        return $this->list_thickness[0]->pivot['price'];
    }

    public function getRealPrice()
    {
        try {
            if ($this->isRoundByList() || $this->isRoundByBox()) {
                $surface = $this->surface;
                $price_cubic = $this->price_cubic;

                if ($this->isListProduct() || $this->isListXpsProduct()) {
                    $surface = $this->getSelectedSurface();
                }

                $result = round($price_cubic * $surface, 2);

                if ($this->isListXpsProduct()) {
                    $result = round($this->getSelectedPrice(), 2);
                }
            } elseif ($this->isRollProduct()) {
                $result = $this->getPrice('roll');
            } elseif ($this->isPieceProduct()) {
                $result = $this->getCurrencyPrice();
            }
            return $result;
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getPrice($type = "piece")
    {
        return $this->attributes["price_$type"];
    }

    public function getPriceWithCurrency()
    {
        return $this->getPrice() . ' ' . (session()->get('currency')['label_short'] ?? 'MDL');
    }

    public function getSalePrice()
    {
        return 0;
    }

    public function getSalePriceWithCurrency()
    {
        return $this->getSalePrice() . ' ' . (session()->get('currency')['label_short'] ?? 'MDL');
    }

    public function getDiscountPrice()
    {
        $price = $this->getPrice();
        $sale_price = $this->getSalePrice();

        return round(100 - ($sale_price / $price) * 100);
    }

    public function parameters()
    {
        return $this->belongsToMany('App\Models\Parameters', 'parameters_products', 'product_id', 'parameter_id')
            ->orderBy('sort')
            ->withPivot('value_id', 'value_en');
    }

    public function params()
    {
        $out = [];
        $parameters = $this->parameters->sortBy('sort');

        if ($parameters->count()) {
            foreach ($parameters as $parameter) {
                $value_id = $parameter->pivot->value_id;
                $values = $parameter->values->pluck('value', 'id');

                if (!isset($values[$value_id])) continue;

                $out[$parameter->name][] = $values[$value_id];
            }
        }

        return collect($out);
    }

    public function getAvailabilityText()
    {
        return trans('common.product_availability_' . $this->availability);
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'product_id', 'product_code')->orderBy('filename');
    }


    public function getSmallPhoto()
    {
        $fileName = config('photos1c.no_photo_url');
        if (isset($this->images{0})) {
            $fileName = $this->images{0}->getSmallPhoto();
        }
        return $fileName;
    }

    public function getBigPhoto()
    {
        $fileName = config('photos1c.no_photo_url');
        if (isset($this->images{0})) {
            $fileName = $this->images{0}->getBigPhoto();
        }
        return $fileName;
    }

    public function scopeAvailable($query)
    {
        return $query->whereIn('availability', [self::AVAILABILITY_LOW, self::AVAILABILITY_MEDIUM, self::AVAILABILITY_FULL]);
    }

    public function getDiscountText()
    {
        return '-' . round($this->attributes['sale']) . '%';
    }

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '') {
            $query->where(function ($query) use ($keyword) {
                $query->where('name', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('name_ro', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('description', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('description_ro', 'LIKE', '%' . $keyword . '%');

                $product_ids = Meta::where('table_type', '\App\Models\Product')
                    ->whereRaw('title LIKE ? OR title_ro LIKE ?', ['%' . $keyword . '%', '%' . $keyword . '%'])
                    ->pluck('table_id')
                    ->toArray();

                if(!empty($product_ids)){
                    $query->orWhereIn('id', $product_ids);
                }
            });
        }
        return $query;
    }

    public function hasSale()
    {
        if ($this->getSalePrice() == 0) {
            return false;
        }

        if ($this->getPrice() == $this->getSalePrice()) {
            return false;
        }

        return true;
    }

    public function getRoute($absolute = true)
    {
        return route('catalog.get-product', $this->slug, $absolute);
    }

    public function getRelated()
    {
        $category = $this->category[0];

        return Product::query()
            ->where('products.enabled', true)
            ->join('products_categories', 'products.id', '=', 'products_categories.product_id')
            ->join('categories', 'products_categories.categories_id', '=', 'categories.id')
            ->where('categories.slug', $category->slug)
            ->where('products.id', '!=', $this->id)
            ->select('products.*')
            ->limit(10)
            ->get();

    }

    public function getProductInfo()
    {
        $data = [
            'id' => $this->id,
            'price_currency' => $this->getPriceWithCurrency(),
        ];

        return $data;
    }

    public function getWeight()
    {
        $weight = $this->attributes['weight'];

        if(in_array($this->product_type, [self::TYPE_LIST, self::TYPE_LIST_XPS])){
            try {
                $weight = $this->getThicknessList()[0]['weight'];
            } catch (\Exception $e) {
                $weight = 0;
            }
        }

        return $weight;
    }

    public function getThicknessList()
    {
        $out = [];

        $list_thickness = $this->list_thickness;

        try {
            if ($list_thickness->isNotEmpty()) {
                foreach ($list_thickness as $list) {
                    $out[] = [
                        'value' => $list->pivot['thickness'],
                        'surface' => $list->pivot['surface'],
                        'num_in_box' => $list->pivot['num_in_box'],
                        'price_cubic' => $list->pivot['price_cubic'],
                        'price' => $list->pivot['price'],
                        'weight' => $list->pivot['weight']
                    ];
                }

                return $out;
            }

            if (Str::contains($this->attributes['thickness'], '|')) {
                $thickness = explode("|", $this->attributes['thickness']);
                foreach ($thickness as $value_list) {
                    $value_list = explode(",", $value_list);
                    $out[] = [
                        'value' => $value_list[1],
                        'surface' => $this->surface,
                        'num_in_box' => $value_list[0],
                        'price_cubic' => 0,
                        'price' => 0
                    ];
                }
            } else {
                $thickness = explode(",", $this->attributes['thickness']);
                foreach ($thickness as $value) {
                    $out[] = [
                        'value' => $value,
                        'surface' => $this->surface,
                        'num_in_box' => null,
                        'price_cubic' => 0,
                        'price' => 0
                    ];
                }
            }
        } catch (\Exception $e) {
        }

        return $out;
    }

    public function setAreaAttribute()
    {
        $this->attributes['area'] = number_format(
            ($this->attributes['width'] / 1000) *
            ($this->attributes['length'] / 1000), 3, '.', '');
    }

    public function setSurfaceAttribute()
    {
        try {
            $thickness = explode(",", $this->attributes['thickness'])[0];
            if (Str::contains($this->attributes['thickness'], '|')) {
                $thickness = explode(",", explode("|", $this->attributes['thickness'])[0])[1];
            }
        } catch (\Exception $e) {
            $thickness = 0;
        }

        $surface = number_format(
            ($this->attributes['width'] / 1000) *
            ($this->attributes['length'] / 1000) *
            ($thickness / 1000), 4, '.', '');

        $this->attributes['surface'] = $surface;
    }

    public function getSurface()
    {
        return $this->attributes['surface'] ?? 0;
    }

    public function setPriceRollAttribute()
    {
        $this->attributes['price_roll'] = number_format($this->attributes['price_square'] * $this->attributes['area'], 2, '.', '');
    }

    public function setPriceListAttribute()
    {
        $this->attributes['price_list'] = number_format($this->attributes['price_cubic'] * $this->attributes['surface'], 4, '.', '');
    }

    public function getParentCategoryId()
    {
        $parent_category_id = null;

        if (isset($this->category[0])) {
            $parent_category_id = $this->category[0]->id;
        }

        if (isset($this->category[0]) && isset($this->category[0]->parents[0])) {
            $parent_category_id = $this->category[0]->parents[0]->id;
        }

        return $parent_category_id;
    }

    public function getParamNumInBox()
    {
        $param_num_in_box = $this->parameters->firstWhere('id', 25);

        if($param_num_in_box) {
            $value_id = $param_num_in_box->pivot->value_id;
            $values = $param_num_in_box->values->pluck('value', 'id');

            return $values[$value_id] ?? '';
        }

        return '';
    }

    public function getNumInBoxThickness($selected_thickness)
    {
        $list_thickness = $this->list_thickness;

        if($list_thickness->isNotEmpty()){
            foreach ($list_thickness as $list){
                if($list->pivot['thickness'] == $selected_thickness){
                    return $list->pivot['num_in_box'];
                }
            }
        }

        return '';
    }

    public function getNumInBoxSurface($selected_thickness)
    {
        $list_thickness = $this->list_thickness;

        if($list_thickness->isNotEmpty()){
            foreach ($list_thickness as $list){
                if($list->pivot['thickness'] == $selected_thickness){
                    return $list->pivot['surface'];
                }
            }
        }

        return '';
    }

    public function getPriceThickness($selected_thickness)
    {
        $list_thickness = $this->list_thickness;

        if($list_thickness->isNotEmpty()){
            foreach ($list_thickness as $list){
                if($list->pivot['thickness'] == $selected_thickness){
                    return $list->pivot['price'];
                }
            }
        }else{
            if($this->product_type == Product::TYPE_PIECE){
                return $this->getPrice();
            }elseif ($this->product_type == Product::TYPE_ROLL){
                return $this->getPrice('roll');
            }
        }

        return 0;
    }

    public function getWeightThickness($selected_thickness)
    {
        $list_thickness = $this->list_thickness;

        if($list_thickness->isNotEmpty()){
            foreach ($list_thickness as $list){
                if($list->pivot['thickness'] == $selected_thickness){
                    return $list->pivot['weight'];
                }
            }
        }else{
            return $this->weight;
        }

        return 0;
    }

    public static function destroy($ids)
    {
        $product = self::find($ids);

        $photos = $product->photos;

        foreach ($photos as $photo) {
            Photos::where('id', $photo->id)->delete();
            File::delete(public_path('uploaded/' . $photo->source));
        }

        return parent::destroy($ids);
    }
}
