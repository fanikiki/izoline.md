<?php

namespace App\Models;


class Models extends BaseModel
{
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $fillable = ['slug'];

    public function manufacturer()
    {
        return $this->hasOne('App\Models\Manufacturers', 'id', 'manufacturer_id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Categories', 'id', 'category_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'model_id');
    }

    public function getFullName()
    {
        return $this->manufacturer->name
        . ' ' . $this->name;
    }

    public function getNameAttribute()
    {
        return $this->attributes['name'];
    }
}
