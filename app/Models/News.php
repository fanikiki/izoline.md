<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class News extends BaseModel
{
    protected $guarded = ['id'];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $with = ['cover', 'preview', 'category'];

    public function category()
    {
        return $this->belongsToMany('App\Models\CategoriesNews', 'news_categories', 'news_id', 'categories_id');
    }

    public function getCategories()
    {
        $categories = $this->category;
        return $categories;
    }

    public function preview()
    {
        return $this->hasOne('App\Models\Photos', 'table_id', 'id')->where('table', 'news_preview');
    }

    public function cover()
    {
        return $this->hasOne('App\Models\Photos', 'table_id', 'id')->where('table', 'news_cover');
    }

    public function previewphoto()
    {
        $fileName = "nophoto.png";

        if (isset($this->preview)) {
            if(File::exists(public_path('uploaded/news/' . $this->preview->source))) {
                $filemtime = filemtime(public_path('uploaded/news/' . $this->preview->source));
                $fileName = '/news/' . $this->preview->source . "?v=$filemtime";
            }
        }

        return $fileName;
    }

    public function coverphoto()
    {
        $fileName = "nophoto.png";

        if (isset($this->cover)) {
            if(File::exists(public_path('uploaded/news/' . $this->cover->source))) {
                $filemtime = filemtime(public_path('uploaded/news/' . $this->cover->source));
                $fileName = '/news/' . $this->cover->source . "?v=$filemtime";
            }
        }

        return $fileName;
    }

    public function getCreatedDate()
    {
        return Carbon::parse($this->created_at)->format('d.m.Y');
    }

    public function hasPreview()
    {
        return $this->preview;
    }

    public function hasCover()
    {
        return $this->cover;
    }

    public function getNewsInfo()
    {
        $name_slug_preview = Str::slug($this->getRawOriginal('name')) . '-preview';
        $name_slug_cover = Str::slug($this->getRawOriginal('name')) . '-cover';

        try {
            $file_time_preview = filemtime(public_path("uploaded/news/{$name_slug_preview}_cookies.md.jpg"));
            $file_time_cover = filemtime(public_path("uploaded/news/{$name_slug_cover}_cookies.md.jpg"));
        } catch (\Exception $e) {
            $file_time_preview = 1;
            $file_time_cover = 1;
        }

        $category = $this->category[0];

        try {
            $data = [
                'name' => $this->name,
                'name_slug_preview' => $name_slug_preview,
                'name_slug_cover' => $name_slug_cover,
                'description_short' => $this->description_short,
                'description' => $this->description,
                'categoryName' => $category->name,
                'categorySlug' => $category->slug,
                'categoryClass' => 'text-red-2',
                'created_date' => $this->getCreatedDate(),
                'categories' => $this->getCategories(),
                'hasCover' => $this->hasCover(),
                'hasPreview' => $this->hasPreview(),
                'url' => route('get-post', [$this->category[0]->slug, $this->slug]),
                'file_time_preview' => $file_time_preview,
                'file_time_cover' => $file_time_cover
            ];
        } catch (\Exception $e) {
            return $this;
        }

        return $data;
    }

    public function scopeEnabled($query)
    {
        return parent::scopeEnabled($query)
            ->where('name', '!=', '');
    }
}
