<?php

namespace App\Models;

class ListThickness extends BaseModel
{
    protected $guarded = [];

    protected $primaryKey = null;
    public $incrementing = false;

    protected $table = 'products_list_thickness';

    public static function calcPrice($price_cubic, $surface)
    {
        return number_format($price_cubic * $surface, 4, '.', '');
    }
}
