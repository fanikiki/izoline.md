<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class BaseModel extends Model
{
    protected $dateFormat = 'd.m.Y';

    public function getCreatedAtAttribute($date)
    {
        return Date::parse($date)->format($this->dateFormat);
    }

    public function setCreatedAtAttribute($date)
    {
        $this->attributes['created_at'] = Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    /**
     * Create the slug from the title
     */
    public function setSlugAttribute($value)
    {
        if ($value == "") {
            // grab the title and slugify it
            $this->attributes['slug'] = Str::slug($this->name);
        }else{
            $this->attributes['slug'] = Str::slug($value);
        }
    }

    public function scopeGetBySlug($query, $slug)
    {
        return $query->where('slug', $slug)
            ->orWhere('slug_ro', $slug)
            ->orWhere('slug_en', $slug);
    }

    public function setMetaDescriptionAttribute($values)
    {
        $this->saveMeta($values, "meta_description");
    }

    public function setMetaKeywordsAttribute($values)
    {
        $this->saveMeta($values, "meta_keywords");
    }

    public function setTitleAttribute($values)
    {
        $this->saveMeta($values, "title");
    }



    public function setNameAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['name'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['name'] = $value;
                    continue;
                }
                $this->attributes['name_' . $key] = $value ?? '';
            }
        }
    }

    public function setDescriptionAttribute($values)
    {
        if(!empty($values)) {
            if (!is_array($values)) {
                $this->attributes['description'] = $values;
            } else {
                foreach ($values as $key => $value) {
                    if ($key == "ru") {
                        $this->attributes['description'] = $value;
                        continue;
                    }
                    $this->attributes['description_' . $key] = $value ?? '';
                }
            }
        }
    }

    public function setDescriptionShortAttribute($values) {
        if(!$this->containsOnlyNull($values)) {
            if (!is_array($values)) {
                $this->attributes['description'] = $values;
            } else {
                foreach ($values as $key => $value) {
                    if ($key == "ru") {
                        $this->attributes['description_short'] = $value;
                        continue;
                    }
                    $this->attributes['description_short_' . $key] = $value ?? '';
                }
            }
        }
    }

    public function setUpdatedAtAttribute($value)
    {
        $this->attributes['updated_at'] = Carbon::parse($value)->format("Y-m-d H:i:s");
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format($this->dateFormat);
    }

    public function getDescriptionAttribute()
    {
        $locale = Lang::locale();
        $description_lang = $this->attributes['description'];

        if(isset($this->attributes['description_' . $locale])){
           $description_lang = $this->attributes['description_' . $locale];
        }

        if ($locale == "ru"){
            return $this->attributes['description'];
        }else{
            return $description_lang;
        }
    }

    public function getDescriptionShortAttribute()
    {
        $locale = Lang::locale();
        $description_short_lang = $this->attributes['description_short'];


        if(isset($this->attributes['description_short_' . $locale])){
            $description_short_lang = $this->attributes['description_short_' . $locale];
        }

        if ($locale == "ru"){
            return $this->attributes['description_short'];
        }else{
            return $description_short_lang;
        }
    }

    public function getNameAttribute()
    {
        $locale = Lang::locale();
        $name_lang = $this->attributes['name'];

        if(isset($this->attributes['name_' . $locale])){
            $name_lang = $this->attributes['name_' . $locale];
        }

        if ($locale == "ru"){
            return $this->attributes['name'];
        }else{
            return $name_lang;
        }
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photos','table_id')->where('table', $this->getTable())->orderBy('sort');
    }

    public function meta()
    {
        return $this->hasOne('App\Models\Meta','table_id')->where('table', $this->getTable());
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Reviews', 'table_id')->where('table', $this->getTable())->orderBy('created_at', 'desc');
    }

    public function getAverageRating($reviews)
    {
        $sum = 0;
        $num = 0;
        foreach ($reviews as $review) {
            $num++;
            $sum += $review->rating;
        }
        if ($num == 0) return 0;
        return ceil($sum / $num);
    }

    public function scopeEnabled($query)
    {
        return $query->where('enabled', 1);
    }

    public function scopeDisabled($query)
    {
        return $query->where('enabled', 0);
    }

    public function scopeTop($query)
    {
        return $query->where('top', 1);
    }

    public function scopeMainpage($query)
    {
        return $query->where('show_mainpage', true);
    }

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '') {
            $keyword = Str::lower($keyword);
            $keyword_no_last_letter = Str::lower(Str::substr($keyword, 0, -1));

            $query->where(function ($query) use ($keyword_no_last_letter, $keyword) {
                $query->whereRaw("LOWER(name) LIKE ?", "%$keyword%")
                    ->orWhereRaw("LOWER(name_ro) LIKE ?", "%$keyword%")
                    ->orWhereRaw("LOWER(name_en) LIKE ?", "%$keyword%")
                    ->orWhereRaw("LOWER(description) LIKE ?", "%$keyword%")
                    ->orWhereRaw("LOWER(description_ro) LIKE ?", "%$keyword%")
                    ->orWhereRaw("LOWER(description_en) LIKE ?", "%$keyword%")
                    ->orWhereRaw("LOWER(article) LIKE ?", "%$keyword%");

                if(!ctype_digit($keyword[Str::length($keyword) - 1])){
                    $query->orWhereRaw("LOWER(name) LIKE ?", "%$keyword_no_last_letter%")
                        ->orWhereRaw("LOWER(name_ro) LIKE ?", "%$keyword_no_last_letter%")
                        ->orWhereRaw("LOWER(name_en) LIKE ?", "%$keyword_no_last_letter%")
                        ->orWhereRaw("LOWER(description) LIKE ?", "%$keyword_no_last_letter%")
                        ->orWhereRaw("LOWER(description_ro) LIKE ?", "%$keyword_no_last_letter%")
                        ->orWhereRaw("LOWER(description_en) LIKE ?", "%$keyword_no_last_letter%")
                        ->orWhereRaw("LOWER(article) LIKE ?", "%$keyword_no_last_letter%");
                }
            });
        }
        return $query;
    }

    private function saveMeta($values, $type)
    {
        if(!$this->containsOnlyNull($values)) {
            $table = $this->getTable();
            //save model before saving meta
            if (!isset($this->id)) $this->save();
            $table_id = $this->id;

            $meta = Meta::where('table', $table)->where('table_id', $table_id)->first();
            if (!isset($meta)) {
                $meta = new Meta();
            }
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $meta->attributes[$type] = $value;
                    continue;
                }
                $meta->attributes[$type . '_' . $key] = $value ?? '';
            }
            $meta->table = $table;
            $meta->table_id = $table_id;
            $meta->save();
        }
    }

    public function mainphoto($index = 0)
    {
        $fileName = "nophoto.png";

        if (isset($this->photos{$index})) {
            if(File::exists(public_path('uploaded/' . $this->photos{$index}->source))) {
                $filemtime = filemtime(public_path('uploaded/' . $this->photos{$index}->source));
                $fileName = $this->photos{$index}->source . "?v=$filemtime";
            }else{
                return $fileName;
            }
        }

        return $fileName;

    }

    public function hasPhotos()
    {
        $file_exists = false;

        try {
            $file_exists = File::exists(public_path("uploaded/{$this->photos[0]->source}"));
        } catch (\Exception $e) {}

        return count($this->photos) > 0 && $file_exists;
    }

    public function getTitle()
    {
        if (isset($this->meta->title) && ($this->meta->title != '') ) {
            return $this->meta->title;
        }

        return trans('common.meta_title_custom', ['name' => $this->name]);
    }

    public function getMetaKeywords()
    {
        if (isset($this->meta->meta_keywords) && ($this->meta->meta_keywords != '')) {
            return $this->meta->meta_keywords;
        }

        return trans('common.meta_keywords_custom', ['name' => $this->name]);
    }

    public function getMetaDescription()
    {
        if (isset($this->meta->meta_description) && ($this->meta->meta_description != '')) {
            return $this->meta->meta_description;
        }

        return trans('common.meta_description_custom', ['name' => $this->name]);
    }

    public function scopeSort($query)
    {
        return $query->orderBy('sort');
    }

    public function hasMeta()
    {
        return !empty($this->getTitle()) || !empty($this->getMetaKeywords()) || !empty($this->getMetaDescription());
    }

    private function containsOnlyNull($input)
    {
        if($input===null) return true;
        return empty(array_filter($input, function ($a) { return $a !== null;}));
    }
}
