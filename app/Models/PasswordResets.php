<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class PasswordResets extends BaseModel
{
    protected $table = "password_resets";
    protected $primaryKey = 'email';
    protected $guarded = [];
    public $timestamps = false;

    public static function getEmail($token)
    {
        $data = PasswordResets::all();

        if($data->isNotEmpty()) {
            foreach ($data as $d) {
                if (Hash::check($token, $d->token)) {
                    return $d->getRawOriginal('email');
                }
            }
        }

        return false;
    }
}
