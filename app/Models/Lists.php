<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use App\Traits\CommonTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;

class Lists extends Model
{
    use CommonTrait;

    public $timestamps = false;
    protected $guarded = ['id'];
    protected $with = ['photos'];
    protected $casts = ['params' => 'collection'];

    const ID_SLIDER = 1;
    const ID_DOCS = 3;
    const ID_VIDEOS = 9;
    const ID_FAQ = 28;
    const ID_REC = 37;

    private $paths = [];
    private $docs_children = [];

    public function scopeSlider($query)
    {
        return $query->where('parent_id', self::ID_SLIDER);
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Lists', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Lists', 'parent_id')->with('children')->orderBy('sort');
    }

    public function video_category()
    {
        return $this->hasOne('App\Models\CategoriesMaterials', 'id', 'reserve');
    }

    public function mainphoto($index = 0)
    {
        $fileName = 'no_photo.png';

        if (isset($this->photos[$index])) {

            if(File::exists(public_path('uploaded/' . $this->photos[$index]->source))) {
                $fileName = $this->photos[$index]->source;
            }

            if(Controller::hasWebp()){
                if(File::exists(public_path('uploaded/' . File::name($fileName) . '.webp'))){
                    $fileName = File::name($fileName) . '.webp';
                }
            }

            return config('photos.images_url') . $fileName;
        }

        return $fileName;
    }

    public function getValueAttribute()
    {
        return $this->attributes['description_short'];
    }

    public function getBtnName()
    {
        $locale = app()->getLocale();

        return $this->params["btn_name_$locale"] ?? '';
    }

    public function getBtnLink()
    {
        $locale = app()->getLocale();

        return $this->params["btn_link_$locale"] ?? '';
    }

    public function getNewTab()
    {
        return $this->params["new_tab"] ?? '';
    }

    public function getVideoId()
    {
        $video_link = $this->params["video_link"] ?? '';

        if(!empty($video_link)){
            $video_link = preg_replace('/^[^v]+v.(.{11}).*/', '$1', $video_link);
        }

        return $video_link;
    }

    //search by slug
    public static function get($slug)
    {
        $list = self::where('slug', $slug)->with(['children' => function ($query) {
            $query->with('photos')->enabled();
        }])->first();
        if (!isset($list)) {
            return new Collection();
        }
        return $list->children;
    }

    public function getOptionCost()
    {
        $cur_currency = session()->get('currency');
        $cur_currency_name = strtolower($cur_currency['name']);

        if ($cur_currency_name == "mdl"){
            return $this->params['price'] ?? 0;
        }

        return $this->params['price_' . $cur_currency_name] ?? 0;
    }

    public function isShowAlertModal()
    {
        return $this->params['show_alert_modal'] ?? false;
    }

    public static function getEmptySlideName()
    {
        return self::where('parent_id', 1)->count() + 1;
    }

    public function isEmptySlide()
    {
        return is_numeric($this->attributes['name']);
    }

    public function isVideo()
    {
        try {
            return str_contains($this->photos_slide[0]->source, 'mp4');
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getPaths()
    {
        if ($this->parent_id > 0) {
            $parent_list = Lists::find($this->parent_id);

            $this->paths[] = [
                "id" => $parent_list->id,
                "name" => $parent_list->name,
                "slug" => route('admin.lists.index', ['id' => $parent_list->id])
            ];
            $this->parent_id = $parent_list->parent_id;

            return $this->getPaths();
        } else {
            $this->paths[] = [
                "id" => $this->id,
                "name" => $this->name,
            ];
        }

        $this->paths = collect($this->paths)
            ->sortBy("id")
            ->toArray();

        return $this->paths;
    }

    public function getDocsChildren($list = [])
    {
        $lists = empty($list) ? [$this] : [$list];

        $children = collect();
        //$tmp = collect();

        while(count($lists) > 0){
            $nextChildren = collect();
            foreach ($lists as $list) {
                $children = $children->merge($list->children->all());
                $nextChildren = $nextChildren->merge($list->children->all());

                //$tmp[$list->id] = $list->children->pluck('name', 'id')->all();
            }

            $lists = $nextChildren;
        }

        return collect($children)
            ->pluck('name', 'id')
            ->toArray();
    }

    public static function isDocs($list_id)
    {
        if($list_id == Lists::ID_DOCS) return true;

        $list_docs = Lists::find(Lists::ID_DOCS);
        $children = (new Lists)->getDocsChildren($list_docs);

        if(collect($children)->has($list_id)){
            return true;
        }

        return false;
    }

    public function hasPdf()
    {
        return $this->params->pluck('pdf_file');
    }

    public static function getDocs()
    {
        $lists = [Lists::find(Lists::ID_DOCS)];

        //$children = collect();

        $tmp = collect();

        while(count($lists) > 0){
            $nextChildren = collect();
            foreach ($lists as $list) {
                //$children = $children->merge($list->children->all());
                $nextChildren = $nextChildren->merge($list->children->all());

                $tmp[$list->name] = $list->children->all();
            }

            $lists = $nextChildren;
        }

        return $tmp;
    }

    public function getHeading1()
    {
        $locale = Lang::getLocale();

        return $this->params["heading_1_$locale"] ?? '';
    }

    public function getHeading2()
    {
        $locale = Lang::getLocale();

        return $this->params["heading_2_$locale"] ?? '';
    }
}
