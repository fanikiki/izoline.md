<?php

namespace App\Models;

use App\Traits\CommonTrait;
use App\Traits\MetaTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class CategoriesMaterials extends Model
{
    use CommonTrait, MetaTrait;

    protected $guarded = ['id'];

    public function setSlugAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['slug'] = Str::slug($values);
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['slug'] = Str::slug($value);
                    continue;
                }
                $this->attributes['slug_' . $key] = Str::slug($value) ?? '';
            }
        }
    }

    public function videos()
    {
        return $this->hasMany('App\Models\Lists', 'reserve', 'id');
    }
}
