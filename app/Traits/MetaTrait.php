<?php

namespace App\Traits;

use App\Models\Meta;

trait MetaTrait
{
    public function meta()
    {
        return $this->morphOne(Meta::class, 'table');
    }

    public function saveMeta($request)
    {
        $fields = $request->only('meta_description', 'meta_keywords', 'title');

        $meta = $this->meta;

        if (!isset($meta)) {
            $meta = new Meta();
        }

        foreach ($fields as $field => $values) {

            foreach ($values as $key => $value) {

                if (! in_array($key, array_keys(config('app.locales')))) continue;

                if ($key == config('app.base_locale')) {
                    $meta->attributes[$field] = $value;
                    continue;
                }

                $meta->attributes[$field . '_' . $key] = $value;

            }
        }

        $this->meta()->save($meta);

    }

}
