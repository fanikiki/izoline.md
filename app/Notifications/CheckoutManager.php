<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CheckoutManager extends Notification implements ShouldQueue
{
    use Queueable;
    public $locale = 'ru';
    protected $form;
    protected $cart;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($form, $cart)
    {
        $this->locale = app()->getLocale();
        $this->form = $form;
        $this->cart = $cart;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->from(config('custom.from_address'), 'Izoline')
            ->subject('[' . env('APP_NAME', 'Izoline') . '] ' . trans('common.mail_checkout_manager'))
            ->view('emails.send-checkout-manager-form');

        $message->viewData['data'] = $this->form;
        $message->viewData['cart'] = $this->cart;

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
