<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderCreated extends Notification implements ShouldQueue
{
    use Queueable;
    public $locale = 'en';
    protected $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->locale = app()->getLocale();
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data['order'] = $this->order;
        $data['data'] = $this->order->data;
        $data['order_amount'] = $this->order->getSubTotalAmount();

        $message = (new MailMessage)
            ->from(config('custom.from_address'), 'Izoline')
            ->subject('[' . env('APP_NAME', 'Izoline') . '] ' . trans('common.order_from_site'))
            ->replyTo(config('custom.admin_email'), 'Izoline')
            ->view('emails.send-card');

        $message->viewData['data'] = $data;

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
