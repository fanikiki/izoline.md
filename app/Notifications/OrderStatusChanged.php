<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderStatusChanged extends Notification implements ShouldQueue
{
    use Queueable;
    public $locale = 'ru';

    protected $order;
    protected $new_status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order, $new_status)
    {
        $this->locale = $order->data['lang'];
        $this->order = $order;
        $this->new_status = $new_status;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data['order'] = $this->order;
        $data['data'] = $this->order->data;
        $data['order_amount'] = $this->order->getSubTotalAmount();
        if($this->order->status != Order::STATUS_DELIVERED) {
            $data['show_order_details'] = true;
        } else {
            $data['show_order_details'] = false;
        }

        $message = (new MailMessage)
            ->from(config('custom.from_address'), 'Izoline')
            ->subject('[' . env('APP_NAME', 'Izoline') . '] ' . trans('common.new_order_status'))
            ->replyTo(config('custom.admin_email'), 'Izoline')
            ->view('emails.order-status-changed');

        $message->viewData['data'] = $data;

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
