<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ConvertImages;
use App\Models\CategoriesMaterials;
use App\Models\Lists;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ListsController extends Controller
{
    public function index(Request $request)
    {
        $id      = $request->id;

        $parent = null;
        if ($id > 0){
            $parent  = Lists::where('id', $id)->with('children')->first();
            $data    = $parent->children;

        }else{
            $data    = Lists::where('parent_id', 0)->get();
        }

        $parents = $this->getParentsArray();
        $model = 'lists';

        return view('admin.lists.index')->with(compact('data', 'parents', 'id', 'model', 'parent'));
    }

    public function create(Request $request)
    {
        $parent_id = $request->id;
        $parents = Lists::whereDoesntHave('parent')->pluck('name', 'id')->toArray();

        switch ($parent_id){
            case Lists::ID_SLIDER:
                $view = view('admin.lists.edit_slide')->with(compact( 'parents', 'parent_id'));
                break;
            case Lists::ID_VIDEOS:
                $categories = CategoriesMaterials::pluck('name', 'id')->toArray();
                $view = view('admin.lists.edit_videos')->with(compact('parent_id', 'categories'));
                break;
            case Lists::ID_FAQ:
                $view = view('admin.lists.edit_faq')->with(compact('parents', 'parent_id'));
                break;
            default:
                $view = view('admin.lists.edit')->with(compact('parents', 'parent_id'));
                break;
        }

        if(Lists::isDocs($parent_id)){
            $docs_list = Lists::find(Lists::ID_DOCS);
            $parents = $docs_list->getDocsChildren();

            $view = view('admin.lists.edit_docs')->with(compact( 'parents', 'parent_id'));
        }

        return $view;
    }

    public function store(Request $request)
    {
        $rules = array(
            'name.*'      => 'required',
            //'slug'        => 'required'
        );

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return back()->withErrors($validator);
        }

        return $this->save($request);
    }

    private function save(Request $request, $id = null)
    {
        // store
        if (!isset($id)) {
            $data = new Lists();
            $data->created_at  = Carbon::now();
        }else{
            $data = Lists::find($id);
        }

        $data->name = $request->name;

        if (!empty($request->slug)) {
            $data->slug = Str::slug($request->slug);
        } else if (!empty($request->name['ru'])) {
            $data->slug = Str::slug($request->name['ru']);
        } else {
            $data->slug = time();
        }

        $data->parent_id         = $request->parent_id ?? 0;

        $data->description       = $request->description;
        $data->description_short = $request->description_short;

        //params
        if(!$request->has('is_docs')){
            if (!empty($request->params)) {
                $data->params = $request->params;
            }else{
                $data->params = null;
            }
        }

        $data->sort = $request->sort ?? 0;
        $data->reserve = $request->reserve ?? null;

        if($id == Lists::ID_REC){
            $data->params = $request->params;
        }

        $data->save();

        $this->UpdatePhotos($request, $data->id);

        if($request->has('is_docs')) {
            $this->savePdf($request, $data);
        }

        ConvertImages::dispatch($data->id, 'lists');

        // redirect
        Session::flash('message', trans('common.saved'));

        return redirect()->route('admin.lists.edit', $data->id);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function edit($id)
    {
        $data       = Lists::find($id);
        //$parents    = $this->getParentsArray();
        $parents    = Lists::whereDoesntHave('parent')->pluck('name', 'id')->toArray();
        $parent_id  = $data->parent_id;

        $view = view('admin.lists.edit')->with(compact('data', 'parents', 'parent_id'));

        switch ($parent_id){
            case Lists::ID_SLIDER:
                $view = view('admin.lists.edit_slide')->with(compact('data', 'parents', 'parent_id'));
                break;
            case Lists::ID_VIDEOS:
                $categories = CategoriesMaterials::pluck('name', 'id')->toArray();
                $view = view('admin.lists.edit_videos')->with(compact('data', 'parent_id', 'categories'));
                break;
            case Lists::ID_FAQ:
                $view = view('admin.lists.edit_faq')->with(compact('data', 'parents', 'parent_id'));
                break;
        }

        if ($id == Lists::ID_REC) {
            $products = Product::enabled()->oldest('name')->pluck('name', 'id')->toArray();
            $view = view('admin.lists.edit_recommended')->with(compact('data', 'parents', 'parent_id', 'products'));
        }

        if(Lists::isDocs($parent_id)){
            $docs_list = Lists::find(Lists::ID_DOCS);
            $parents = $docs_list->getDocsChildren();

            $view = view('admin.lists.edit_docs')->with(compact( 'data', 'parents', 'parent_id'));
        }

        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name.*'        => 'required',
            //'slug'          => 'required'
        );

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return back()->withErrors($validator);
        }

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Lists::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    private function getParentsArray()
    {
        $parent = Lists::where('parent_id', 0)->get();
        if ($parent->count() == 0){
            return [];
        }
        return $parent->pluck('name', 'id')->toArray();
    }

    public function savePdf($request, $data)
    {
        if(!empty($request->params['pdf_name']) && !empty($request->params['pdf_file'])){
            $params = $data->params;

            if (!File::exists(public_path("uploaded/docs"))) {
                File::makeDirectory(public_path("uploaded/docs"));
            }

            $list_docs_folder = public_path("uploaded/docs/$data->id");
            if (!File::exists($list_docs_folder)) {
                File::makeDirectory($list_docs_folder);
            }

            foreach ($request->params['pdf_file'] as $index => $file){
                $params[] = [
                    'pdf_name' => $request->params['pdf_name'][$index],
                    'pdf_file' => $file->getClientOriginalName()
                ];

                $path = $list_docs_folder . "/" . $file->getClientOriginalName();

                File::move($file->getRealPath(), $path);
                chmod($path, 0755);
            }

            $data->params = $params;
            $data->save();
        }
    }

    public function removePdf(Request $request)
    {
        $list_id = $request->get('id');
        $pdf_file = $request->get('pdf_file');
        $list = Lists::find($list_id);

        $new_params = $list->params->filter(function ($value, $key) use ($pdf_file) {
            return $value['pdf_file'] != $pdf_file;
        });

        $list->params = $new_params;
        $list->save();

        File::delete(public_path("uploaded/docs/$list_id/$pdf_file"));

        return ['success' => true];
    }
}
