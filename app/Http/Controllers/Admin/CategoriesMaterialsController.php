<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoriesMaterials;
use App\Models\Lists;
use App\Models\Parameters;
use App\Models\Product;
use App\Models\Settings;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CategoriesMaterialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = \request('id');
        $parent = CategoriesMaterials::find($id);

        return view('admin.categories_materials.index')->with(compact('id', 'parent'));
    }

    public static function datatablesCommon($query)
    {
        return Datatables::eloquent($query)
            ->editColumn('name', function ($item) {
                return view('admin.partials.datatable-title', compact('item'))->render();
            })
            ->addColumn('actions', function ($item) {
                return view('admin.partials.datatable-actions', compact('item'))->render();
            })
            ->rawColumns(['name', 'actions']);
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = CategoriesMaterials::query();

        return self::datatablesCommon($query)
            ->make(true);
    }

    public function create()
    {
        return view('admin.categories_materials.edit')->with($this->needs());
    }

    public function store(Request $request)
    {
        $rules = array(
            'name.*' => 'required',
            'slug'   => 'required|unique:categories_materials,id,{$id}'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        $data = CategoriesMaterials::updateOrCreate(['id' => $id],
            $request->except(
                [
                    'file_upload',
                    'photos',
                    'parent',
                    'show_menu'
                ]));

        $data->save();

        $data->saveMeta($request);

        // redirect
        Session::flash('message', trans('common.saved'));

        return redirect()->route('admin.categories_materials.edit', $data->id);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data = CategoriesMaterials::find($id);

        return view('admin.categories_materials.edit')->with(compact('data'))->with($this->needs($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name.*' => 'required',
            'slug'   => [
                'required',
                Rule::unique('categories_materials')->ignore($id, 'id')
            ]
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        CategoriesMaterials::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    private function needs($id = null)
    {
        $categories = CategoriesMaterials::query();
        if (isset($id)) {
            $categories->where('id', '!=', $id);
        }
        $categories = $categories->pluck('name', 'id')->toArray();

        return compact('categories');
    }
}
