<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Settings;
use App\Notifications\OrderStatusChanged;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class OrdersController extends Controller
{
    public function index()
    {
        $data = Order::where('status', '!=', Order::STATUS_DELETED)
            ->latest('created_at')
            ->get();

        $statuses = config('custom.order_status');

        foreach ($data as $item) {
            $item->title = "Заказ #" . $item->id;
            $item->status_name = $statuses[$item->status];
            $item->author = $item->user_id ? $item->user->name : '';
        }

        return view('admin.orders.index')->with(compact('data'));
    }

    public static function datatablesCommon($query)
    {
        $statuses = config('custom.order_status');

        $data = Datatables::eloquent($query)
            ->editColumn('created_at', function ($item) {
                return Carbon::parse($item->created_at)->format('d.m.Y H:i:s');
            })
            ->editColumn('amount', function ($item) use ($statuses) {
                try {
                    $amount = number_format($item->data['amount'], 2, '.', ' ');
                    $amount .= " " . $item->data['currency']['label'];
                } catch (\Exception $e) {
                    $amount = "";
                }

                return $amount;
            })
            ->editColumn('status', function ($item) use ($statuses) {
                return view('admin.partials.datatable-order-status', compact('item', 'statuses'))->render();
            })
            ->addColumn('actions', function ($item) {
                return view('admin.partials.datatable-actions', compact('item'))->render();
            })
            ->rawColumns(['status', 'actions']);

        $search = \request()->search['value'];
        if(!empty($search)){
            $data = $data->filter(function ($query2) use ($search){
                $query2->where(function ($query3) use ($search) {
                    $query3->orWhereRaw("orders.id like ?", "%$search%");
                    $query3->orWhereRaw("DATE_FORMAT(orders.created_at, '%d.%m.%Y') like ?", "%$search%");
                    $query3->orWhereRaw("orders.data like ?", "%$search%");
                });
            });
        }

        return $data;
    }

    public function ajaxData()
    {
        $query = Order::query()
            ->where('status', '!=', Order::STATUS_DELETED)
            ->with('user');

        return self::datatablesCommon($query)
            ->make(true);
    }

    private function save(Request $request, $id){
        // store
        $order = Order::find($id);

        if($request->status != $order->status) {
            try {
                Notification::route('mail', $order->data['orderInfo']['email'])
                    ->notify(new OrderStatusChanged($order, $request->status));
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            }
        }

        $order->status = $request->status;

        $order->save();

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect()->route('admin.orders.edit', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function edit($id)
    {
        $data['order'] = Order::find($id);
        $data['orderInfo'] = $data['order']->data['orderInfo'];
        $data['currency'] = $data['order']->data['currency'];

        $data['amount'] = $data['order']->data['amount'];
        $data['ip'] = $data['order']->data['ip'];
        $data['user_agent'] = $data['order']->data['user_agent'];
        $data['products'] = $data['order']->data['products'];

        return view('admin.orders.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->save($request, $id);
    }

    public function changeStatus(Request $request)
    {
        $order_id = $request->order_id;
        $new_status = $request->new_status;

        $order = Order::find($order_id);
        $order->status = $new_status;
        $order->save();

        return ['success' => true];
    }

    public function settings(Request $request)
    {
        $manager_email = Settings::get('manager_email');

        return view('admin.orders.settings')
            ->with(compact(
                'manager_email'
            ));
    }

    public function saveSettings(Request $request)
    {
        Settings::set("manager_email", $request->manager_email);

        Artisan::call('cache:clear');

        Session::flash('message', trans('common.saved'));

        return redirect()->to('/admin/orders/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->status = -1;
        $order->save();

        Session::flash('message', trans('common.deleted'));
        return back();
    }
}
