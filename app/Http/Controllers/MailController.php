<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use App\Notifications\AskFormMessage;
use App\Notifications\CheapPriceForm;
use App\Notifications\ConsultationForm;
use App\Notifications\ContactFormMessage;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Notification;

class MailController extends Controller
{
    private $from_mail;
    private $from_name;
    private $to_mail;
    private $to_name;

    public function __construct(){
        $this->from_mail = env('MAIL_FROM_ADDRESS');
        $this->from_name = trans('common.meta_title');
        $this->to_mail   = config('custom.admin_email');
        $this->to_name   = env('APP_URL');
    }

    public function sendAskForm(Request $request)
    {
        $manager_email = Settings::get('manager_email');

        Notification::route('mail', $manager_email)
            ->notify(new AskFormMessage($request->all()));

        return ['success' => true];
    }

    public function sendCheapForm(Request $request)
    {
        $manager_email = Settings::get('manager_email');

        Notification::route('mail', $manager_email)
            ->notify(new CheapPriceForm($request->all()));

        return ['success' => true];
    }

    public function sendConsultationForm(Request $request)
    {
        $manager_email = Settings::get('manager_email');

        Notification::route('mail', $manager_email)
            ->notify(new ConsultationForm($request->all()));

        return ['success' => true];
    }

    public function sendContactsForm(Request $request)
    {
        $manager_email = Settings::get('manager_email');

        Notification::route('mail', $manager_email)
            ->notify(new ContactFormMessage($request->all()));

        return ['success' => true];
    }
}
