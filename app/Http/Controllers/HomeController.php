<?php

namespace App\Http\Controllers;

use App\Models\Lists;
use App\Models\Manufacturers;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $slider = Lists::find(Lists::ID_SLIDER)->children;

        $recommended = CatalogController::getRecommended();

        $brands = Manufacturers::enabled()
            ->get();

        $faq = Lists::firstWhere('slug', 'main-faq')
            ->children;

        return view('index')->with(compact(
            'slider',
            'recommended',
            'brands',
            'faq'
        ));
    }

    public function getSearch(Request $request)
    {
        $searchword = $request->searchword;

        $productsQuery = Product::enabled()->searchByKeyword($searchword);
        $products = $productsQuery->paginate(16);

        return view('search-result', compact('searchword', 'products'));
    }

    public function changeCurrency($currency_name)
    {
        $currencies = config('custom.currencies');

        $index = array_search($currency_name, array_column($currencies, 'name'));

        if ($index !== false) {
            session()->put('currency', $currencies[$index]);
        }

        return back();
    }

    public function getWishlist()
    {
        $products = session('wishlist', []);
        $products = collect($products)->map(function ($id){
           return Product::find($id);
        });
        $products = Controller::paginate($products);

        return view('wishlist')->with(compact('products'));
    }

    public function addWishlist(Request $request)
    {
        $product_id = (int)$request->product_id;
        if (empty($product_id)) return ['success' => true];

        $wishlist = session('wishlist', []);

        if (!in_array($product_id, $wishlist)) {
            $wishlist[] = $product_id;
            session(['wishlist' => $wishlist]);
        }

        return ['success' => true];
    }

    public function removeWishlist(Request $request)
    {
        $product_id = $request->product_id;

        $wishlist = session('wishlist', []);

        $index = array_search($product_id, $wishlist);

        if ($index !== false) {
            unset($wishlist[$index]);
            session(['wishlist' => $wishlist]);
        }

        return ['success' => true];
    }

    public function getCompare()
    {
        $products = session('compare', []);
        $products = collect($products)->map(function ($id){
            return Product::find($id);
        });

        return view('compare')->with(compact('products'));
    }

    public function addCompare(Request $request)
    {
        $product_id = (int)$request->product_id;
        if (empty($product_id)) return ['success' => true];

        $wishlist = session('compare', []);

        if (!in_array($product_id, $wishlist)) {
            $wishlist[] = $product_id;
            session(['compare' => $wishlist]);
        }

        return ['success' => true];
    }

    public function removeCompare(Request $request)
    {
        $product_id = $request->product_id;

        $wishlist = session('compare', []);

        $index = array_search($product_id, $wishlist);

        if ($index !== false) {
            unset($wishlist[$index]);
            session(['compare' => $wishlist]);
        }

        return ['success' => true];
    }
}
