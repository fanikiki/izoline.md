<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Lists;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use mysql_xdevapi\Collection;

class CatalogController extends Controller
{
    public $sel_params = [];

    public function getCatalog(Request $request)
    {
        $categories = Categories::enabled()
            ->whereDoesntHave('parents')
            ->with('children')
            ->oldest('sort')
            ->get();

        $query = Product::query()
            ->with('list_thickness')
            ->where('enabled', true);

        $products = $this->filter($query, $request);

        $products = $products->get();
        foreach ($products as $p){
            $p->price_sort = (int)$p->getCurrencyTypePrice();
        }

        if(!empty($request->sort)){
            switch ($request->sort){
                case 'price_asc':
                    $products = $products->sortBy('price_sort');
                    break;
                case 'price_desc':
                    $products = $products->sortByDesc('price_sort');
                    break;
                default:
                    break;
            }
        }

        $products = Controller::paginate($products, 24);

        $recommended = CatalogController::getRecommended();

        $category_parameters = Categories::enabled()
            ->whereDoesntHave('parents')
            ->get()
            ->map(function ($item){
                return $item->parameters;
            })->flatten()->unique('id');

        $sel_params = $this->sel_params;

        return view('catalog.index')->with(compact(
            'categories',
            'products',
            'recommended',
            'category_parameters',
            'sel_params'
        ));
    }

    public function getParentCategory($category_slug, Request $request)
    {
        $parent_category = Categories::enabled()
            ->where('slug', $category_slug)
            ->firstOrFail();

        $products = collect();
        if($parent_category->products->count()){
            $query = Product::query();
            $query->where('products.enabled', true)
                ->join('products_categories', 'products.id', '=', 'products_categories.product_id')
                ->join('categories', 'products_categories.categories_id', '=', 'categories.id')
                ->where('categories.slug', $parent_category->slug)
                ->select('products.*');

            $products = $this->filter($query, $request);

            $products = $products->paginate(24);
        }

        $categories = $parent_category->children()->paginate(24);

        $recommended = CatalogController::getRecommended($parent_category->id);

        $sel_params = $this->sel_params;

        return view('catalog.category')
            ->with(compact(
                    'parent_category',
                    'categories',
                    'recommended',
                    'products',
                    'sel_params'
                )
            );
    }

    public function getChildrenCategory($category_slug, $subcategory_slug, Request $request)
    {
        $parent_category = Categories::enabled()
            ->where('slug', $category_slug)
            ->firstOrFail();

        $child_category = Categories::enabled()
            ->where('slug', $subcategory_slug)
            ->firstOrFail();

        $recommended = CatalogController::getRecommended($child_category->id);

        $categories = $parent_category->children;

        $products = Product::query()
            ->with('list_thickness')
            ->where('products.enabled', true)
            ->join('products_categories', 'products.id', '=', 'products_categories.product_id')
            ->join('categories', 'products_categories.categories_id', '=', 'categories.id')
            ->leftJoin('products_list_thickness', 'products_list_thickness.product_id', '=', 'products.id')
            ->where('categories.slug', $child_category->slug)
            ->selectRaw('products.*, IF(products_list_thickness.price_cubic IS NULL, products.price_cubic, products_list_thickness.price_cubic) AS thickness_price_cubic')
            ->groupBy('products.id');

        $products = $this->filter($products, $request);

        $products = $products->paginate(24);

        $category_parameters = Categories::enabled()
            ->whereDoesntHave('parents')
            ->get()
            ->map(function ($item){
                return $item->parameters;
            })->flatten()->unique('id');

        $sel_params = $this->sel_params;

        return view('catalog.subcategory')->with(compact(
            'parent_category',
            'child_category',
            'categories',
            'products',
            'recommended',
            'category_parameters',
            'sel_params'
        ));
    }

    private function filter($query, Request $request)
    {
        if($request->filled('price_range')){
            $price_range = explode(';', $request->price_range);
            $price_from = $price_range[0];
            $price_to = $price_range[1];

            if($request->price_range != '0;3456'){
                $query->whereRaw('(price_square OR thickness_price_cubic OR price_piece BETWEEN ? AND ?)', [$price_from, $price_to]);
            }

            $this->sel_params['price_from'] = $price_range[0];
            $this->sel_params['price_to'] = $price_range[1];
        }

        if($request->filled('parameters')) {
            $parameters = $request->parameters;

            $ids = collect();
            foreach ($parameters as $parameter_id => $values){
                $currentIds = DB::table('parameters_products')
                    ->where('parameter_id', $parameter_id)
                    ->whereIn('value_id', $values)
                    ->get()
                    ->pluck('product_id')
                    ->toArray();

                $this->sel_params['parameter_' . $parameter_id] = $values;

                if ($ids->isEmpty()) {
                    $ids = collect($currentIds);
                } else {
                    $ids = $ids->intersect($currentIds);
                }
            }

            if($ids->isNotEmpty()){
                $query->whereIn('products.id', $ids->toArray());
            }else{
                $query->where('products.id', -1);
            }
        }

        $sort = $request->sort;
        $product_type = $request->product_type;

        switch ($sort) {
            case "price_asc":
                switch ($product_type){
                    case Product::TYPE_LIST:
                    case Product::TYPE_LIST_XPS:
                        $query->oldest('thickness_price_cubic');
                        break;
                    case Product::TYPE_ROLL:
                        $query->oldest('price_square');
                        break;
                    case Product::TYPE_PIECE:
                        $query->oldest('price_piece');
                        break;
                    default:
                        break;
                }
                break;
            case "price_desc":
                switch ($product_type){
                    case Product::TYPE_LIST:
                    case Product::TYPE_LIST_XPS:
                        $query->latest('thickness_price_cubic');
                        break;
                    case Product::TYPE_ROLL:
                        $query->latest('price_square');
                        break;
                    case Product::TYPE_PIECE:
                        $query->latest('price_piece');
                        break;
                    default:
                        break;
                }
                break;
            default:
                $query->latest("products.created_at");
                break;
        }

        $this->sel_params['sort'] = $sort;

        return $query;
    }

    private function getCurrencyLbl()
    {
        return '_' . session('currency')['name'];
    }

    private function getLangLbl()
    {
        return '_' . Lang::getLocale();
    }

    public static function getRecommended($category_id = null)
    {
        $recommended_ids = Lists::find(Lists::ID_REC)->params;

        if(!empty($recommended_ids)){
            $recommended = Product::whereIn('id', $recommended_ids)->get();

            return $recommended
                ->take(12);
        }

        return collect();
    }

    public function getProductsJson(Request $request)
    {
        $query = Product::query();

        $filtered = $this->filter($query, $request);

        $products = $filtered->get()
            ->map(function ($product) {
                return [
                    'id' => $product->id,
                    'name' => $product->name,
                    'thumbphoto' => $product->thumbphoto(),
                    'mainphoto' => $product->mainphoto(),
                    'url' => $product->getRoute(),
                    'has_sale' => $product->hasSale(),
                    'price' => $product->getPrice(),
                    'price_currency' => $product->getPriceWithCurrency(),
                    'sale_price' => $product->getSalePrice(),
                    'sale_price_currency' => $product->getSalePriceWithCurrency()
                ];
            });

        return ['success' => true, 'products' => $products];
    }
}
