<?php

namespace App\Http\Controllers\Auth;

use App\Mail\SendRegisterSuccessful;
use App\Models\Promo;
use App\Models\User;
use App\Models\UserAuto;
use App\Notifications\UserRegistered;
use App\Notifications\UserRegisteredAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }

    public function showRegistrationForm(Request $request)
    {
        return view('auth.register');
    }

    public function register(Request $request) {
        $validation = $this->validator($request->all());
        if ($validation->fails()) {
            return ['success' => false, 'data' => [trans('common.email_user_found', [
                'login_route' => route('login'),
                'recover_route' => route('password.request')
            ])]];
        } else {
            $user = $this->create($request->all());

            $locale = app()->getLocale();
            $user->notify(new UserRegistered($user, $request->password, $locale));

            //письмо админу
            Notification::route('mail', config('custom.admin_email'))
                ->notify(new UserRegisteredAdmin($request->email, $locale));

            Auth::login($user);

            return ['success' => true];
        }
    }
}
