<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = route('index');
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login(Request $request)
    {
        // validate the info, create rules for the inputs
        $rules = array(
            'email' => 'required',
            'password' => 'required'
        );

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json(['success' => false, 'data' => $validation->errors()->toArray()]);
        }

        $userdata = array(
            'email' => $request->get('email'),
            'password' => $request->get('password')
        );

        // attempt to do the login
        if (!Auth::attempt($userdata)) {
            return response()->json(['success' => false, 'data' => [trans('common.failed')]]);
        }

        return response()->json(['success' => true]);
    }
}
