<?php

namespace App\Mail;

use App\Models\Reservation;
use GuzzleHttp\Psr7\Response;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class SendReservationNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $locale;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reservation_id)
    {
        $this->reservation = Reservation::find($reservation_id);
        if ($this->reservation->locale != '') {
            $this->locale = $this->reservation->locale;
        } else {
            $this->locale = config('app.fallback_locale');
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        App::setLocale($this->locale);
        $subject = trans('common.send_reservation_subject');
        return $this->view('emails.send-reservation')->subject($subject);
    }
}
