<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class SendRegisterSuccessful extends Mailable
{
    use Queueable, SerializesModels;

    /*
     * Data parameters
     */
    public $locale;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($locale = '')
    {
        if ($locale != '') {
            $this->locale = $locale;
        } else {
            $this->locale = config('app.fallback_locale');
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        App::setLocale($this->locale);
        return $this->view('emails.register-successful')->subject(trans('common.email_register_successful_title'));
    }

}
