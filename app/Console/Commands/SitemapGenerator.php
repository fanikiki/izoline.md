<?php

namespace App\Console\Commands;

use App\Models\Categories;
use App\Models\Content;
use App\Models\Product;
use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class SitemapGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 900);

        $sitemap = Sitemap::create();

        $this->categorySitemap($sitemap);
        $this->productSitemap($sitemap);
        $this->contentSitemap($sitemap);

        $sitemap->writeToFile(public_path('sitemap.xml'));
    }

    private function categorySitemap($sitemap)
    {
        $categories = Categories::enabled()->get();
        foreach ($categories as $category) {
            $sitemap->add(Url::create(route('catalog.get-parent-category', [$category->slug], false))
                ->setPriority(0.5)
                ->setChangeFrequency('daily')
                ->addAlternate('ru' . route('catalog.get-parent-category', [$category->slug], false), 'ru'));

            if ($category->children->isNotEmpty()) {
                foreach ($category->children as $child) {
                    if($child->enabled) {
                        $sitemap->add(Url::create(route('catalog.get-child-category', [$category->slug, $child->slug], false))
                            ->setPriority(0.5)
                            ->setChangeFrequency('daily')
                            ->addAlternate('ru' . route('catalog.get-child-category', [$category->slug, $child->slug], false), 'ru'));
                    }
                }
            }
        }
    }

    private function productSitemap($sitemap)
    {
        $products = Product::enabled()->get();
        foreach ($products as $product) {
            $sitemap->add(Url::create($product->getRoute(false))
                ->setPriority(0.5)
                ->setChangeFrequency('daily')
                ->addAlternate('ru' . $product->getRoute(false), 'ru'));
        }
    }

    private function contentSitemap($sitemap)
    {
        $sitemap->add(Url::create('/')
            ->setPriority(0.5)
            ->setChangeFrequency('daily')
            ->addAlternate('ru/', 'ru'));

        $sitemap->add(Url::create(route('get-contacts', [], false))
            ->setPriority(0.5)
            ->setChangeFrequency('daily')
            ->addAlternate('ru' . route('get-contacts', [], false), 'ru'));

        $content = Content::enabled()->get();
        foreach ($content as $c) {
            $sitemap->add(Url::create(route('content', $c->slug, false))
                ->setPriority(0.5)
                ->setChangeFrequency('daily')
                ->addAlternate('ru' . route('content', $c->slug, false), 'ru'));
        }
    }
}
