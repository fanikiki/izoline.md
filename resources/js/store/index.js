import {createStore} from "vuex";
import Api from "../services/api";
import cart from "./cart";
import shop from "./shop";

const store = createStore({
    state: {
        showWishListNotification: false,
        showCartNotification: false,
        showCompareNotification: false
    },
    mutations: {
        SHOW_CART_NOTIFICATION(state){
            state.showCartNotification = true;

            setTimeout(() => {
                state.showCartNotification = false;
            }, 1500);
        },
        SHOW_WISHLIST_NOTIFICATION(state){
            state.showWishListNotification = true;

            setTimeout(() => {
                state.showWishListNotification = false;
            }, 1500);
        },
        SHOW_COMPARE_NOTIFICATION(state){
            state.showCompareNotification = true;

            setTimeout(() => {
                state.showCompareNotification = false;
            }, 1500);
        }
    },
    actions: {
        async fetchLocaleMessages(context, data) {
            try {
                return await Api(false).get(`/locales/${data.lang}.json?id=${data.locale_timestamp}`).then(function (response) {
                    window.i18n = response.data;
                    return response.data;
                });
            } catch (e) {
                console.error(e);
            }
        },

        async addToWishlist({commit}, {product_id}) {
            commit('SHOW_WISHLIST_NOTIFICATION');

            try {
                const params = {
                    'product_id': product_id
                }

                await Api().post('/wishlist/add', params).then(response => response.data);
            } catch (e) {
                console.error(e);
            }
        },

        async removeWishlist({commit}, {product_id}) {
            try {
                const params = {
                    'product_id': product_id
                }

                await Api().post('/wishlist/remove', params).then(response => window.location.reload());
            } catch (e) {
                console.error(e);
            }
        },

        async addToCompare({commit}, {product_id}) {
            commit('SHOW_COMPARE_NOTIFICATION');

            try {
                const params = {
                    'product_id': product_id
                }

                await Api().post('/compare/add', params).then(response => response.data);
            } catch (e) {
                console.error(e);
            }
        },

        async removeCompare({commit}, {product_id}) {
            try {
                const params = {
                    'product_id': product_id
                }

                await Api().post('/compare/remove', params).then(response => response.data);

                window.location.reload();

            } catch (e) {
                console.error(e);
            }
        },
    },
    getters: {

    },
    modules: {
        cart,
        shop
    },
});

export {store};
