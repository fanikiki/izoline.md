import Api from "../services/api";

export default {
    state: {
        cartProducts: [],
        cartQty: 0,
        cartAmount: 0,
        cartWeight: 0,
        cartSurface: 0,
        validateForm: false
    },

    getters: {
        cartProducts: s => s.cartProducts,
        cartQty: s => s.cartQty,
        cartAmount: s => s.cartAmount,
        cartWeight: s => s.cartWeight,
        cartSurface: s => s.cartSurface
    },

    mutations: {
        REMOVE_CART(state, product){
            state.cartProducts = state.cartProducts.filter(p => p.hash !== product.hash);
        },
        SET_CART_PRODUCTS(state, products) {
            state.cartProducts = products;
        },
        SET_CART_QUANTITY(state, quantity) {
            state.cartQty = quantity;
        },
        SET_CART_AMOUNT(state, amount) {
            state.cartAmount = amount;
        },
        SET_CART_WEIGHT(state, weight){
            state.cartWeight = weight;
        },
        SET_CART_SURFACE(state, surface){
            state.cartSurface = surface;
        }
    },

    actions: {
        async addToCart({commit}, {id, q, thickness = null}) {
            commit('SHOW_CART_NOTIFICATION');

            try {
                const params = {
                    id: id,
                    q: q,
                    thickness: thickness
                }

                const response = await Api().post('/cart/add', params).then(response => response.data);

                commit('SET_CART_PRODUCTS', response.products);
                commit('SET_CART_QUANTITY', response.quantity);
                commit('SET_CART_AMOUNT', response.amount);
            } catch (e) {
                console.error(e);
            }
        },
        async getCartInfo({commit}) {
            try {
                const response = await Api().post('/cart/get-cart-info').then(response => response.data);

                commit('SET_CART_PRODUCTS', response.products);
                commit('SET_CART_QUANTITY', response.quantity);
                commit('SET_CART_AMOUNT', response.amount);
                commit('SET_CART_WEIGHT', response.weight);
                commit('SET_CART_SURFACE', response.surface);

            } catch (e) {
                console.error(e);
            }
        },
        async getCartQuantity({commit}) {
            try {
                const response = await Api().get('/cart/get-cart-quantity').then(response => response.data);

                commit('SET_CART_QUANTITY', response.quantity);
                commit('SET_CART_AMOUNT', response.amount);

            } catch (e) {
                console.error(e);
            }
        },
        async changeCartQuantity({commit}, product) {
            try {
                const params = {
                    id: product.id,
                    q: product.quantity,
                    thickness: product.thickness
                }

                const response = await Api().post('/cart/change-quantity', params).then(response => response.data);

                commit('SET_CART_QUANTITY', response.quantity);
                commit('SET_CART_AMOUNT', response.amount);
                commit('SET_CART_WEIGHT', response.weight);
                commit('SET_CART_SURFACE', response.surface);

            } catch (e) {
                console.error(e);
            }
        },
        async removeCart({commit}, product) {
            try {
                const params = {
                    id: product.id,
                    q: 0,
                    thickness: product.thickness
                }

                const response = await Api().post('/cart/change-quantity', params).then(response => response.data);

                commit('REMOVE_CART', product);
                commit('SET_CART_QUANTITY', response.quantity);
                commit('SET_CART_AMOUNT', response.amount);
                commit('SET_CART_WEIGHT', response.weight);
                commit('SET_CART_SURFACE', response.surface);

            } catch (e) {
                console.error(e);
            }
        },
        async submitCart({commit}, params) {
            try {
                return await Api().post('/cart/checkout', params).then(response => response.data);
            } catch (e) {
                console.error(e);
            }
        }
    }
}
