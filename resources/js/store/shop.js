import Api from "../services/api";
import query from './../mixins/query.mixin'

export default {
    state: {
        query_params: query.methods.getQueryParams(window.location.search)
    },

    getters: {

    },

    mutations: {

    },

    actions: {

    }
}
