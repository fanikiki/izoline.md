export default {
    methods: {
        getQueryParams(qs) {
            qs = qs.split('+').join(' ');

            var params = {},
                tokens,
                re = /[?&]?([^=]+)=([^&]*)/g;

            while (tokens = re.exec(qs)) {
                params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
            }

            return params;
        },
        serialize(obj) {
            let parameters = [];
            for (let property in obj) {
                if (obj.hasOwnProperty(property)) {
                    parameters.push(encodeURI(property + '=' + obj[property]));
                }
            }

            return parameters.join('&');
        },
        addQueryParamsToUrl(params) {
            let modifiedUrl = new URL(window.location.href);

            Object.keys(params).forEach(function (key) {
                if (modifiedUrl.searchParams.has(key)) {
                    modifiedUrl.searchParams.set(key, params[key]);
                } else {
                    modifiedUrl.searchParams.append(key, params[key]);
                }
            });

            window.history.replaceState(null, '', modifiedUrl.toString());
        }
    },
}
