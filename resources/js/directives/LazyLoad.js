export default {
    mounted: el => {
        function loadImage() {
            el.src = 'imgs/default/cookies.md-no-image-cake-thumbn.svg';

            setTimeout(function () {
                if (el.dataset && el.dataset.srcset) {
                    el.src = el.dataset.srcset
                    el.srcset = el.dataset.srcset
                }

                if(el.dataset.backgroundImage){
                    el.style.backgroundImage = el.dataset.backgroundImage
                }
            }, 25);
        }

        function handleIntersect(entries, observer) {
            entries.forEach(entry => {
                if (!entry.isIntersecting) {
                    return;
                } else {
                    loadImage();
                    observer.unobserve(el);
                }
            });
        }

        function createObserver() {
            const options = {
                root: null,
                threshold: "0"
            };

            const observer = new IntersectionObserver(handleIntersect, options);

            observer.observe(el);
        }

        if (!window["IntersectionObserver"]) {
            loadImage();
        } else {
            createObserver();
        }
    }
};
