import axios from 'axios'

export default (baseUrl = true) => {
    return axios.create({
        baseURL: baseUrl ? window.baseUrl : '',
        withCredentials: true,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            'Pragma': 'no-cache',
            'Expires': '0',
        }
    })
}
