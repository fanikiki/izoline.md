@extends('body')
@section('centerbox')
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a
                                href="{{ route('index') }}">@lang('common.home')</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active"
                            aria-current="page">@lang('common.compare')</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        @if($products->isNotEmpty())
            <div class="table-responsive table-bordered table-compare-list mb-10 border-0">
                <table class="table">
                    <tbody class="d-flex flex-column">
                    <tr class="d-flex">
                        <th class="min-w-120 min-width-200 min-width-200-md-lg d-flex justify-content-center">
                            <div class="my-auto">@lang('common.product')</div>
                        </th>
                        @foreach($products as $product)
                            <td class="col">
                                <a href="{{ route('catalog.get-product', $product->slug) }}" class="product d-block">
                                    <div class="product-compare-image">
                                        <div class="d-flex mb-3">
                                            <img style="width: 100px;" class="img-fluid mx-auto"
                                                 src="{{ $product->mainphoto() }}" alt="{{ $product->name }}">
                                        </div>
                                    </div>
                                    <h3 class="product-item__title text-blue font-weight-bold mb-3">{{ $product->name }}</h3>
                                </a>
                            </td>
                        @endforeach
                    </tr>
                    <tr class="d-flex">
                        <th class="min-w-120 min-width-200 min-width-200-md-lg d-flex justify-content-center">
                            <div class="my-auto">@lang('common.product_id')</div>
                        </th>
                        @foreach($products as $product)
                            <td class="col">{{ $product->sku }}</td>
                        @endforeach
                    </tr>
                    <tr class="d-flex">
                        <th class="min-w-120 min-width-200 min-width-200-md-lg d-flex justify-content-center">
                            <div class="my-auto">@lang('common.price')</div>
                        </th>
                        @foreach($products as $product)
                            <td class="col">
                                <div class="product-price">{!! $product->getCurrencyTypePrice() !!}</div>
                            </td>
                        @endforeach
                    </tr>

                    <tr class="d-flex">
                        <th class="min-w-120 min-width-200 min-width-200-md-lg d-flex justify-content-center">
                            <div class="my-auto">@lang('common.specification')</div>
                        </th>
                        @foreach($products as $product)
                            @if($product->params()->isNotEmpty())
                                <td class="col">
                                    <ul>
                                        @foreach($product->params() as $name => $values)
                                            <li><span>{{ $name }}: {{ implode(", ", $values) }}</span></li>
                                        @endforeach
                                    </ul>
                                </td>
                            @endif
                        @endforeach
                    </tr>

                    <tr class="d-flex">
                        <th class="min-w-120 min-width-200 min-width-200-md-lg d-flex justify-content-center">
                            <div class="my-auto">@lang('common.add_to_cart')</div>
                        </th>
                        @foreach($products as $product)
                            <td class="col">
                                <a href="javascript:void(0);"
                                   @click="$store.dispatch('addToCart', {'product_id': '{{ $product->id }}', 'q': 1})"
                                   class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-3 px-xl-5">@lang('common.add_to_cart')</a>
                            </td>
                        @endforeach
                    </tr>

                    <tr class="d-flex">
                        <th class="min-w-120 min-width-200 min-width-200-md-lg d-flex justify-content-center">
                            <div class="my-auto">@lang('common.remove')</div>
                        </th>
                        @foreach($products as $product)
                            <td class="text-center col">
                                <a href="javascript:void(0);"
                                   @click="$store.dispatch('removeCompare', {'product_id': '{{ $product->id }}'})"
                                   class="text-gray-90"><i class="fa fa-times"></i></a>
                            </td>
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>
        @else
            <di style="display: block; min-height: 400px;">
                <h6>@lang('common.no_products')</h6>
            </di>
        @endif
    </div>
@endsection
