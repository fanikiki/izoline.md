<!-- ========== FOOTER ========== -->
<footer>
    <div class="bg-primary py-3">
        <div class="container">
            <div class="row align-items-center px-3">
                <div class="col-md-4 col-lg-2">
                    <div class="row align-items-center">
                        <h2 class="font-size-24 mb-lg-0 mb-1 ml-3">@lang('common.have_question')</h2>
                    </div>
                </div>
                <div class="col-md-auto col-lg-10 d-flex">
                    <form class="js-form-message w-100 row" id="askFrm">
                        <div class="col-6 col-lg-3 px-3 mb-2 mb-lg-0">
                            <label class="sr-only" for="subscribeSrName">@lang('common.name')</label>
                            <input type="text" class="form-control border-0 height-40" name="name" id="subscribeSrName" placeholder="@lang('common.name')" aria-label="@lang('common.name')" required>
                        </div>
                        <div class="col-6 col-lg-3 px-3 mb-2 mb-lg-0">
                            <label class="sr-only" for="subscribeSrPhone">@lang('common.telephone')</label>
                            <input type="tel" class="form-control border-0 height-40" name="phone" id="subscribeSrPhone" placeholder="@lang('common.telephone')" aria-label="@lang('common.telephone')" required>
                        </div>
                        <label class="sr-only" for="subscribeSrEmail">@lang('common.your_question')</label>
                        <div class="input-group input-group-pill col-md-12 col-lg-6">
                            <input type="text" class="form-control border-0 height-40" name="question" id="subscribeSrEmail" placeholder="@lang('common.your_question')" aria-label="@lang('common.your_question')" aria-describedby="subscribeButton" required>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-dark btn-sm-wide height-40 py-2" id="subscribeButton">@lang('common.send')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer-newsletter -->
    <!-- Footer-bottom-widgets -->
    <div class="pt-8 pb-4 bg-gray-13">
        <div class="container mt-1">
            <div class="row">
                <div class="col-lg-5">
                    <div class="mb-6">
                        <a href="{{ route('index') }}" class="d-inline-block">
                            <img src="img/izoline-logo.png" width="150" alt="Izoline">
                        </a>
                    </div>
                    <div class="mb-4">
                        <div class="row no-gutters">
                            <div class="col-auto">
                                <i class="ec ec-support text-primary font-size-56"></i>
                            </div>
                            <div class="col pl-3">
                                <div class="font-size-13 font-weight-light">@lang('common.our_telephones')</div>
                                <a href="tel:@lang('common.site_phone1')" class="font-size-20 text-gray-90">@lang('common.site_phone1')</a><br>
                                <a href="tel:@lang('common.site_phone2')" class="font-size-20 text-gray-90">@lang('common.site_phone2')</a><br>
                                <a href="tel:@lang('common.site_phone3')" class="font-size-20 text-gray-90">@lang('common.site_phone3')</a>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <h6 class="mb-1 font-weight-bold">@lang('common.address')</h6>
                        <address class="">
                            @lang('common.site_address')
                        </address>
                    </div>
                    {{--<div class="my-4 my-md-4">
                        <ul class="list-inline mb-0 opacity-7">
                            <li class="list-inline-item mr-0">
                                <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                    <span class="fab fa-facebook-f btn-icon__inner"></span>
                                </a>
                            </li>
                            <li class="list-inline-item mr-0">
                                <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                    <span class="fab fa-google btn-icon__inner"></span>
                                </a>
                            </li>
                            <li class="list-inline-item mr-0">
                                <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                    <span class="fab fa-twitter btn-icon__inner"></span>
                                </a>
                            </li>
                        </ul>
                    </div>--}}
                </div>
                <div class="col-lg-7">
                    <div class="row">

                        @if(isset($footer_categories) && $footer_categories->isNotEmpty())
                            <div class="col-12 col-md mb-4 mb-md-0">
                                <h6 class="mb-3 font-weight-bold">@lang('common.our_goods')</h6>
                                <!-- List Group -->
                                <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                    @foreach($footer_categories->take(5) as $category)
                                        @if($category->parents->count())
                                            <li><a class="list-group-item list-group-item-action"
                                                   href="{{ route('catalog.get-child-category', [$category->parents[0]->slug, $category->slug]) }}">
                                                    {{ $category->name }}
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a class="list-group-item list-group-item-action"
                                                   href="{{ route('catalog.get-parent-category', $category->slug) }}">
                                                    {{ $category->name }}
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                                <!-- End List Group -->
                            </div>

                            <div class="col-12 col-md mb-4 mb-md-0">
                                <!-- List Group -->
                                <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                    @foreach($footer_categories->skip(5)->take(5) as $category)
                                        @if($category->parents->count())
                                            <li><a class="list-group-item list-group-item-action"
                                                   href="{{ route('catalog.get-child-category', [$category->parents[0]->slug, $category->slug]) }}">
                                                    {{ $category->name }}
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a class="list-group-item list-group-item-action"
                                                   href="{{ route('catalog.get-parent-category', $category->slug) }}">
                                                    {{ $category->name }}
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                                <!-- End List Group -->
                            </div>
                        @endif

                        <div class="col-12 col-md mb-4 mb-md-0">
                            <h6 class="mb-3 font-weight-bold">@lang('common.our_shop')</h6>
                            <!-- List Group -->
                            <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                <li><a class="list-group-item list-group-item-action" href="{{ route('index') }}">@lang('common.home')</a></li>
                                <li><a class="list-group-item list-group-item-action" href="{{ route('content', 'vozvrat-tovara') }}">@lang('common.returns')</a></li>
                                <li><a class="list-group-item list-group-item-action" href="{{ route('content', 'usloviya-dostavki') }}">@lang('common.delivery_terms')</a></li>
                                <li><a class="list-group-item list-group-item-action" href="{{ route('content', 'samovyvoz') }}">@lang('common.pickup')</a></li>
                                <li><a class="list-group-item list-group-item-action" href="{{ route('content', 'politika-konfidenczialinosti') }}">@lang('common.confidence')</a></li>
                                <li><a class="list-group-item list-group-item-action" href="{{ route('get-contacts') }}">@lang('common.contacts')</a></li>
                            </ul>
                            <!-- End List Group -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer-bottom-widgets -->
    <!-- Footer-copy-right -->
    <div class="bg-gray-14 py-2">
        <div class="container">
            <div class="flex-center-between d-block d-md-flex">
                <div class="mb-3 mb-md-0">© <a href="#" class="font-weight-bold text-gray-90">Izoline</a> - @lang('common.all_rights_reserved')</div>
                <div class="text-md-right">
                    <div class="mb-3 mb-md-0">Developed by <a href="https://xsort.md/" class="font-weight-bold text-gray-90">Xsort Web Studio</a></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer-copy-right -->
</footer>
<!-- ========== END FOOTER ========== -->
