<!-- ========== HEADER ========== -->
<header id="header" class="u-header u-header-left-aligned-nav">
    <div class="u-header__section">
        <!-- Topbar -->
        <div class="u-header-topbar py-2 d-none d-xl-block">
            <div class="container">
                <div class="d-flex align-items-center">
                    <div class="topbar-left">
                        <ul class="list-inline mb-0">
                            <li
                                class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                <a href="{{ route('content', 'usloviya-dostavki') }}"
                                    class="u-header-topbar__nav-link">@lang('common.shipping')</a>
                            </li>
                            <li
                                class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                <a href="{{ route('content', 'samovyvoz') }}"
                                    class="u-header-topbar__nav-link">@lang('common.pickup')</a>
                            </li>
                            <li
                                class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                <a href="{{ route('content', 'vozvrat-tovara') }}"
                                    class="u-header-topbar__nav-link">@lang('common.return_terms')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="topbar-right ml-auto">
                        <ul class="list-inline mb-0">
                            <li
                                class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                <a href="{{ route('get-contacts') }}" class="u-header-topbar__nav-link"><i
                                        class="ec ec-map-pointer mr-1"></i>@lang('common.address')</a>
                            </li>
                            <li
                                class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                <!-- Language -->
                                <label class="switch">
                                    <input type="checkbox">

                                    @if(Lang::getLocale() == 'ro')

                                    <a href="ru/{{ $cur_link }}">
                                        <span class="slider">
                                            <span class="text-switch pr-1">Ru</span>
                                        </span>
                                    </a>

                                    @elseif(Lang::getLocale() == 'ru')

                                    <a href="{{ $cur_link }}">
                                        <span class="slider-ru">
                                            <span class="text-switch-ru pl-1">Ro</span>
                                        </span>
                                    </a>

                                    @endif

                                </label>
                                <!-- End Language -->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Topbar -->

        <!-- Logo-Search-header-icons -->
        <div class="py-2 py-xl-5 bg-primary-down-lg">
            <div class="container my-0dot5 my-xl-0">
                <div class="row align-items-center">
                    <!-- Logo-offcanvas-menu -->
                    <div class="col-auto">
                        <!-- Nav -->
                        <nav
                            class="navbar navbar-expand u-header__navbar py-0 justify-content-xl-between max-width-270 min-width-270">
                            <!-- Logo -->
                            <a class="order-1 order-xl-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-center"
                                href="{{ route('index') }}" aria-label="Electro">
                                <img src="img/izoline-logo-1.png" alt="Izoline">
                            </a>
                            <!-- End Logo -->

                            <!-- Fullscreen Toggle Button -->
                            <div class="d-xl-none">
                                <div class="lang-indicator" id="sidebarHeaderInvokerMenu"
                                aria-controls="sidebarHeader" aria-haspopup="true" aria-expanded="false"
                                data-unfold-event="click" data-unfold-hide-on-scroll="false"
                                data-unfold-target="#sidebarHeader1" data-unfold-type="css-animation"
                                data-unfold-animation-in="fadeInLeft" data-unfold-animation-out="fadeOutLeft"
                                data-unfold-duration="500">
                                    @if(Lang::getLocale() == 'ro')
                                        <span>RO</span>
                                    @elseif(Lang::getLocale() == 'ru')
                                        <span>RU</span>
                                    @endif
                                </div>
                                
                                <button id="sidebarHeaderInvokerMenu" type="button"
                                    class="navbar-toggler d-block btn u-hamburger mr-3 mr-xl-0"
                                    aria-controls="sidebarHeader" aria-haspopup="true" aria-expanded="false"
                                    data-unfold-event="click" data-unfold-hide-on-scroll="false"
                                    data-unfold-target="#sidebarHeader1" data-unfold-type="css-animation"
                                    data-unfold-animation-in="fadeInLeft" data-unfold-animation-out="fadeOutLeft"
                                    data-unfold-duration="500">
                                    <span id="hamburgerTriggerMenu" class="u-hamburger__box">
                                        <span class="u-hamburger__inner"></span>
                                    </span>
                                </button>
                            </div>
                            <!-- End Fullscreen Toggle Button -->
                        </nav>
                        <!-- End Nav -->

                        <!-- ========== HEADER SIDEBAR ========== -->
                        <aside id="sidebarHeader1" class="u-sidebar u-sidebar--left"
                            aria-labelledby="sidebarHeaderInvokerMenu">
                            <div class="u-sidebar__scroller">
                                <div class="u-sidebar__container">
                                    <div class="u-header-sidebar__footer-offset pb-0">
                                        <!-- Toggle Button -->
                                        
                                        <!-- End Toggle Button -->

                                        <!-- Content -->
                                        <div class="js-scrollbar u-sidebar__body">
                                            <div id="headerSidebarContent"
                                                class="u-sidebar__content u-header-sidebar__content">
                                                <!-- Logo -->
                                                <div class="d-flex align-items-center justify-content-between u-header-sidebar__header">
                                                    <div>
                                                        <a class="d-flex ml-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-vertical mr-0 mb-0"
                                                            href="{{ route('index') }}" aria-label="Electro">
                                                            <img src="img/izoline-logo.png" alt="Izoline">
                                                        </a>
                                                    </div>

                                                    <div>
                                                        <label class="switch">
                                                            <input type="checkbox">

                                                            @if(Lang::getLocale() == 'ro')

                                                            <a href="ru/{{ $cur_link }}">
                                                                <span class="slider">
                                                                    <span class="text-switch pr-1">Ru</span>
                                                                </span>
                                                            </a>

                                                            @elseif(Lang::getLocale() == 'ru')

                                                            <a href="{{ $cur_link }}">
                                                                <span class="slider-ru">
                                                                    <span class="text-switch-ru pl-1">Ro</span>
                                                                </span>
                                                            </a>

                                                            @endif
                                                        </label>
                                                    </div>

                                                    <div class="button-close__mob-menu">
                                                        <button type="button" class="close pt-1"
                                                                id="buttonCloseHeader"
                                                                aria-controls="sidebarHeader1"
                                                                aria-haspopup="true"
                                                                aria-expanded="false"
                                                                data-unfold-event="click"
                                                                data-unfold-hide-on-scroll="false"
                                                                data-unfold-target="#sidebarHeader1"
                                                                data-unfold-type="css-animation"
                                                                data-unfold-animation-in="fadeInLeft"
                                                                data-unfold-animation-out="fadeOutLeft"
                                                                data-unfold-duration="500">
                                                            <span aria-hidden="true"><i class="ec ec-close-remove"></i></span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <!-- End Logo -->

                                                <!-- List -->
                                                <ul id="headerSidebarList" class="u-header-collapse__nav">
                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                        <a class="u-header-collapse__nav-link"
                                                            href="{{ route('index') }}">
                                                            @lang('common.home')
                                                        </a>
                                                    </li>

                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                        <a class="u-header-collapse__nav-link"
                                                            href="{{ route('catalog.get-catalog') }}">
                                                            @lang('common.shop')
                                                        </a>
                                                    </li>


                                                    @if(isset($site_categories) && $site_categories->isNotEmpty())
                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                        <a class="u-header-collapse__nav-link u-header-collapse__nav-pointer"
                                                            href="javascript:void(0);"
                                                            data-target="#categoriesSidebarPagesCollapse" role="button"
                                                            data-toggle="collapse" aria-expanded="false"
                                                            aria-controls="headerSidebarPagesCollapse">
                                                            @lang('common.categories')
                                                        </a>

                                                        <div id="categoriesSidebarPagesCollapse" class="collapse"
                                                            data-parent="#headerSidebarContent">
                                                            <ul id="headerSidebarPagesMenu"
                                                                class="u-header-collapse__nav-list">
                                                                @foreach($site_categories as $category)
                                                                <li><a class="u-header-collapse__submenu-nav-link"
                                                                        href="{{ route('catalog.get-parent-category', $category->slug) }}">{{
                                                                        $category->name }}</a></li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    @endif

                                                    <!-- Shop Columns -->
                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                        <a class="u-header-collapse__nav-link"
                                                            href="{{ route('get-contacts') }}">
                                                            @lang('common.contacts')
                                                        </a>
                                                    </li>
                                                    <!-- End Shop Columns -->
                                                </ul>
                                                <!-- End List -->
                                            </div>

                                            <div class="space-block"></div>

                                            <div class="navbar-nav__footer">
                                                <div class="d-block">
                                                    <div class="mb-3 pt-3">
                                                        <div class="row no-gutters">
                                                            <div class="col-auto">
                                                                <i class="ec ec-support text-primary font-size-56"></i>
                                                            </div>
                                                            <div class="col pl-3">
                                                                <div class="font-size-13 font-weight-light">@lang('common.our_telephones')</div>
                                                                <a href="tel:@lang('common.site_phone1')" class="font-size-16 text-gray-90">@lang('common.site_phone1')</a><br>
                                                                <a href="tel:@lang('common.site_phone2')" class="font-size-16 text-gray-90">@lang('common.site_phone2')</a><br>
                                                                <a href="tel:@lang('common.site_phone3')" class="font-size-16 text-gray-90">@lang('common.site_phone3')</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="pb-3">
                                                        <h6 class="mb-1 font-weight-bold text-center font-size-15">@lang('common.address')</h6>
                                                        <address class="text-center font-size-13 mb-0">
                                                            @lang('common.site_address')
                                                        </address>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Content -->
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <!-- ========== END HEADER SIDEBAR ========== -->
                    </div>
                    <!-- End Logo-offcanvas-menu -->
                    <!-- Search Bar -->
                    <div class="col d-none d-xl-block">
                        <form class="js-focus-state" action="{{ route('get-search-result') }}">
                            <label class="sr-only" for="searchProduct">Search</label>
                            <div class="input-group">
                                <input type="text"
                                    class="form-control py-2 pl-5 font-size-15 border-right-0 height-40 border-width-2 rounded-left-pill border-primary"
                                    name="searchword" id="searchProduct" placeholder="@lang('common.search')"
                                    value="{{ session('searchword') }}" required>
                                <div class="input-group-append">
                                    <!-- Select -->
                                    <select
                                        class="js-select selectpicker dropdown-select custom-search-categories-select"
                                        data-style="btn height-40 text-gray-60 font-weight-normal border-top border-bottom border-left-0 rounded-0 border-primary border-width-2 pl-0 pr-5 py-2"
                                        name="category_id">
                                        <option value="">@lang('common.all_categories')</option>
                                        @isset($site_categories)
                                        @foreach($site_categories as $category)
                                        <option value="{{ $category->id }}" @if(session('category_id')==$category->
                                            id)selected @endif>{{ $category->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <!-- End Select -->
                                    <button class="btn btn-primary height-40 py-2 px-3 rounded-right-pill" type="submit"
                                        id="searchProducts">
                                        <span class="ec ec-search font-size-24"></span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End Search Bar -->
                    <!-- Customer Care -->
                    <div class="d-none d-xl-block col-md-auto">
                        <div class="d-flex">
                            <i class="ec ec-support font-size-50 text-primary"></i>
                            <div class="ml-2">
                                <div class="phone">
                                    <strong><a href="tel:@lang('common.site_phone1')"
                                            class="text-gray-90">@lang('common.site_phone1')</a></strong>
                                </div>
                                <div class="email">
                                    E-mail: <a href="mailto:@lang('common.site_email')"
                                        class="text-gray-90">@lang('common.site_email')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Customer Care -->
                    <!-- Header Icons -->
                    <div class="col col-xl-auto text-right text-xl-left pl-0 pl-xl-3 position-static">
                        <div class="d-inline-flex">
                            <ul class="d-flex list-unstyled mb-0 align-items-center">
                                <!-- Search -->
                                <li class="col d-xl-none px-2 px-sm-3 position-static">
                                    <a id="searchClassicInvoker"
                                        class="font-size-22 text-gray-90 text-lh-1 btn-text-secondary"
                                        href="javascript:;" role="button" data-toggle="tooltip" data-placement="top"
                                        title="@lang('common.search')" aria-controls="searchClassic"
                                        aria-haspopup="true" aria-expanded="false" data-unfold-target="#searchClassic"
                                        data-unfold-type="css-animation" data-unfold-duration="300"
                                        data-unfold-delay="300" data-unfold-hide-on-scroll="true"
                                        data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
                                        <span class="ec ec-search"></span>
                                    </a>

                                    <!-- Input -->
                                    <div id="searchClassic"
                                        class="dropdown-menu dropdown-unfold dropdown-menu-right left-0 mx-2"
                                        aria-labelledby="searchClassicInvoker">
                                        <form class="js-focus-state input-group px-3"
                                            action="{{ route('get-search-result') }}">
                                            <input class="form-control" type="search" name="searchword"
                                                value="{{ session('searchword') }}"
                                                placeholder="@lang('common.search')">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary px-3" type="submit"><i
                                                        class="font-size-18 ec ec-search"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- End Input -->
                                </li>
                                <!-- End Search -->
                                <li class="col"><a href="{{ route('get-compare') }}" class="text-gray-90"
                                        data-toggle="tooltip" data-placement="top" title="@lang('common.compare')"><i
                                            class="font-size-22 ec ec-compare"></i></a></li>
                                <li class="col">
                                    <a href="{{ route('get-wishlist') }}" class="text-gray-90" data-toggle="tooltip"
                                        data-placement="top" title="" data-original-title="@lang('common.wishlist')">
                                        <i class="font-size-22 ec ec-favorites"></i>
                                    </a>
                                </li>
                                <li class="col pr-0">
                                    <cart cart_route="{{ route('cart.index') }}"></cart>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Header Icons -->
                </div>
            </div>
        </div>
        <!-- End Logo-Search-header-icons -->

        <!-- Primary-menu-wide -->
        <div class="d-none d-xl-block bg-primary">
            <div class="container">
                <div class="min-height-45">
                    <!-- Nav -->
                    <nav
                        class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--wide u-header__navbar--no-space">
                        <!-- Navigation -->
                        <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
                            <ul class="navbar-nav u-header__navbar-nav">
                                <!-- Home -->
                                <li class="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover"
                                    data-animation-in="slideInUp" data-animation-out="fadeOut" data-position="left">
                                    <a id="homeMegaMenu" class="nav-link u-header__nav-link" href="{{ route('index') }}"
                                        aria-haspopup="true" aria-expanded="false">@lang('common.home')</a>
                                </li>
                                <!-- End Home -->

                                <!-- Shop -->
                                <li class="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover"
                                    data-animation-in="slideInUp" data-animation-out="fadeOut" data-position="left">
                                    <a id="ShopMegaMenu" class="nav-link u-header__nav-link"
                                        href="{{ route('catalog.get-catalog') }}" aria-haspopup="true"
                                        aria-expanded="false">@lang('common.shop')</a>
                                </li>
                                <!-- End Shop -->

                                <!-- Categories -->
                                <li id="category-box" class="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover"
                                    data-animation-in="slideInUp" data-animation-out="fadeOut">

                                    <div class="img-categories-box">
                                        <img alt="Izoline" class="img-categories" src="img/cat_back.png" />
                                    </div>
                                    <a id="CategoriesMegaMenu"
                                        class="nav-link u-header__nav-link u-header__nav-link-toggle"
                                        href="javascript:;" aria-haspopup="true"
                                        aria-expanded="false">@lang('common.categories')</a>


                                    <!-- Categories - Mega Menu -->
                                    <div class="hs-mega-menu w-50 u-header__sub-menu"
                                        aria-labelledby="CategoriesMegaMenu">
                                        <div class="row u-header__mega-menu-wrapper">
                                            @if(isset($site_categories) && $site_categories->isNotEmpty())
                                            @foreach($site_categories as $category)
                                            @if (count($category->children) > 0)
                                            <div class="col-md-4">
                                                <span class="u-header__sub-menu-title">
                                                    <a href="{{ route('catalog.get-parent-category', $category->slug) }}"
                                                        class="title-link">{{ $category->name }}</a>
                                                </span>
                                                <ul class="u-header__sub-menu-nav-group mb-3">
                                                    @foreach($category->children as $subcategory)
                                                    <li>
                                                        <a href="{{ route('catalog.get-child-category', [$category->slug, $subcategory->slug]) }}"
                                                            class="nav-link u-header__sub-menu-nav-link">{{
                                                            $subcategory->name }}</a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                            @endforeach
                                            <div class="col-md-4">
                                                @foreach($site_categories as $category)
                                                @if (!count($category->children) > 0)
                                                <span class="u-header__sub-menu-title">
                                                    <a href="{{ route('catalog.get-parent-category', $category->slug) }}"
                                                        class="title-link">{{ $category->name }}</a>
                                                </span>
                                                @endif
                                                @endforeach
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- End Categories - Mega Menu -->
                                </li>
                                <!-- End Pages -->

                                <!-- Contacts -->
                                <li class="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover"
                                    data-animation-in="slideInUp" data-animation-out="fadeOut">
                                    <a id="ContactsMegaMenu" class="nav-link u-header__nav-link"
                                        href="{{ route('get-contacts') }}" aria-haspopup="true"
                                        aria-expanded="false">@lang('common.contacts')</a>
                                </li>
                                <!-- End Contacts -->

                                <!-- About -->
                                <li class="nav-item hs-has-mega-menu u-header__nav-item" data-event="hover"
                                    data-animation-in="slideInUp" data-animation-out="fadeOut" data-position="left">
                                    <a id="AboutMegaMenu" class="nav-link u-header__nav-link"
                                        href="{{ route('content', 'about-us') }}" aria-haspopup="true"
                                        aria-expanded="false">@lang('common.about_us')</a>
                                </li>
                                <!-- End About -->

                            </ul>
                        </div>
                        <!-- End Navigation -->
                    </nav>
                    <!-- End Nav -->
                </div>
            </div>
        </div>
        <!-- End Primary-menu-wide -->
    </div>
</header>
<!-- ========== END HEADER ========== -->
