<div class="main-product-item__outer h-100 w-100">
    <div class="product-item__inner px-xl-4 p-3">
        <div class="product-item__body pb-xl-2">
            <div class="mb-2 product-category">
                <small>{{ $product->category[0]->name }}</small>
            </div>
            <h5 class="mb-1 product-item__title">
                <a href="{{ route('catalog.get-product', [$product->slug]) }}" class="text-blue font-weight-bold">{{ $product->name }}</a>
            </h5>
            <div class="mb-2">
                <a href="{{ route('catalog.get-product', [$product->slug]) }}" class="d-block text-center">
                    <img class="img-fluid mx-auto" src="{{ $product->mainphotothumb() }}"
                         alt="{{ $product->name }}" style="height: 200px;"></a>
            </div>
            <div class="mb-2 product-params">
                {!! $product->params_text !!}
            </div>
            <div class="flex-center-between mb-1">
                <div class="prodcut-price d-flex align-items-center position-relative">
                    <ins class="font-size-20 text-color-special text-decoration-none">{!! $product->getCurrencyTypePrice() !!}</ins>
                </div>
                <div class="prodcut-add-cart">
                    <a @click="$store.dispatch('addToCart', {'id': '{{ $product->id }}', 'q': 1})"
                       href="javascript:void(0);"
                       class="btn-add-cart btn-primary transition-3d-hover"><i
                            class="ec ec-add-to-cart"></i></a>

                    @if(Route::is('get-wishlist'))
                        <a @click="$store.dispatch('removeWishlist', {'product_id': '{{ $product->id }}'})"
                           href="javascript:void(0);"
                           class="btn-add-cart remove-wishlist-mobile btn-primary text-gray-6 font-size-13"><i
                                class="fa fa-trash"></i></a>
                    @endif
                </div>
            </div>
        </div>
        <div class="product-item__footer">
            <div class="border-top pt-2 flex-center-between flex-wrap">
                <a @click="$store.dispatch('addToCompare', {'product_id': '{{ $product->id }}'})"
                   href="javascript:void(0);"
                   class="text-gray-6 font-size-13">
                    <i class="ec ec-compare mr-1 font-size-15"></i> @lang('common.add_compare')</a>

                @if(!in_array($product->id, session('wishlist', [])))
                    <a @click="$store.dispatch('addToWishlist', {'product_id': '{{ $product->id }}'})" href="javascript:void(0);" class="text-gray-6 font-size-13"><i
                            class="ec ec-favorites mr-1 font-size-15"></i>@lang('common.add_wishlist')</a>
                @endif

                @if(Route::is('get-wishlist'))
                    <a @click="$store.dispatch('removeWishlist', {'product_id': '{{ $product->id }}'})" href="javascript:void(0);" class="text-gray-6 font-size-13"><i
                            class="fa fa-trash"></i> @lang('common.remove')</a>
                @endif
            </div>
        </div>
    </div>
</div>
