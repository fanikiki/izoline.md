<div class="product-item__outer w-100">
    <div class="product-item__inner remove-prodcut-hover py-4 row">
        <div class="product-item__header col-6 col-md-2">
            <div class="mb-2">
                <a href="{{ route('catalog.get-product', [$product->slug]) }}"
                   class="d-block text-center">
                    <img class="img-fluid"
                         src="{{ $product->mainphotothumb() }}"
                         alt="{{ $product->name }}">
                </a>
            </div>
        </div>
        <div class="product-item__body col-6 col-md-7">
            <div class="pr-lg-10">
                <div class="mb-2">
                    <small>{{ $product->category[0]->name }}</small>
                </div>
                <h5 class="mb-2 product-item__title">
                    <a href="{{ route('catalog.get-product', [$product->slug]) }}"
                        class="text-blue font-weight-bold">
                        {{ $product->name }}
                    </a>
                </h5>
                <div class="prodcut-price d-md-none">
                    <div class="text-gray-100">{{ $product->getPriceWithCurrency() }}</div>
                </div>

                <div class="mb-2 product-params">
                    {!! $product->params_text !!}
                </div>
            </div>
        </div>
        <div class="product-item__footer col-md-3 d-md-block">
            <div class="mb-2 flex-center-between">
                <div class="prodcut-price">
                    <div class="text-gray-100">{!! $product->getCurrencyTypePrice() !!}</div>
                </div>
                <div class="prodcut-add-cart">
                    <a @click="$store.dispatch('addToCart', {'id': '{{ $product->id }}', 'q': 1})"
                       href="javascript:void(0);"
                       class="btn-add-cart btn-primary transition-3d-hover"><i
                            class="ec ec-add-to-cart"></i></a>
                </div>
            </div>
            <div
                class="flex-horizontal-center justify-content-between justify-content-wd-center flex-wrap border-top pt-3">
                <a @click="$store.dispatch('addToCompare', {'product_id': '{{ $product->id }}'})"
                   href="javascript:void(0);"
                   class="text-gray-6 font-size-13 mx-wd-3"><i
                        class="ec ec-compare mr-1 font-size-15"></i> @lang('common.add_compare')</a>

                @if(!in_array($product->id, session('wishlist', [])))
                    <a @click="$store.dispatch('addToWishlist', {'product_id': '{{ $product->id }}'})" href="javascript:void(0);"
                       class="text-gray-6 font-size-13 mx-wd-3"><i
                            class="ec ec-favorites mr-1 font-size-15"></i>@lang('common.add_wishlist')</a>
                @endif
            </div>
        </div>
    </div>
</div>
