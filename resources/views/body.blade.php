<!DOCTYPE html>
<html lang="{{ Lang::getLocale() }}">
<head>
    <base href="/">
    <!-- Title -->
    <title>@yield('title', trans('common.title'))</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta (OG) -->
    <meta name="keywords" content="@yield('meta_keywords', trans('common.meta_keywords'))">
    <meta name="description" content="@yield('meta_description', trans('common.meta_description'))">

    <meta property="og:site_name" content="izoline.md" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="@yield('title', trans('common.title'))" />
    <meta property="og:description" content="@yield('meta_description', trans('common.meta_description'))" />
    <meta property="og:image" itemprop="image" content="@yield('og_image', url('/img/share.jpg'))" />
    <meta name="twitter:image" itemprop="image" content="@yield('og_image', url('/img/share.jpg'))" />
    <meta property="og:locale" content="{{ app()->getLocale() }}" />

    @hasSection('og_price_amount')
        <meta property="og:price:amount" content="@yield('og_price_amount')" />
    @endif
    @hasSection('og_price_currency')
        <meta property="og:price:currency" content="@yield('og_price_currency')" />
    @endif
    @hasSection('og_product_brand')
        <meta property="product:brand" content="@yield('og_product_brand')" />
    @endif
    @hasSection('og_product_availability')
        <meta property="product:availability" content="@yield('og_product_availability')" />
    @endif
    @hasSection('og_product_condition')
        <meta property="product:condition" content="@yield('og_product_condition')" />
    @endif

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicons/favicon.ico') }}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="css/font-electro.css">

    <link rel="stylesheet" href="vendor/animate.css/animate.min.css">
    <link rel="stylesheet" href="vendor/hs-megamenu/src/hs.megamenu.css">
    <link rel="stylesheet" href="vendor/ion-rangeslider/css/ion.rangeSlider.css">
    <link rel="stylesheet" href="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="vendor/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="vendor/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="vendor/bootstrap-select/dist/css/bootstrap-select.min.css">

    <!-- CSS Electro Template -->
    {{--<link rel="stylesheet" href="css/theme.css">--}}

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    @yield('styles')

    <script>
        @if(isset($locale_timestamp))
            window.locale_timestamp = "{{ $locale_timestamp }}";
        @endif
        window.lang = "{{ Lang::getLocale() }}";
        window.baseUrl = "{{ route('index') }}";
        window.isAuth = "{{ auth()->check() ?? '' }}";
    </script>

    @if(app()->environment('prod'))
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5NFV6JVD');</script>
        <!-- End Google Tag Manager -->
    @endif
</head>

<body class="body">
@if(app()->environment('prod'))
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NFV6JVD"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
@endif

<div id="app">
    <notification-popup></notification-popup>

    @include('partials.header')

    <!-- ========== MAIN CONTENT ========== -->
    <main id="content" role="main">
        @yield('centerbox')
    </main>
    <!-- ========== END MAIN CONTENT ========== -->

    @include('partials.footer')

    <!--Cookies-->
    <div class="d-none justify-content-center align-items-end cookies">
        <div class="cookies-block container">
            <div class="mr-2 cookies-text">
                <p class="mb-0">
                    @lang('common.cookies_text')
                </p>

                <p class="mb-0">
                    @lang('common.cookies_text_second')
                    <a class="text-blue" href="{{ route('content', 'politika-konfidenczialinosti') }}">
                        @lang('common.confidence')
                    </a>
                </p>
            </div>

            <div class="ml-2">
                <button type="button" class="btn btn-white button-good">
                    @lang('common.good')
                </button>
            </div>
        </div>
    </div>
</div>

@include('partials.modals')

<!-- Go to Top -->
<a class="js-go-to u-go-to" href="#"
   data-position='{"bottom": 115, "right": 30 }'
   data-type="fixed"
   data-offset-top="400"
   data-compensation="#header"
   data-show-effect="slideInUp"
   data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
</a>
<!-- End Go to Top -->

<!-- JS Global Compulsory -->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
<script src="vendor/popper.js/dist/umd/popper.min.js"></script>
<script src="vendor/bootstrap/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="vendor/appear.js"></script>
<script src="vendor/jquery.countdown.min.js"></script>
<script src="vendor/hs-megamenu/src/hs.megamenu.js"></script>
<script src="vendor/svg-injector/dist/svg-injector.min.js"></script>
<script src="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="vendor/fancybox/jquery.fancybox.min.js"></script>
<script src="vendor/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
<script src="vendor/typed.js/lib/typed.min.js"></script>
<script src="vendor/slick-carousel/slick/slick.js"></script>
<script src="vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- JS Electro -->
<script src="js/hs.core.js"></script>
<script src="js/components/hs.countdown.js"></script>
<script src="js/components/hs.header.js"></script>
<script src="js/components/hs.hamburgers.js"></script>
<script src="js/components/hs.unfold.js"></script>
<script src="js/components/hs.focus-state.js"></script>
<script src="js/components/hs.malihu-scrollbar.js"></script>
<script src="js/components/hs.validation.js"></script>
<script src="js/components/hs.fancybox.js"></script>
<script src="js/components/hs.onscroll-animation.js"></script>
<script src="js/components/hs.slick-carousel.js"></script>
<script src="js/components/hs.range-slider.js"></script>
<script src="js/components/hs.show-animation.js"></script>
<script src="js/components/hs.svg-injector.js"></script>
<script src="js/components/hs.go-to.js"></script>
<script src="js/components/hs.selectpicker.js"></script>

<script src="{{ mix('js/vendor.js') }}"></script>
<script src="{{ mix('js/manifest.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>

@yield('scripts')

<!-- JS Plugins Init. -->
<script>
    $(window).on('load', function () {
        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            direction: 'horizontal',
            pageContainer: $('.container'),
            breakpoint: 767.98,
            hideTimeOut: 0
        });

        // initialization of svg injector module
        $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#header'));

        // initialization of animation
        $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
            afterOpen: function () {
                $(this).find('input[type="search"]').focus();
            }
        });

        // initialization of popups
        $.HSCore.components.HSFancyBox.init('.js-fancybox');

        // initialization of countdowns
        var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
            yearsElSelector: '.js-cd-years',
            monthsElSelector: '.js-cd-months',
            daysElSelector: '.js-cd-days',
            hoursElSelector: '.js-cd-hours',
            minutesElSelector: '.js-cd-minutes',
            secondsElSelector: '.js-cd-seconds'
        });

        // initialization of malihu scrollbar
        $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

        // initialization of forms
        $.HSCore.components.HSFocusState.init();

        // initialization of form validation
        $.HSCore.components.HSValidation.init('.js-validate', {
            rules: {
                confirmPassword: {
                    equalTo: '#signupPassword'
                }
            }
        });

        $.HSCore.components.HSRangeSlider.init('.js-range-slider');

        // initialization of show animations
        $.HSCore.components.HSShowAnimation.init('.js-animation-link');

        // initialization of fancybox
        $.HSCore.components.HSFancyBox.init('.js-fancybox');

        // initialization of slick carousel
        $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');

        // initialization of hamburgers
        $.HSCore.components.HSHamburgers.init('#hamburgerTrigger');

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
            beforeClose: function () {
                $('#hamburgerTrigger').removeClass('is-active');
            },
            afterClose: function() {
                $('#headerSidebarList .collapse.show').collapse('hide');
            }
        });

        function addQueryParamsToUrl(params) {
            let modifiedUrl = new URL(window.location.href);

            Object.keys(params).forEach(function (key) {
                if (modifiedUrl.searchParams.has(key)) {
                    modifiedUrl.searchParams.set(key, params[key]);
                } else {
                    modifiedUrl.searchParams.append(key, params[key]);
                }
            });

            window.history.replaceState(null, '', modifiedUrl.toString());
        }

        $('#headerSidebarList [data-toggle="collapse"]').on('click', function (e) {
            e.preventDefault();

            var target = $(this).data('target');

            if($(this).attr('aria-expanded') === "true") {
                $(target).collapse('hide');
            } else {
                $(target).collapse('show');
            }
        });

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

        // initialization of select picker
        $.HSCore.components.HSSelectPicker.init('.js-select');

        $("#exampleModal").modal('show');

        /**
         * FORMS
         */
        $("#askFrm").submit(function (e){
            e.preventDefault();

            let _this = $(this);
            _this.find("button").attr("disabled", true);

            $.post("{{ route('send-ask-form') }}", $(this).serialize(), function (){
                _this.find("button").attr("disabled", false);
                _this[0].reset();

                $("#askFrmModal").modal("show");
            });
        });
        $("#cheapFrm").submit(function (e){
            e.preventDefault();

            let _this = $(this);
            _this.find("button").attr("disabled", true);

            $.post("{{ route('send-cheap-form') }}", $(this).serialize(), function (){
                _this.find("button").attr("disabled", false);
                _this[0].reset();

                $("#cheapFrmModal").modal("hide");
                $("#askFrmModal").modal("show");
            });
        });
        $("#consultationFrm").submit(function (e){
            e.preventDefault();

            let _this = $(this);
            _this.find("button").attr("disabled", true);

            $.post("{{ route('send-consultation-form') }}", $(this).serialize(), function (){
                _this.find("button").attr("disabled", false);
                _this[0].reset();

                $("#consultationFrmModal").modal("hide");
                $("#askFrmModal").modal("show");
            });
        });
        $("#contactFrm").submit(function (e){
            e.preventDefault();

            let _this = $(this);
            _this.find("button").attr("disabled", true);

            $.post("{{ route('send-contacts-form') }}", $(this).serialize(), function (){
                _this.find("button").attr("disabled", false);
                _this[0].reset();

                $("#contactFrmModal").modal("show");
            });
        });

        $(".select-sort").on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            e.stopImmediatePropagation();

            let sort_val = $(this).val();

            addQueryParamsToUrl({'sort': sort_val});

            @if(Route::currentRouteName() != 'catalog.get-catalog')
                addQueryParamsToUrl({'product_type': '{{ $products[0]->product_type ?? '' }}'});
            @endif

            window.location.reload();
        });
    });
</script>

<script>
    $(document).ready(function() {
        var menuWidth = $("#category-box").width();
        $(".img-categories-box").width(menuWidth);
    });
</script>

@if(app()->environment() != 'local')
    <script>
        $(function () {
            setTimeout(function () {
                var script = document.createElement('script');
                script.src = "//code.jivo.ru/widget/WjaAvQhHuY";
                script.setAttribute('async', '')
                document.getElementsByTagName('head')[0].appendChild(script);
            }, 4000);
        });
    </script>
@endif

<!-- Mobile menu & filter -->
<script>
    $(document).ready(function() {
        const sidebar = document.querySelector('#sidebarHeader1');
        const targetButton = document.querySelector('#buttonCloseHeader');

        if (sidebar && targetButton) {
            sidebar.addEventListener('click', function(event) {
                if (event.target === sidebar) {
                    targetButton.click();
                }
            });
        }
    });
</script>

<script>
    $(document).ready(function() {
        const sidebarFilter = document.querySelector('#sidebarContent1');
        const targetButtonFilter = document.querySelector('#buttonCloseContent');

        if (sidebarFilter && targetButtonFilter) {
            sidebarFilter.addEventListener('click', function(event) {
                if (event.target === sidebarFilter) {
                    targetButtonFilter.click();
                }
            });
        }
    });
</script>

<script>
    $(document).ready(function() {
        $(".u-header-collapse__nav-pointer").click(function() {
            $(".navbar-nav__footer, .u-sidebar__content").toggleClass("u-header-collapse__open-subnav");
            $("#mCSB_1_scrollbar_vertical").toggleClass("d-block");
            $("#mCSB_1").toggleClass("mobile-menu__category-open-block");
            $(".space-block").toggleClass("space-block-active");
            $(".u-header-sidebar__content").removeClass("filter-mob-menu");
        });

        $("#sidebarNavToggler1").click(function() {
            $(".u-header-sidebar__content").addClass("filter-mob-menu");
        });

        $("#buttonCloseContent").click(function() {
            $(".u-header-sidebar__content").removeClass("filter-mob-menu");
        });
    });
</script>

<script>
    $(document).ready(function() {
        const sidebarHeader = document.querySelector('#sidebarHeader1');
        const sidebarContent = document.querySelector('#sidebarContent1');

        if (sidebarHeader && sidebarContent) {
            function checkSidebarClasses() {
                if ($('#sidebarHeader1').hasClass('u-unfold--hidden') && $('#sidebarContent1').hasClass('u-unfold--hidden')) {
                    $('.body').removeClass('u-unfold-opened');
                }
            }
        } else if (sidebarHeader || sidebarContent) {
            function checkSidebarClasses() {
                if ($('#sidebarHeader1').hasClass('u-unfold--hidden') || $('#sidebarContent1').hasClass('u-unfold--hidden')) {
                    $('.body').removeClass('u-unfold-opened');
                }
            }
        }

        if (sidebarHeader || sidebarContent) {
            const observer = new MutationObserver(checkSidebarClasses);

            observer.observe(document, { subtree: true, attributes: true, attributeFilter: ['class'] });
        }
    });
</script>

<!--Cookies-->
<script>
    function setCookie(name, value) {
        document.cookie = name + "=" + (value || "") + "; path=/";
    }

    function hasCookie(name) {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            while (cookie.charAt(0) == ' ') {
                cookie = cookie.substring(1, cookie.length);
            }
            if (cookie.indexOf(name + "=") == 0) {
                return true;
            }
        }
        return false;
    }

    if (!hasCookie('d-flex-set')) {
        $('.cookies').removeClass('d-none');
        $('.cookies').addClass('d-flex');

        $(document).ready(function(){
            $(".button-good").click(function(){
                $(".cookies").removeClass("d-flex");
                $(".cookies").addClass("d-none");
                setCookie('d-flex-set', 'true');
            });
        });
    }
</script>

@yield('schema')
</body>
</html>
