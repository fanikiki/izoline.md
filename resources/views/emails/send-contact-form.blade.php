<h1>@lang('common.mail_new_message')</h1>
<br>
<b>@lang('common.name'):</b> {{ $data['name'] ?? '' }}<br/>
<b>E-mail:</b> {{ $data['email'] ?? '' }}<br/>
<b>@lang('common.phone'):</b> {{ $data['phone'] ?? '' }}<br/>
<b>@lang('common.message'):</b> {{ $data['message'] ?? '' }}<br/>


