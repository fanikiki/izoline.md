@extends('body')
@section('centerbox')
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a
                                href="{{ route('index') }}">@lang('common.home')</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active"
                            aria-current="page">@lang('common.documentation')</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="mb-10 text-center">
            <h1>@lang('common.documentation')</h1>
        </div>

        <div class="col-lg-12 mb-10">
            @if(!empty($docs))
                @foreach($docs as $doc)
                    <!-- Basics Accordion -->
                    <div id="accordion_{{$doc->id}}" class="about-accordion">
                        <!-- Card -->
                        <div class="card mb-4 border-color-4 rounded-0">
                            <div class="card-header card-collapse border-color-4" id="heading_{{$doc->id}}">
                                <h5 class="mb-0">
                                    <button type="button"
                                            class="btn btn-link btn-block flex-horizontal-center card-btn p-0 font-size-18"
                                            data-toggle="collapse"
                                            data-target="#basicsCollapse_{{ $doc->id }}"
                                            aria-expanded="false"
                                            aria-controls="basicsCollapse_{{ $doc->id }}">
                                            <span class="border border-color-5 rounded font-size-12 mr-5">
                                                <i class="fas fa-plus"></i>
                                                <i class="fas fa-minus"></i>
                                            </span>
                                        {{ $doc->name }}
                                    </button>
                                </h5>
                            </div>

                            <div id="basicsCollapse_{{ $doc->id }}" class="collapse"
                                 aria-labelledby="heading_{{$doc->id}}"
                                 data-parent="#accordion_{{$doc->id}}">
                                <div class="card-body">
                                    @if($doc->children->isNotEmpty())
                                        @foreach($doc->children as $child)
                                            <!-- Inner Accordion -->
                                            <div id="innerAccordion_{{ $child->id }}" class="about-accordion">
                                                <!-- Inner Card -->
                                                <div class="card mb-4 border-color-4 rounded-0">
                                                    <div class="card-header card-collapse border-color-4"
                                                         id="innerHeadingOne">
                                                        <h5 class="mb-0">
                                                            <button type="button"
                                                                    class="btn btn-link btn-block flex-horizontal-center card-btn p-0 font-size-18"
                                                                    data-toggle="collapse"
                                                                    data-target="#innerCollapse_{{ $child->id }}"
                                                                    aria-expanded="false"
                                                                    aria-controls="innerCollapseOne">
                                                            <span
                                                                class="border border-color-5 rounded font-size-12 mr-5">
                                                                <i class="fas fa-plus"></i>
                                                                <i class="fas fa-minus"></i>
                                                            </span>
                                                                {{ $child->name }}
                                                            </button>
                                                        </h5>
                                                    </div>

                                                    @if($child->children->isNotEmpty())
                                                        @foreach($child->children as $child2)
                                                            <div id="innerCollapse_{{ $child->id }}" class="collapse"
                                                                 aria-labelledby="heading_{{$doc->id}}"
                                                                 data-parent="#innerAccordion_{{ $child->id }}">
                                                                <div class="card-body">
                                                                    <div id="innerAccordion_{{ $child2->id }}"
                                                                         class="about-accordion">
                                                                        <!-- Inner Card -->
                                                                        <div class="card mb-4 border-color-4 rounded-0">
                                                                            @if($child2->children->isNotEmpty())
                                                                                @foreach($child2->children as $child3)
                                                                                    <div
                                                                                        class="card-header card-collapse border-color-4"
                                                                                        id="innerHeading_{{ $child2->id }}">
                                                                                        <h5 class="mb-0">
                                                                                            <button type="button"
                                                                                                    class="btn btn-link btn-block flex-horizontal-center card-btn p-0 font-size-18 collapsed"
                                                                                                    data-toggle="collapse"
                                                                                                    data-target="#innerCollapse_{{ $child3->id }}"
                                                                                                    aria-expanded="false"
                                                                                                    aria-controls="innerCollapseOne">
                                                                                        <span
                                                                                            class="border border-color-5 rounded font-size-12 mr-5"><i
                                                                                                class="fas fa-plus"></i><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                                {{ $child2->name }}
                                                                                            </button>
                                                                                        </h5>
                                                                                    </div>

                                                                                    <div
                                                                                        id="innerCollapse_{{ $child3->id }}"
                                                                                        class="collapse"
                                                                                        data-parent="#innerAccordion_{{ $child2->id }}">
                                                                                        <div class="card-body">
                                                                                            <div
                                                                                                id="innerAccordion_{{ $child3->id }}"
                                                                                                class="about-accordion">
                                                                                                <!-- Inner Card -->
                                                                                                <div
                                                                                                    class="card mb-4 border-color-4 rounded-0">
                                                                                                    <div
                                                                                                        class="card-header card-collapse border-color-4"
                                                                                                        id="innerHeading_{{ $child3->id }}">
                                                                                                        <h5 class="mb-0">
                                                                                                            <button
                                                                                                                type="button"
                                                                                                                class="btn btn-link btn-block flex-horizontal-center card-btn p-0 font-size-18 collapsed"
                                                                                                                data-toggle="collapse"
                                                                                                                data-target="#innerCollapse_pdf_{{ $child3->id }}"
                                                                                                                aria-expanded="false"
                                                                                                                aria-controls="innerCollapseOne">
                                                                                                                <span
                                                                                                                    class="border border-color-5 rounded font-size-12 mr-5"><i
                                                                                                                        class="fas fa-plus"></i><i
                                                                                                                        class="fas fa-minus"></i>
                                                                                                                </span>
                                                                                                                {{ $child3->name }}
                                                                                                            </button>
                                                                                                        </h5>
                                                                                                    </div>

                                                                                                    <div id="innerCollapse_pdf_{{ $child3->id }}" class="collapse"
                                                                                                         data-parent="#innerAccordion_{{ $child3->id }}">
                                                                                                        <div class="card-body">
                                                                                                            <ul>
                                                                                                                @foreach($child3->params as $param)
                                                                                                                    <li>
                                                                                                                        <a target="_blank"
                                                                                                                           href="uploaded/docs/{{ $child3->id }}/{{ $param['pdf_file'] }}">
                                                                                                                            <i class="fa fa-file-pdf"></i> {{ $param['pdf_name'] }}
                                                                                                                        </a>
                                                                                                                    </li>
                                                                                                                @endforeach
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endforeach
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        @if($child->params)
                                                            <div id="innerCollapse_{{ $child->id }}" class="collapse"
                                                                 aria-labelledby="heading_{{$doc->id}}"
                                                                 data-parent="#innerAccordion_{{ $child->id }}">
                                                                <div class="card-body">
                                                                    <ul>
                                                                        @foreach($child->params as $param)
                                                                            <li>
                                                                                <a target="_blank"
                                                                                   href="uploaded/docs/{{ $child->id }}/{{ $param['pdf_file'] }}">
                                                                                    <i class="fa fa-file-pdf"></i> {{ $param['pdf_name'] }}
                                                                                </a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                    <!-- End Basics Accordion -->
                @endforeach
            @endif
        </div>
    </div>
@endsection

@section('styles')
    <style>
        li a {
            font-size: 18px;
            color: black;
        }
    </style>
@endsection
