@extends('admin.body')
@section('title', 'Оповещения')

@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a href="admin/users">Пользователи</a></small>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> События всех
                пользователей</small>
        </h1>
    </div>

    <div class="row celebrations-filters">
        <div class="col-sm-2 col-xs-12">
            <div class="form-group">
                <div class="col-xs-12 no-padding">
                    <select name="month" class="form-control" id="filter-month">
                        <option value="">--- все месяцы ---</option>
                        @foreach($months as $k => $month)
                            <option value="{{ $k }}">{{ $month }}</option>
                        @endforeach
                    </select>
                </div>
            </div><!-- /.form-group -->
        </div>
        <div class="col-sm-2 col-xs-12">
            <div class="form-group">
                <div class="col-xs-12 no-padding">
                    <select name="year" class="form-control" id="filter-year">
                        <option value="">--- все годы ---</option>
                        @foreach($years as $year)
                            <option value="{{ $year }}">{{ $year }}</option>
                        @endforeach
                    </select>
                </div>
            </div><!-- /.form-group -->
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div class="table-header"> Список записей</div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover table-responsive dataTable">
                <thead>
                <tr>
                    <th align="center">ID</th>
                    <th align="center" width="10%">Пользователь</th>
                    <th align="center">Email</th>
                    <th align="center">Телефон</th>
                    <th align="center" width="10%">Дата</th>
                    <th align="center" width="1%">Описание</th>
                    <th align="center" class="nosort" width="1%">Оповещения</th>
                    <th align="center" width="8%">МК</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@endsection

@section('styles')
    {{HTML::style('ace/assets/css/dataTables.responsive.css')}}
@endsection

@section('scripts')

    <script>
        var columns = [
            {data : 'id', 'className': 'center'},
            {
                data : 'user_name',
                "render": {
                    "display": function (data, type, full) {
                        return `<a href='admin/users/${full.user_id}/edit'>${data}</a>`;
                    }
                },
                'className': 'center',
                'searchable': true
            },
            {data : 'user_email', 'className': 'center'},
            {data : 'user_phone', 'className': 'center'},
            {
                data : 'celebration_date_sort',
                "render": {
                    "display": function (data, type, full) {
                        return `<i class="ace-icon fa fa-calendar-o"></i> ${full.celebration_date}`;
                    }
                },
                'className': 'center',
                'searchable': true
            },
            {data : 'celebration_description', 'className': 'center', 'sortable': false},
            {data : 'alert_dates', 'className': 'center', 'searchable': true, 'sortable': false},
            {
                data : 'mk',
                "render": {
                    "display": function (data, type, full) {
                        return data == 1 ? `<i class="ace-icon fa fa-check green"></i>` : `<i class="ace-icon fa fa-times red"></i>`;
                    }
                },
                'className': 'center',
                'searchable': true,
                'sortable': true
            },
        ];
    </script>

    @include('admin.partials.datatable-init-ajax', ['ajax' => 'admin/get-celebrations'])

    <script>
        $("#filter-month").change(function () {
            let params = {};
            params['month'] = $(this).val();
            params['year'] = $("select#filter-year").val();

            $("#dynamic-table").data("dt_params", params);
            $("#dynamic-table").DataTable().ajax.reload();
        });

        $("#filter-year").change(function () {
            let params = {};
            params['month'] = $("select#filter-month").val();
            params['year'] = $(this).val();

            $("#dynamic-table").data("dt_params", params);
            $("#dynamic-table").DataTable().ajax.reload();
        });

        $('#dynamic-table').on('click', '.remove-notification', function() {
            if (!confirm("Удалить оповещение?")) {
                return false;
            }
            var id      =  $(this).data('id');
            var $thisdiv = $(this);

            $.post("admin/remove-user-notification", {'id': id}, function() {
                $thisdiv.parent().remove();
            });
        });
    </script>
@endsection
