@extends('admin.body')
@section('title', 'Редактирование пользователя')
@section('centerbox')

    @if(isset($data))
        @include('admin.users.add_phone_modal')
        @include('admin.users.add_address_modal')
        @include('admin.users.add_event_modal')
        @include('admin.users.add_notification_modal')
        @include('admin.orders.order_history_modal')
    @endif

    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a
                    href="{{ URL::to('admin/users') }}">Пользователи</a></small>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Редактирование
                пользователя @isset($data)
                    №{{$data->id}}
                @endisset</small>
        </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/users', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/users/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-3">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive"><i
                        class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить
                </button>
            </div>
            @if(isset($data))
                <div class="col-sm-3">
                    <button type="button" class="btn btn-purple btn-block btn-responsive btnAddPhone"><i
                            class="ace-icon fa fa-phone bigger-120"></i> Добавить телефон
                    </button>
                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn btn-purple btn-block btn-responsive btnAddAddress"><i
                            class="ace-icon fa fa-building bigger-120"></i> Добавить адрес
                    </button>
                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn btn-warning btn-block btn-responsive btnOrderHistory"><i
                            class="ace-icon fa fa-list bigger-120"></i> История заказов
                    </button>
                </div>
            @endif
        </div><!-- /.row -->

        @if(isset($data))
            <div class="row">
                <div class="col-sm-3 margin-top-15">
                    <a href="{{route('admin.login-as-user', $data->id)}}" class="btn btn-warning btn-block btn-responsive"><i
                            class="ace-icon fa fa-key bigger-120"></i> Войти под пользователем
                    </a>
                </div>
            </div>
        @endif
    </div><!-- /.form-actions -->

    <div class="tabbable">
        <ul class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="@if(!request()->has('show_celebrations'))active @endif">
                <a href="#common" data-toggle="tab">Основные данные</a>
            </li>
            @if(isset($data))
                <li class="@if(request()->has('show_celebrations'))active @endif">
                    <a href="#events" data-toggle="tab"><i class="ace-icon fa fa-calendar"></i> События</a>
                </li>
            @endif
        </ul>

        <div class="tab-content">
            <div class="tab-pane @if(!request()->has('show_celebrations'))in active @endif" id="common">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label('name', 'Имя', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('name', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('phone', 'Телефон', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('phone', (isset($data) ? $data->phone : old('phone')), array('class' =>
                                'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('email', 'Email', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('email', (isset($data->email) ? $data->email : old('email')), array('class' =>
                                'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('open_password', 'Пароль', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('open_password', (isset($data->open_password) ? $data->open_password : old
                                ('open_password')), array('class' => 'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>

                        @if(isset($data) && !empty($data->getOtherPhones()))
                            @foreach($data->getOtherPhones() as $k => $item)
                                <div class="other-phone-container">
                                    <div class="form-group">
                                        {{ Form::label('phone', 'Доп. телефон ' . $loop->iteration, ['class'=>'col-sm-3 control-label no-padding-right']) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('other_phones[phone][]', (isset($item) ? $item['phone'] : old('phone')), array('class' =>
                                            'col-sm-11 col-xs-12')) }}
                                            <div style="display: inline-block;">
                                                <a href="javascript:void(0);" class="deletePhone"
                                                   data-phone="{{$item['phone']}}"><i
                                                        class="ace-icon fa fa-trash red"></i> Удалить</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('comment', 'Комментарий ' . $loop->iteration, ['class'=>'col-sm-3 control-label no-padding-right']) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('other_phones[comment][]', (isset($item) ? $item['comment'] : old('comment')), array('class' =>
                                            'col-sm-11 col-xs-12')) }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        @if(isset($data) && !empty($data->getAddresses()))
                            @foreach($data->getAddresses() as $k => $item)
                                <div class="form-group">
                                    {{ Form::label('phone', 'Адрес ' . $loop->iteration, ['class'=>'col-sm-3 control-label no-padding-right']) }}
                                    <div class="col-sm-9">
                                        {{ Form::text('addresses[address][]', (isset($item) ? $item['address'] : old('phone')), array('class' =>
                                        'col-sm-11 col-xs-12')) }}
                                        <div style="display: inline-block;">
                                            <a href="javascript:void(0);" class="deleteAddress"
                                               data-address="{{$item['address']}}"><i
                                                    class="ace-icon fa fa-trash red"></i> Удалить</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="checkbox" name="masterclass_agree" value="1" @if(isset($data) && $data->masterclass_agree) checked @endif> Интересуется мастер-классами
                            </div>
                        </div>
                    </div><!-- /.col-sm-6 -->
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label('rights', 'Роль', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                <select name="rights" class="col-sm-9">
                                    <option value="0" @if(isset($data->rights) && $data->rights == 0) selected @endif>Пользователь</option>
                                    <option value="2" @if(isset($data->rights) && $data->rights == 2) selected @endif>Партнер</option>
                                    <option value="1" @if(isset($data->rights) && $data->rights == 1) selected @endif>Администратор</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('telegram', 'Telegram', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('telegram', (isset($data->telegram) ? $data->telegram : old('telegram')), array('class' =>
                                'col-sm-11 col-xs-12')) }}

                                @if(isset($data) && !empty($data->telegram))
                                    <a style="display: inline-block; margin-top: 5px" target="_blank"
                                       href="{{ $data->getTelegramLink() }}"><i class="ace-icon fa fa-telegram bigger-130"></i>
                                        Написать в Telegram
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('viber', 'Viber', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('viber', (isset($data->viber) ? $data->viber : old('viber')), array('class' =>
                                'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('facebook', 'Facebook', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('facebook', (isset($data->facebook) ? $data->facebook : old('facebook')), array('class' =>
                                'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('instagram', 'Instagram', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('instagram', (isset($data->instagram) ? $data->instagram : old('instagram')), array('class' =>
                                'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                    </div><!-- /.col-sm-6 -->
                </div><!-- /.row -->
            </div>
            <div class="tab-pane @if(request()->has('show_celebrations'))in active @endif" id="events">
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-purple btn-block btn-responsive btnAddEvent"><i
                                class="ace-icon fa fa-plus-square"></i> Добавить событие
                        </button>
                    </div>
                </div>

                <div class="row">
                    @if(isset($data) && !empty($data->celebrations))
                        @foreach($data->celebrations as $celebration)
                            <div class="col-sm-6">
                                <input type="hidden" name="celebrations[id][]" value="{{$celebration->id}}">

                                <div class="widget-box">
                                    <div class="widget-header">
                                        <h4 class="widget-title">Событие №{{ $loop->iteration }}</h4>
                                        <div class="pull-right" style="margin: 8px;">
                                            <a href="javascript:void(0);" class="deleteEvent"
                                               data-id="{{$celebration->id}}"><i
                                                    class="ace-icon fa fa-trash red"></i> Удалить</a>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        {{ Form::label('date', 'Дата', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                                                        <div class="col-sm-9">
                                                            <input type="text" name="celebrations[date][]"
                                                                   class="col-sm-11 col-xs-12 date-picker celebration-date"
                                                                   data-date-format="dd.mm.yyyy"
                                                                   value="{{ !empty($celebration) ? date('d.m.Y', strtotime($celebration->date)) : '' }}"/>
                                                            <button data-celebration-id="{{ $celebration->id }}" type="button" class="btn btn-success btn-mini btnAddNotification"><i
                                                                    class="ace-icon fa fa-calendar"></i> Оповещения
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        {{ Form::label('description', 'Описание', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                                                        <div class="col-sm-9">
                                                            {{ Form::textarea('celebrations[description][]', (!empty($celebration) ? $celebration['description'] : old('description')), array('class' =>
                                                            'col-sm-11 col-xs-12', 'rows' => 6)) }}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        {{ Form::label('repeat', 'Повторяется ли событие', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                                                        <div class="col-sm-9">
                                                            {{ Form::select('celebrations[repeat][]', config('custom.events_repeat'), $celebration['repeat'], ['class'=>'celebration-repeat col-sm-11 col-xs-12']) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.col-sm-6 -->
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection
@section('styles')
    <style>
        input[type=checkbox].ace.input-lg + .lbl::before {
            margin-right: 7px;
        }
    </style>
@endsection

@section('scripts')
    @include('admin.partials.datepicker')

    <script>
        $(function () {
            $(".btnAddPhone").click(function () {
                $("#addPhoneModal").modal('show');
            });

            $(".btnAddAddress").click(function () {
                $("#addAddressModal").modal('show');
            });

            $(".btnAddEvent").click(function () {
                $("#addEventModal").modal('show');
            });

            $(".btnOrderHistory").click(function () {
                $("#order_history_modal").modal('show');
            });

            $(".btnAddNotification").click(function () {
                let celebration_id = $(this).data('celebration-id');
                let remind_inputs = $("#addNotificationModal").find('.remind-input');
                remind_inputs.prop('checked', false);

                $("#addNotificationModal").find('input[name=celebration_id]').val(celebration_id);

                $.get('{{ route('admin.get-celebration-notifications') }}', {'celebration_id': celebration_id}, function (response){
                    $(response).each(function (i, n){
                        $(remind_inputs).each(function (k, r){
                            if($(r).val() == n['config_events_remind']){
                                $(r).prop('checked', true);
                            }
                        });
                    });

                    $("#addNotificationModal").modal('show');
                });
            });

            @if(isset($data))
                $(".deletePhone").click(function () {

                    if (confirm('Вы действительно хотите удалить доп. телефон?')) {
                        let phone = $(this).data('phone');

                        $.post('{{ route('admin.remove-user-phone') }}', {
                            'user_id': '{{$data->id}}',
                            'phone': phone
                        }, function () {
                            window.location.reload();
                        });
                    }
                });

                $(".deleteAddress").click(function () {

                    if (confirm('Вы действительно хотите удалить адрес?')) {
                        let address = $(this).data('address');

                        $.post('{{ route('admin.remove-user-address') }}', {
                            'user_id': '{{$data->id}}',
                            'address': address
                        }, function () {
                            window.location.reload();
                        });
                    }
                });

                $(".deleteEvent").click(function () {

                    if (confirm('Вы действительно хотите удалить событие?')) {
                        let id = $(this).data('id');

                        $.post('{{ route('admin.remove-user-event') }}', {
                            'id': id
                        }, function () {
                            window.location.reload();
                        });
                    }
                })
            @endif
        });
    </script>
@endsection
