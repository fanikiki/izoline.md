@extends('admin.body')
@section('title', 'Редактирование пользователя')
@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a href="{{ URL::to('admin/users') }}">Пользователи</a></small>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Редактирование пользователя @isset($data) №{{$data->id}} @endisset</small>
        </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/users', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/users/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            @if(isset($data) && Auth::user()->email == 'support@xsort.md')
                <div class="col-sm-2">
                    <a target="_blank" href="{{route('admin.login-as', [$data->id])}}" class="btn btn-info"><i
                            class="ace-icon fa fa-key"></i> Войти под пользователем</a>
                </div>
            @endif
        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="tabbable">
        <ul class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#common" data-toggle="tab">Основное</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane in active" id="common">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label('name', 'Имя', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('name', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('lastname', 'Фамилия', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('lastname', (isset($data->lastname) ? $data->lastname : old('lastname')), array('class' => 'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('phone', 'Телефон', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('phone', (isset($data) ? $data->phone : old('phone')), array('class' =>
                                'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('params[company]', 'Фирма', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('params[company]', (isset($data->params['company']) ? $data->params['company'] : old('params[company]')), array('class' =>
                                'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('email', 'Email', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('email', (isset($data->email) ? $data->email : old('email')), array('class' =>
                                'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('open_password', 'Пароль', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                {{ Form::text('open_password', (isset($data->open_password) ? $data->open_password : old
                                ('open_password')), array('class' => 'col-sm-11 col-xs-12')) }}
                            </div>
                        </div>
                    </div><!-- /.col-sm-6 -->
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label('rights', 'Роль', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                            <div class="col-sm-9">
                                <select name="rights" class="col-sm-9">
                                    <option value="0" @if(isset($data->rights) && $data->rights == 0) selected @endif>Пользователь</option>
                                    <option value="1" @if(isset($data->rights) && $data->rights == 1) selected @endif>Администратор</option>
                                </select>
                            </div>
                        </div>
                    </div><!-- /.col-sm-6 -->
                </div><!-- /.row -->
            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection
@section('styles')
    <style>
        input[type=checkbox].ace.input-lg+.lbl::before {
            margin-right: 7px;
        }
    </style>
@endsection
