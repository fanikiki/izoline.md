{!! HTML::script('//cdn.ckeditor.com/4.7.3/standard/ckeditor.js') !!}
<script>
    $(document).ready(function(){
        var FORM_ID = '{{ $form_id ?? '' }}';
        if (FORM_ID != "") FORM_ID = "#" + FORM_ID + " ";
        $(FORM_ID + '.ckeditor').each(function(){
            CKEDITOR.replace( $(this).attr('id'), {
                language: 'ru',
                filebrowserImageBrowseUrl:  '/filemanager?type=Image',
                filebrowserBrowseUrl:       '/filemanager?type=File',
                allowedContent: true
            });
        });
    });
</script>
