@php
    $statuses = config('custom.order_status');

    $class = "";
    switch ($item->status){
        case \App\Models\Order::STATUS_NEW:
            $class = "label-success";
            break;
        case \App\Models\Order::STATUS_DELIVERING:
            $class = "label-warning";
            break;
        case \App\Models\Order::STATUS_DELIVERED:
            $class = "label-info";
            break;
        default:
            break;
    }
@endphp

<label class="label {{ $class }}">{{ $statuses[$item->status] }}</label>
