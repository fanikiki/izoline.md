{!! HTML::style('ace/assets/css/datepicker.css') !!}
{!! HTML::script('ace/assets/js/date-time/bootstrap-datepicker.js') !!}
<script type="text/javascript">
    jQuery(function($) {
        $('.date-picker').each(function() {
            if(this.type !== 'date') {   //if browser doesn't support "date" input
                var $this = $(this);
                var dp = $(this).datepicker({
                    weekStart: 1,
                    autoclose:true,
                    language: 'ru'
                });
                $this.closest('.input-group').find('.input-group-addon').click(function(e) {
                    e.preventDefault();
                    $this.click();
                    dp.show();
                    $this.focus();
                });
            }
        });
    })
</script>
