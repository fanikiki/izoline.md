@extends('admin.common.list',
    [
        'title'       =>  'Блог',
        'desc'        =>  'Список статей сайта',
        'model'       =>  'news',
        'fields'      =>  ['name' => 'Наименование', 'created_at' => 'Создан'],
        'data'        =>  $data
    ]
)



