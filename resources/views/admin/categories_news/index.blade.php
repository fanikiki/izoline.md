@extends('admin.body')
@section('title', 'Категории')

@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Категории (блог)</small>
        </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <a class="btn  btn-success" href="{{ URL::to('admin/categories_news/create') }}">
                <i class="ace-icon fa fa-plus-square-o  bigger-120"></i>
                Создать
            </a>
            <div class="clearfix"><div class="pull-right tableTools-container"></div></div>
            <div class="table-header"> Список категорий блога </div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover table-responsive dataTable">
                <thead>
                <tr>
                    <th align="center">ID</th>
                    <th align="center">Создан</th>
                    <th align="center">Название</th>
                    <th align="center"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody bgcolor="white"></tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@endsection

@section('styles')
    {{HTML::style('ace/assets/css/dataTables.responsive.css')}}
@endsection

@section('scripts')

    <script>
        var data = [];
        var columns = [
            { data : 'id', className : 'center'},
            { data : 'created_at', className : 'center' },
            { data : 'name', className : 'center'},
            { data : 'actions', className : 'center' }
        ];
    </script>

    @include('admin.partials.datatable-init-ajax', ['ajax' => 'admin/get-categories-news'])

@endsection
