@extends('admin.body')
@section('title', 'Параметры')


@section('centerbox')
<div class="page-header">
    <h1> <a href="{{ URL::to('admin/parameters') }}">Параметры</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование </small> </h1>
</div>

@include('admin.partials.errors')

@if(!isset($data))
{{ Form::open(['url' => 'admin/parameters', 'class' => 'form-horizontal']) }}
@else
{{ Form::open(['url' => 'admin/parameters/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
@endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">
                        @if (isset($data))
                        <div class="btn btn-link">
                            <i class="ace-icon fa fa- bigger-120 green"></i>
                            ID: {{ $data->id }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="checkbox">
                    <label class="block">
                        <input name="is_filter" type="checkbox" class="ace input-lg" @if(isset($data) && $data->is_filter == 1)checked="checked" @endif value="1">
                        <span class="lbl bigger-120"> Показывать в фильтре</span>
                    </label>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->


    <div class="row">
        <div class="col-sm-6">
            <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                    <li class="active">
                        <a data-toggle="tab" href="#name_ru" aria-expanded="false">RU</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#name_ro" aria-expanded="false">RO</a>
                    </li>
                    <div class="center">
                        <span class="label label-xlg label-purple">Наименование параметра</span>
                    </div>
                </ul>
                <div class="tab-content">
                    <div id="name_ru" class="tab-pane in active">
                        {{ Form::textarea('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                    <div id="name_ro" class="tab-pane">
                        {{ Form::textarea('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('sort', 'Сортировка', ['class'=>'showTip L_sort col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('sort', (isset($data->sort) ? $data->sort : old('sort')), array('class' => 'showTip L_sort col-sm-11 col-xs-12 showTip L1')) }}
                </div>
            </div>
            <div class="form-group hide">
                {{ Form::label('type', 'Тип', ['class'=>'col-sm-3 control-label no-padding-right showTip L2']) }}
                <div class="col-sm-9">
                    @if (!isset($data))
                        {{ Form::select('type', config('custom.parameter_types'), 0, ['class' => 'form-control col-sm-11 col-xs-12 showTip L2']) }}
                    @else
                        {{ Form::select('type', config('custom.parameter_types'), $data->type, ['class' => 'form-control col-sm-11 col-xs-12 showTip L2', 'disabled' => 'disabled']) }}
                    @endif
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->

    <div class="space"></div>

    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#values" data-toggle="tab">Значения</a>
            </li>
        </ul>

        <div class="tab-content">
             <div class="tab-pane active" id="values">
                 <div class="tab-content">
                     <ul id="sortable">
                         @if (isset($data) && $data->values->isNotEmpty())
                             @foreach($data->values as $item)
                                 @include('admin.parameters.one_value')
                             @endforeach
                         @else
                             @include('admin.parameters.one_value')
                         @endif
                     </ul>
                     <a href="javascript:addItem();" class="btn btn-sm btn-info showTip L3">
                         <i class="ace-icon fa fa-plus-circle bigger-110"></i>
                         Добавить
                     </a>
                 </div>
             </div>
        </div>
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>
{{ Form::close() }}
@endsection


@section('scripts')
    {!! HTML::script('ace/assets/js/jquery-ui.js') !!}
    {!! HTML::script('ace/dist/js/bootstrap-colorpicker.min.js') !!}


    <script type="text/javascript">

dw_Tooltip.defaultProps = {
}
dw_Tooltip.content_vars = {
    L3: 'Для добавления <b>нового значения</b> в список значений, нажмите кнопку <b>Добавить</b>, затем укажите <b>название значения</b> в появившемся поле. При заполнении всех полей нажмите кнопку <b>Сохранить</b>',
    L4: 'На данной странице можно <b>создать/отредактировать</b> тип параметра. <br><br>Созданный параметр можно будет использвать на странице редактирования товара (типоразмер шин или диска, аксессуары), например: сезонность, индекс нагрузки, шипы и др.  <br><br> - Если выбрать тип <b>"текст"</b> - значит значение этого параметра нужно будет ввести вручную  (например: индекс скорости) <br>- Если выбрать тип <b>"список"</b> - надо предварительно создать варианты этого списка (пример: сезонность - зима, лето, весна), а затем на странице редактирования товара можно будет выбрать один из этих вариантов',
    L_sort: `Диапазоны для сортировки параметров:
            <br><br>
            <b>100</b> - фасовка и упаковка;<br>
            <b>200</b> - предназначение;<br>
            <b>300</b> - внешние особенности и материал;<br>
            <b>400</b> - горючесть;<br>
            <b>500</b> - физические и химические характеристики;<br>
            <b>600</b> - страна производитель.`
}
</script>

    <script>
        $(document).ready(function() {
            $('#sortable').sortable();

            if ($('select[name=type]').val() == 1) {
                $('.tabbable').removeClass('hide');
            }

            initDelete();
        });

        $('select[name=type]').on('change', function() {
            if ( $('select[name=type]').val() == 1 ){
                $('.tabbable').removeClass('hide');
            }else{
                $('.tabbable').addClass('hide');
            }
        });

        function addItem() {
            $clone = $('#sortable li:last-child').clone();
            $clone.find('input').val("");
            $('#sortable').append($clone);

            initDelete();
        }

        function initDelete() {
            $('#sortable li i.delete').click(function() {
                var $this = $(this);
                var param_value_id = $(this).closest('li').find('input[type=hidden]').val();
                if ($(this).closest('li').find('input[type=hidden]').val() == 0) {
                    $(this).closest('li').remove();
                } else {
                    if (confirm("Значение параметра будет удалено для всех товаров!!! Продолжить?")) {
                        $.get('admin/json/remove-parameter-value',
                            {
                                'param_value_id': param_value_id
                            },
                            function (response) {
                                toastr.success('Значение параметра удалено для всех товаров');
                                $this.closest('li').remove();
                            }
                        );
                    }
                }
            });
        }
    </script>
@endsection


@section('styles')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />

    <style>
        #sortable {
            border: 1px solid #eee;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            margin-right: 10px;
            width: 100%;
        }

        #sortable li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            border: 1px solid #ccc;
            background-color: #cce2c1;
        }
    </style>
    <style>
    div#tipDiv {
    font-size:12px; line-height:1.2; letter-spacing: .2px;
    color:#000; background-color:white; padding: 2px;

    padding:4px;
    width:320px;
    box-shadow: 0 1px 5px 0 rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.12);
}
</style>
@endsection
