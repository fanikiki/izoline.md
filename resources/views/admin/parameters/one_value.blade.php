<li class="ui-state-default">
    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
    <input type="hidden" name="values[id][]" value="{{ $item->id ?? 0 }}" />
    <input type="text" name="values[ru][]" placeholder="значение RU" value="{{ $item->value ?? '' }}" />
    <input type="text" name="values[ro][]" placeholder="значение RO" value="{{ $item->value_ro ?? '' }}" />
    <i class="ace-icon fa fa-times-circle delete"></i>
</li>
