@if ($category->children->count() == 0)
    <option value="{{ $category->id }}" {{ in_array($category->id, $selected) ? 'selected' : '' }}>{{ $category->name }}</option>
@else
    <optgroup label="{{ $category->name }}">
        @foreach($category->children as $child)
            @include('admin.collections.partials.list-item', ['category' => $child])
        @endforeach
    </optgroup>
@endif
