@extends('admin.body')
@section('title', 'Стоимость доставки')
@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="{{ URL::to('admin/orders') }}">Заказы</a>
            <small class="multiple-paths"><i class="ace-icon fa fa-angle-double-right"></i> Стоимость доставки</small>
        </h1>
    </div>
    <form method="POST" action="{{route('admin.save-settings')}}" id="saveOrderSettingsFrm">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="form-group">
                        {{ Form::label('cream_chisinau', 'По Кишиневу кремовые', ['class'=>'col-sm-2 control-label no-padding-right']) }}
                        <div class="col-sm-4">
                            {{ Form::number('settings[cream_chisinau]', (isset($cream_chisinau) ? $cream_chisinau : old('cream_chisinau')), array('class' => 'col-sm-11 col-xs-12', 'min' => 1)) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('not_cream_chisinau', 'По Кишиневу не кремовые', ['class'=>'col-sm-2 control-label no-padding-right']) }}
                        <div class="col-sm-4">
                            {{ Form::number('settings[not_cream_chisinau]', (isset($not_cream_chisinau) ? $not_cream_chisinau : old('not_cream_chisinau')), array('class' => 'col-sm-11 col-xs-12', 'min' => 1)) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('not_cream_chisinau', 'Пригород Кишинева не кремовые', ['class'=>'col-sm-2 control-label no-padding-right']) }}
                        <div class="col-sm-4">
                            {{ Form::number('settings[not_cream_chisinau_suburb]', (isset($not_cream_chisinau_suburb) ? $not_cream_chisinau_suburb : old('not_cream_chisinau_suburb')), array('class' => 'col-sm-11 col-xs-12', 'min' => 1)) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('cream_chisinau', 'Пригород Кишинева кремовые', ['class'=>'col-sm-2 control-label no-padding-right']) }}
                        <div class="col-sm-4">
                            {{ Form::number('settings[cream_chisinau_suburb]', (isset($cream_chisinau_suburb) ? $cream_chisinau_suburb : old('cream_chisinau_suburb')), array('class' => 'col-sm-11 col-xs-12', 'min' => 1)) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('not_cream_moldova', 'По Молдове не кремовые', ['class'=>'col-sm-2 control-label no-padding-right']) }}
                        <div class="col-sm-4">
                            {{ Form::number('settings[not_cream_moldova]', (isset($not_cream_moldova) ? $not_cream_moldova : old('not_cream_moldova')), array('class' => 'col-sm-11 col-xs-12', 'min' => 1)) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-success btnSaveForm">Сохранить</button>
        </div>
    </form>
@endsection
