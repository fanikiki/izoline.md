@extends('admin.common.list',
    [
        'title'       =>  'Бренды',
        'desc'        =>  'Список брендов',
        'model'       =>  'manufacturers',
        'fields'      =>  ['name' => 'Наименование', 'slug' => 'Ссылка'],
        'data'        =>  $data
    ]
)
