@extends('admin.body')
@section('title', 'Коллекции')

@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Рекомендуемые товары на
                главной</small>
        </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/lists', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/lists/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group hide">
                {{ Form::label('name[ru]', 'Наименование', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">До 12 товаров.</div>
            <div class="widget-box">
                <div class="widget-header">
                    <h4>
                        Товары
                    </h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php $selected = (isset($data->params) ? $data->params : []); ?>
                                {{ Form::select('params[]', $products, $selected, ['class' => 'form-control chosencat', 'id' => 'products', 'multiple' => 'multiple', 'data-placeholder' => 'Выберите товары']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <div class="space"></div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}

@endsection

@section('scripts')
    {!! HTML::script('ace/dist/js/bootstrap-colorpicker.min.js') !!}

    @include('admin.partials.ckeditor')

    @include('admin.partials.datepicker')

    @include('admin.partials.chosen')
@append
