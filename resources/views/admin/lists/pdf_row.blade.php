<li class="ui-state-default">
    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
    <input type="text" name="params[pdf_name][]" placeholder="Название" value="{{ $param['pdf_name'] ?? '' }}"/>

    @if(!empty($param['pdf_file']))
        <div class="pdf-container">
            <a class="pdf-file" target="_blank" href="uploaded/docs/{{ $data->id }}/{{ $param['pdf_file'] }}"><i class="ace-icon fa fa-file"></i> {{ $param['pdf_file'] }}</a>
        </div>
    @else
        <div class="pdf-container">
            <input class="pdf-file" type="file" name="params[pdf_file][]" placeholder="Файл"/>
        </div>
    @endif
    <i data-pdf-file="{{ $param['pdf_file'] ?? null }}" class="ace-icon fa fa-times-circle delete"></i>
</li>
