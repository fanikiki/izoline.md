@extends('admin.body')
@section('title', 'Справочники')


@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a href="{{route('admin.lists.index')}}">Справочники</a></small>
            @if(isset($data))
                @foreach($data->getPaths() as $path)
                    <small class="@if(count($data->getPaths()) > 1)multiple-paths @endif">
                        @if(isset($path['slug']))
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <a href="{{$path['slug']}}">{{ $path['name'] }}</a>
                        @else
                            <i class="ace-icon fa fa-angle-double-right"></i> {{ $path['name'] }}
                        @endif
                    </small>
                @endforeach
            @else
                <small class="multiple-paths"><i class="ace-icon fa fa-angle-double-right"></i> Редактирование справочника</small>
            @endif
        </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/lists', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/lists/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    @if(isset($data))
        <input type="hidden" name="_id" value="{{$data->id}}">
    @endif
    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive"><i
                        class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить
                </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </div>

                            <div class="btn btn-link">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                Изменен: {{ $data->updated_at }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-12">
            <div class="tabbable">
                <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                    <li class="active">
                        <a href="#name_ru" data-toggle="tab" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a href="#name_ro" data-toggle="tab" aria-expanded="false">RO</a>
                    </li>

                    <div class="flex justify-content-around slider-link">
                        <div class="center"><span class="label label-xlg label-purple">Вопрос</span></div>
                    </div>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="name_ru">
                        {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name')), array('style'=>'width:100%')) }}
                    </div>
                    <div class="tab-pane" id="name_ro">
                        {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('style'=>'width:100%')) }}
                    </div>
                </div>
            </div>
            <div class="tabbable">
                <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                    <li class="active">
                        <a href="#description_ru" data-toggle="tab" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a href="#description_ro" data-toggle="tab" aria-expanded="false">RO</a>
                    </li>

                    <div class="flex justify-content-around slider-link">
                        <div class="center"><span class="label label-xlg label-purple">Ответ</span></div>
                    </div>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="description_ru">
                        {{ Form::textarea('description[ru]', (isset($data->description) ? $data->description : old('description')), array('class' => 'ckeditor form-control', 'id' => 'editor_description_ru', 'rows' => '5', 'style'=>'width:100%')) }}
                    </div>
                    <div class="tab-pane" id="description_ro">
                        {{ Form::textarea('description[ro]', (isset($data->description_ro) ? $data->description_ro : old('description_ro')), array('class' => 'ckeditor form-control', 'id' => 'editor_description_ro', 'rows' => '5', 'style'=>'width:100%')) }}
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group hide">
                {{ Form::label('parent', 'Родитель', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    @if(isset($parents))
                        @if(isset($parent_id))
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, $parent_id, ['class'=>'col-sm-11 col-xs-12']) }}
                        @else
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, '', ['class'=>'col-sm-11 col-xs-12']) }}
                        @endif
                    @endif
                </div>
            </div>
            <div class="form-group hide">
                <div class="tabbable">
                    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                        <li class="active">
                            <a data-toggle="tab" href="#slug_ru" aria-expanded="true">RU</a>
                        </li>
                        <div class="center">
                            <span class="label label-xlg label-purple">URL</span>
                        </div>
                    </ul>
                    <div class="tab-content">
                        <div id="slug_ru" class="tab-pane active">
                            {{ Form::textarea('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <hr>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.datepicker')

    @include('admin.partials.slug',['type' => 'input', 'input_names'=>'name[ru]','slugs'=>'slug'])

    {!! HTML::script('ace/assets/js/jquery-ui.js') !!}

    <script>
        $(document).ready(function () {


            $('#sortable').sortable();
            initDelete();
        });

        function addItem() {
            $clone = $('#sortable li:last-child').clone();
            $clone.find('input').val("");
            $('#sortable').append($clone);

            initDelete();
        }

        function initDelete() {
            $('#sortable li i.delete').click(function () {
                $(this).closest('li').remove();
            });
        }

    </script>

@endsection


@section('styles')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>

    <style>
        img {
            object-fit: contain;
        }

        #sortable {
            border: 1px solid #eee;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            margin-right: 10px;
            width: 100%;
        }

        #sortable li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            border: 1px solid #ccc;
            background-color: #cce2c1;
        }
    </style>
@endsection
