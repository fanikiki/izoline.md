@extends('admin.body')
@section('title', 'Справочники')


@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a href="{{route('admin.lists.index')}}">Справочники</a></small>
            @if(isset($data))
                @foreach($data->getPaths() as $path)
                    <small class="@if(count($data->getPaths()) > 1)multiple-paths @endif">
                        @if(isset($path['slug']))
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <a href="{{$path['slug']}}">{{ $path['name'] }}</a>
                        @else
                            <i class="ace-icon fa fa-angle-double-right"></i> {{ $path['name'] }}
                        @endif
                    </small>
                @endforeach
            @else
                <small class="multiple-paths"><i class="ace-icon fa fa-angle-double-right"></i> Редактирование справочника</small>
            @endif
        </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/lists', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/lists/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    @if(isset($data))
        <input type="hidden" name="_id" value="{{$data->id}}">
    @endif
    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive"><i
                        class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить
                </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </div>

                            <div class="btn btn-link">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                Изменен: {{ $data->updated_at }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name', 'Название (в админке)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', (isset($data->name) ? $data->name : ''), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="tabbable">
                <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                    <li class="active">
                        <a href="#heading_1_ru" data-toggle="tab" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a href="#heading_1_ro" data-toggle="tab" aria-expanded="false">RO</a>
                    </li>

                    <div class="flex justify-content-around slider-link">
                        <div class="center"><span class="label label-xlg label-purple">Заголовок</span></div>
                    </div>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="heading_1_ru">
                        {{ Form::text('params[heading_1_ru]', (isset($data->params['heading_1_ru']) ? $data->params['heading_1_ru'] : old('heading_1_ru')), array('style'=>'width:100%')) }}
                    </div>
                    <div class="tab-pane" id="heading_1_ro">
                        {{ Form::text('params[heading_1_ro]', (isset($data->params['heading_1_ro']) ? $data->params['heading_1_ro'] : old('heading_1_ro')), array('style'=>'width:100%')) }}
                    </div>
                </div>
            </div>
            <div class="tabbable">
                <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                    <li class="active">
                        <a href="#heading_2_ru" data-toggle="tab" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a href="#heading_2_ro" data-toggle="tab" aria-expanded="false">RO</a>
                    </li>

                    <div class="flex justify-content-around slider-link">
                        <div class="center"><span class="label label-xlg label-purple">Подзаголовок</span></div>
                    </div>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="heading_2_ru">
                        {{ Form::text('params[heading_2_ru]', (isset($data->params['heading_2_ru']) ? $data->params['heading_2_ru'] : old('heading_2_ru')), array('style'=>'width:100%')) }}
                    </div>
                    <div class="tab-pane" id="heading_2_ro">
                        {{ Form::text('params[heading_2_ro]', (isset($data->params['heading_2_ro']) ? $data->params['heading_2_ro'] : old('heading_2_ro')), array('style'=>'width:100%')) }}
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="tabbable">
                <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                    <li class="active">
                        <a href="#btn-name-ru" data-toggle="tab" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a href="#btn-name-ro" data-toggle="tab" aria-expanded="false">RO</a>
                    </li>

                    <div class="flex justify-content-around slider-link">
                        <div class="center"><span class="label label-xlg label-purple">Название кнопки</span></div>
                    </div>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="btn-name-ru">
                        {{ Form::text('params[btn_name_ru]', (isset($data->params['btn_name_ru']) ? $data->params['btn_name_ru'] : old('btn_name_ru')), array('style'=>'width:100%')) }}
                    </div>
                    <div class="tab-pane" id="btn-name-ro">
                        {{ Form::text('params[btn_name_ro]', (isset($data->params['btn_name_ro']) ? $data->params['btn_name_ro'] : old('btn_name_ro')), array('style'=>'width:100%')) }}
                    </div>
                </div>
            </div>

            <div class="tabbable" style="margin-top: 10px;">
                <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                    <li class="active">
                        <a href="#btn-link-ru" data-toggle="tab" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a href="#btn-link-ro" data-toggle="tab" aria-expanded="false">RO</a>
                    </li>

                    <div class="flex justify-content-around slider-link">
                        <div class="center"><span class="label label-xlg label-purple">Ссылка</span></div>

                        <div class="flex gap-10">
                            {{ Form::label('new_tab', 'Открывать в новом окне', ['class'=>'no-padding control-label no-padding-right']) }}
                            <label style="margin: 0;">
                                <input name="params[new_tab]"
                                       class="ace ace-switch ace-switch-3"
                                       type="checkbox"
                                       value="1"
                                       @if (isset($data->params['new_tab']) && $data->params['new_tab'] == 1) checked="checked" @endif>
                                <span class="lbl"></span>
                            </label>
                        </div>
                    </div>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="btn-link-ru">
                        {{ Form::text('params[btn_link_ru]', (isset($data->params['btn_link_ru']) ? $data->params['btn_link_ru'] : old('btn_link_ru')), array('style'=>'width:100%')) }}
                    </div>
                    <div class="tab-pane" id="btn-link-ro">
                        {{ Form::text('params[btn_link_ro]', (isset($data->params['btn_link_ro']) ? $data->params['btn_link_ro'] : old('btn_link_ro')), array('style'=>'width:100%')) }}
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group hide">
                {{ Form::label('parent', 'Родитель', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    @if(isset($parents))
                        @if(isset($parent_id))
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, $parent_id, ['class'=>'col-sm-11 col-xs-12']) }}
                        @else
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, '', ['class'=>'col-sm-11 col-xs-12']) }}
                        @endif
                    @endif
                </div>
            </div>
            <div class="form-group hide">
                <div class="tabbable">
                    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                        <li class="active">
                            <a data-toggle="tab" href="#slug_ru" aria-expanded="true">RU</a>
                        </li>
                        <div class="center">
                            <span class="label label-xlg label-purple">URL</span>
                        </div>
                    </ul>
                    <div class="tab-content">
                        <div id="slug_ru" class="tab-pane active">
                            {{ Form::textarea('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <hr>
    <div class="row">

        <div class="col-xs-12">
            <div class="col-xs-6 col-xs-offset-6">


            </div>
        </div>
    </div>

    <hr>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>

            <li class="hide">
                <a href="#meta" data-toggle="tab">META</a>
            </li>
        </ul>
    </div>

    <div class="tab-content">
        @include('admin.partials.photos', [
            'label_text' => '1 фото - RO<br>2 фото - RU',
            'table' => 'lists',
            'table_id' => isset($data->id) ? $data->id : 0,
            'class' => 'active'
        ])

        @include('admin.partials.meta')

    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.datepicker')

    @include('admin.partials.slug',['type' => 'input', 'input_names'=>'name[ru]','slugs'=>'slug'])

    {!! HTML::script('ace/assets/js/jquery-ui.js') !!}

    <script>
        $(document).ready(function () {


            $('#sortable').sortable();
            initDelete();
        });

        function addItem() {
            $clone = $('#sortable li:last-child').clone();
            $clone.find('input').val("");
            $('#sortable').append($clone);

            initDelete();
        }

        function initDelete() {
            $('#sortable li i.delete').click(function () {
                $(this).closest('li').remove();
            });
        }

    </script>

@endsection


@section('styles')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>

    <style>
        img {
            object-fit: contain;
        }

        #sortable {
            border: 1px solid #eee;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            margin-right: 10px;
            width: 100%;
        }

        #sortable li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            border: 1px solid #ccc;
            background-color: #cce2c1;
        }
    </style>
@endsection
