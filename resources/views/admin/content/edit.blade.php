@extends('admin.body')
@section('title', 'Контент')

@section('centerbox')

    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a
                    href="{{ URL::to('admin/content') }}">Контент</a></small>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Редактирование
                страницы @isset($data)
                    "{{$data->name}}"
                @endisset</small>
        </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/content', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/content/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive"><i
                        class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить
                </button>
            </div>
            <!--
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-yellow btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить и закрыть</button>
            </div>-->
            <div class="col-sm-2 ">
                <label>
                    {{ Form::checkbox('enabled',  1, (isset($data) && $data->enabled == 1 ? true : false), ['class' => 'ace']) }}
                    <span class="lbl hide"> Включить </span>
                </label>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </div>

                            <div class="btn btn-link">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                Изменен: {{ $data->updated_at }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <label class="col-sm-offset-6">
                    @if (isset($data))
                        <a class="label label-info" href="{{ route('content', [$data->slug]) }}" target="_blank">Посмотреть
                            на сайте</a>
                    @endif
                </label>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->


    <div class="row">
        <div class="col-sm-6">
            <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                    <li class="active">
                        <a data-toggle="tab" href="#name_ru" aria-expanded="false">RU</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#name_ro" aria-expanded="false">RO</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="name_ru">
                        {{ Form::textarea('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                    <div class="tab-pane" id="name_ro">
                        {{ Form::textarea('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="text" name="date" id="mydate" class="form-control date-picker"
                               data-date-format="dd-mm-yyyy"
                               value="{{ (isset($data->created_at) ? date('d-m-Y', strtotime($data->created_at)) : old('date', Date::now()->format('d-m-Y'))) }}"/>
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group hide">
                {{ Form::label('sort', 'Сортировка', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::number('sort', (!empty($data->sort) ? $data->sort : 0), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <div class="space"></div>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#description" data-toggle="tab">Описание</a>
            </li>
            <li>
                <a href="#meta" data-toggle="tab">META</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="description">

                <div class="tabbable tabs-left">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#descRu" data-toggle="tab">RU</a>
                        </li>
                        <li>
                            <a href="#descRo" data-toggle="tab">RO</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane in active" id="descRu">
                            {{ Form::textarea('description[ru]', (isset($data->description) ? $data->description : old('description')), array('class' => 'ckeditor', 'id' => 'editor_ru')) }}
                        </div>
                        <div class="tab-pane" id="descRo">
                            {{ Form::textarea('description[ro]', (isset($data->description_ro) ? $data->description_ro : old('description_ro')), array('class' => 'ckeditor', 'id' => 'editor_ro')) }}
                        </div>
                    </div>

                </div>
            </div>
            @include('admin.partials.meta')
        </div>
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.slug',['input_names'=>'name[ru]','slugs'=>'slug'])

    @include('admin.partials.datepicker')

@endsection
