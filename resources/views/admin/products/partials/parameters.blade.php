<div class="tab-pane" id="productsParameters2">
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Тип товара: </label>
                <div class="col-sm-4">
                    <select name="product_type" id="product-type" class="form-control">
                        <option value="">--- Выберите тип ---</option>
                        <option value="1" @if(isset($data) && $data->product_type == 1)selected @endif>Листовой товар</option>
                        <option value="4" @if(isset($data) && $data->product_type == 4)selected @endif>Листовой товар (XPS)</option>
                        <option value="2" @if(isset($data) && $data->product_type == 2)selected @endif>Рулонный товар</option>
                        <option value="3" @if(isset($data) && $data->product_type == 3)selected @endif>Штучный товар</option>
                    </select>
                </div>
            </div>

            <div id="product_type_1" class="hide">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Единица измерения</label>
                    <div class="col-sm-4">
                        <div class="control-group flex">
                            <div class="radio">
                                <label>
                                    <input name="measure_type" type="radio" class="ace" value="4" @if(isset($data) && $data->measure_type == 4)checked @endif>
                                    <span class="lbl"> упак.</span>
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input name="measure_type" type="radio" class="ace" value="2" @if(isset($data) && $data->measure_type == 2)checked @endif>
                                    <span class="lbl"> лист.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Округление</label>
                    <div class="col-sm-4">
                        <div class="control-group flex">
                            <div class="radio">
                                <label>
                                    <input name="round_by" type="radio" class="ace" value="4" @if(isset($data) && $data->round_by == 4)checked @endif>
                                    <span class="lbl"> упак.</span>
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input name="round_by" type="radio" class="ace" value="2" @if(isset($data) && $data->round_by == 2)checked @endif>
                                    <span class="lbl"> лист.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Длина, мм</label>
                    <div class="col-sm-4">
                        <input type="number" name="length" class="form-control param-length" value="{{ $data->length ?? '' }}" step="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Ширина, мм</label>
                    <div class="col-sm-4">
                        <input type="number" name="width" class="form-control param-width" value="{{ $data->width ?? '' }}" step="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Площадь, м<sup>2</sup></label>
                    <div class="col-sm-4">
                        <input type="number" name="area" class="form-control" value="{{ $data->area ?? '' }}" step="0.001" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Цена за м<sup>3</sup></label>
                    <div class="col-sm-4">
                        <input type="number" name="price_cubic" class="form-control" value="{{ $data->price_cubic ?? '' }}" step="0.01">
                    </div>
                </div>

                <hr>

                @if(isset($data) && $data->list_thickness->isNotEmpty())
                    @foreach($data->list_thickness as $list)
                        <div class="row thickness-list">
                            <a href="javascript:void(0);" class="red btnDeleteThicknessRow"><i class="ace-icon fa fa-times-circle"></i></a>

                            <div class="col-md-2 custom-width">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Толщина, мм</label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[thickness][]" class="form-control thickness" value="{{ $list->pivot->thickness ?? 0 }}" step="0.01">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 custom-width">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Объем, м<sup>3</sup></label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[surface][]" class="form-control surface" value="{{ $list->pivot->surface ?? 0 }}" step="0.0001">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 custom-width">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Вес, кг</label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[weight][]" class="form-control" value="{{ $list->pivot->weight ?? 0 }}" step="0.001">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 custom-width">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Шт.</label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[num_in_box][]" class="form-control" value="{{ $list->pivot->num_in_box ?? 0 }}" step="1">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 custom-width">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Цена за лист.</label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[price][]" class="form-control" value="{{ $list->pivot->price ?? 0 }}" step="0.0001" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                @else
                    <div class="row thickness-list">
                        <a href="javascript:void(0);" class="red btnDeleteThicknessRow"><i class="ace-icon fa fa-times-circle"></i></a>

                        <div class="col-md-2 custom-width">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Толщина, мм</label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[thickness][]" class="form-control thickness" value="{{ $data->list_thickness['thickness'] ?? 0 }}" step="0.01">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 custom-width">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Объем, м<sup>3</sup></label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[surface][]" class="form-control surface" value="{{ $data->list_thickness['surface'] ?? 0 }}" step="0.0001">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 custom-width">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Вес, кг</label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[weight][]" class="form-control" value="{{ $data->list_thickness['weight'] ?? 0 }}" step="0.001">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 custom-width">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Шт.</label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[num_in_box][]" class="form-control" value="{{ $data->list_thickness['num_in_box'] ?? 0 }}" step="1">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 custom-width">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Цена за лист.</label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[price][]" class="form-control" value="{{ $data->list_thickness['price'] ?? 0 }}" step="0.0001" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <button type="button" class="btn btn-success btn-mini btnAddThicknessRow"><i class="ace-icon fa fa-plus"></i></button>
            </div>

            <div id="product_type_2" class="hide">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Единица измерения</label>
                    <div class="col-sm-4">
                        <div class="control-group flex">
                            <div class="radio">
                                <label>
                                    <input name="measure_type" type="radio" class="ace" value="3" @if(isset($data) && $data->measure_type == 3)checked @endif>
                                    <span class="lbl"> рул.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Округление</label>
                    <div class="col-sm-4">
                        <div class="control-group flex">
                            <div class="radio">
                                <label>
                                    <input name="round_by" type="radio" class="ace" value="3" @if(isset($data) && $data->round_by == 3)checked @endif>
                                    <span class="lbl"> рул.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Длина, мм</label>
                    <div class="col-sm-4">
                        <input type="number" name="length" class="form-control param-length" value="{{ $data->length ?? '' }}" step="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Ширина, мм</label>
                    <div class="col-sm-4">
                        <input type="number" name="width" class="form-control param-width" value="{{ $data->width ?? '' }}" step="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Толщина, мм</label>
                    <div class="col-sm-4">
                        <input type="number" name="thickness" class="form-control param-thickness" value="{{ $data->thickness ?? '' }}" step="0.01">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Площадь, м<sup>2</sup></label>
                    <div class="col-sm-4">
                        <input type="number" name="area" class="form-control" value="{{ $data->area ?? '' }}" step="0.001" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Объем, м<sup>3</sup></label>
                    <div class="col-sm-4">
                        <input type="number" name="surface" class="form-control" value="{{ $data->surface ?? '' }}" step="0.0001" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Вес, кг</label>
                    <div class="col-sm-4">
                        <input type="number" name="weight" class="form-control" value="{{ $data->weight ?? '' }}" step="0.001">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Цена за м<sup>2</sup></label>
                    <div class="col-sm-4">
                        <input type="number" name="price_square" class="form-control" value="{{ $data->price_square ?? '' }}" step="0.01">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Цена за рул.</label>
                    <div class="col-sm-4">
                        <input type="number" name="price_roll" class="form-control" value="{{ $data->price_roll ?? '' }}" step="0.01">
                    </div>
                </div>
            </div>

            <div id="product_type_3" class="hide">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Единица измерения</label>
                    <div class="col-sm-4">
                        <div class="control-group flex">
                            <div class="radio">
                                <label>
                                    <input name="measure_type" type="radio" class="ace" value="1" @if(isset($data) && $data->measure_type == 4)checked @endif>
                                    <span class="lbl"> шт.</span>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="measure_type" type="radio" class="ace" value="4" @if(isset($data) && $data->measure_type == 1)checked @endif>
                                    <span class="lbl"> уп.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Округление</label>
                    <div class="col-sm-4">
                        <div class="control-group flex">
                            <div class="radio">
                                <label>
                                    <input name="round_by" type="radio" class="ace" value="1" @if(isset($data) && $data->round_by == 1)checked @endif>
                                    <span class="lbl"> шт.</span>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="round_by" type="radio" class="ace" value="4" @if(isset($data) && $data->round_by == 4)checked @endif>
                                    <span class="lbl"> уп.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Длина, мм</label>
                    <div class="col-sm-4">
                        <input type="number" name="length" class="form-control param-length" value="{{ $data->length ?? '' }}" step="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Ширина, мм</label>
                    <div class="col-sm-4">
                        <input type="number" name="width" class="form-control param-width" value="{{ $data->width ?? '' }}" step="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Толщина, мм</label>
                    <div class="col-sm-4">
                        <input type="number" name="thickness" class="form-control param-thickness" value="{{ $data->thickness ?? '' }}" step="0.01">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Площадь, м<sup>2</sup></label>
                    <div class="col-sm-4">
                        <input type="number" name="area" class="form-control" value="{{ $data->area ?? '' }}" step="0.001" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Объем, м<sup>3</sup></label>
                    <div class="col-sm-4">
                        <input type="number" name="surface" class="form-control" value="{{ $data->surface ?? '' }}" step="0.0001" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Вес, кг</label>
                    <div class="col-sm-4">
                        <input type="number" name="weight" class="form-control" value="{{ $data->weight ?? '' }}" step="0.001">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Цена за упаковку</label>
                    <div class="col-sm-4">
                        <input type="number" name="price_box" class="form-control" value="{{ $data->price_box ?? '' }}" step="0.01">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Цена за шт.</label>
                    <div class="col-sm-4">
                        <input type="number" name="price_piece" class="form-control" value="{{ $data->price_piece ?? '' }}" step="0.01">
                    </div>
                </div>
            </div>

            <div id="product_type_4" class="hide">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Единица измерения</label>
                    <div class="col-sm-4">
                        <div class="control-group flex">
                            <div class="radio">
                                <label>
                                    <input name="measure_type" type="radio" class="ace" value="4" @if(isset($data) && $data->measure_type == 4)checked @endif>
                                    <span class="lbl"> упак.</span>
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input name="measure_type" type="radio" class="ace" value="2" @if(isset($data) && $data->measure_type == 2)checked @endif>
                                    <span class="lbl"> лист.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Округление</label>
                    <div class="col-sm-4">
                        <div class="control-group flex">
                            <div class="radio">
                                <label>
                                    <input name="round_by" type="radio" class="ace" value="4" @if(isset($data) && $data->round_by == 4)checked @endif>
                                    <span class="lbl"> упак.</span>
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input name="round_by" type="radio" class="ace" value="2" @if(isset($data) && $data->round_by == 2)checked @endif>
                                    <span class="lbl"> лист.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Длина, мм</label>
                    <div class="col-sm-4">
                        <input type="number" name="length" class="form-control param-length" value="{{ $data->length ?? '' }}" step="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Ширина, мм</label>
                    <div class="col-sm-4">
                        <input type="number" name="width" class="form-control param-width" value="{{ $data->width ?? '' }}" step="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Площадь, м<sup>2</sup></label>
                    <div class="col-sm-4">
                        <input type="number" name="area" class="form-control" value="{{ $data->area ?? '' }}" step="0.001" readonly>
                    </div>
                </div>

                <hr>

                @if(isset($data) && $data->list_thickness->isNotEmpty())
                    @foreach($data->list_thickness as $list)
                        <div class="row thickness-list">
                            <a href="javascript:void(0);" class="red btnDeleteThicknessRow xps"><i class="ace-icon fa fa-times-circle"></i></a>

                            <div class="col-md-2 custom-width xps">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Толщина, мм</label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[thickness][]" class="form-control thickness" value="{{ $list->pivot->thickness ?? 0 }}" step="0.01">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 custom-width xps">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Цена за м<sup>3</sup></label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[price_cubic][]" class="form-control" value="{{ $list->pivot->price_cubic ?? 0 }}" step="0.01">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 custom-width xps">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Объем, м<sup>3</sup></label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[surface][]" class="form-control surface" value="{{ $list->pivot->surface ?? 0 }}" step="0.0001">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 custom-width xps">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Вес, кг</label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[weight][]" class="form-control" value="{{ $list->pivot->weight ?? 0 }}" step="0.001">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 custom-width xps">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Шт.</label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[num_in_box][]" class="form-control" value="{{ $list->pivot->num_in_box ?? 0 }}" step="1">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 custom-width xps">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label no-padding-right">Цена за лист.</label>
                                    <div class="col-sm-6">
                                        <input type="number" name="list_thickness[price][]" class="form-control" value="{{ $list->pivot->price ?? 0 }}" step="0.0001" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                @else
                    <div class="row thickness-list">
                        <a href="javascript:void(0);" class="red btnDeleteThicknessRow xps"><i class="ace-icon fa fa-times-circle"></i></a>

                        <div class="col-md-2 custom-width xps">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Толщина, мм</label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[thickness][]" class="form-control thickness" value="{{ $data->list_thickness['thickness'] ?? 0 }}" step="0.01">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 custom-width xps">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Цена за м<sup>3</sup></label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[price_cubic][]" class="form-control thickness" value="{{ $list->pivot->price_cubic ?? 0 }}" step="0.01">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 custom-width xps">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Объем, м<sup>3</sup></label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[surface][]" class="form-control surface" value="{{ $data->list_thickness['surface'] ?? 0 }}" step="0.0001">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 custom-width xps">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Вес, кг</label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[weight][]" class="form-control" value="{{ $data->list_thickness['weight'] ?? 0 }}" step="0.001">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 custom-width xps">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Шт.</label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[num_in_box][]" class="form-control" value="{{ $data->list_thickness['num_in_box'] ?? 0 }}" step="1">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 custom-width xps">
                            <div class="form-group">
                                <label class="col-sm-6 control-label no-padding-right">Цена за лист.</label>
                                <div class="col-sm-6">
                                    <input type="number" name="list_thickness[price][]" class="form-control" value="{{ $data->list_thickness['price'] ?? 0 }}" step="0.0001" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <button type="button" class="btn btn-success btn-mini btnAddThicknessRow"><i class="ace-icon fa fa-plus"></i></button>
            </div>
        </div>
    </div>
</div>
