<div class="tab-pane" id="prices">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('price_usd', 'Цена (USD)', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('price_usd', (isset($data->price_usd) ? $data->price_usd : old('price_usd')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('sale_price_usd', 'Цена со скидкой (USD)', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('sale_price_usd', (isset($data->sale_price_usd) ? $data->sale_price_usd : old('sale_price_usd')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
        </div>
    </div>
</div>
