@extends('admin.body')
@section('title', 'Заказ')

@section('centerbox')
    <div class="page-header">
        <h1><a href="{{ URL::to('admin/orders') }}">Заказы</a> <small><i class="ace-icon fa fa-angle-double-right"></i>
                Редактирование </small></h1>
    </div>

    @include('admin.partials.errors')

    {{ Form::open(['url' => 'admin/orders/' . $data['order']->id, 'method' => 'put', 'class' => 'form-horizontal']) }}

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive"><i
                        class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить
                </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data['order']))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data['order']->id }}
                            </div>

                            <div class="btn btn-link">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                Создан: {{ $data['order']->created_at }}
                                <br>
                                <i class="ace-icon fa fa-calendar bigger-120 green updated-at-icon"></i>
                                Изменен: {{ $data['order']->updated_at }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="widget-box transparent">
        <div class="widget-body">
            <div class="widget-main padding-24">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="row">
                            <div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
                                <b>Общая информация о заказе</b>
                            </div>
                        </div>

                        <div>
                            @if(isset($data['ip']) && \Illuminate\Support\Facades\Auth::id() == 1)
                                <div style="margin-top: 10px; margin-bottom: 10px;">
                                    <strong>IP</strong>:
                                    {{$data['ip']}}
                                </div>
                            @endif
                            @if(isset($data['user_agent']) && \Illuminate\Support\Facades\Auth::id() == 1)
                                <div style="margin-top: 10px; margin-bottom: 10px; word-break: break-all;">
                                    <strong>Useragent</strong>:
                                    {{$data['user_agent']}}
                                </div>
                            @endif
                            <ul class="list-unstyled spaced">
                                <br>
                                <li>
                                    ФИО:
                                    <i class="ace-icon fa fa-caret-right blue"></i>{{ $data['orderInfo']['name'] ?? ''}}
                                </li>
                                <li>
                                    Телефон:
                                    <i class="ace-icon fa fa-caret-right blue"></i>{{ $data['orderInfo']['phone'] ?? ''}}
                                </li>
                                <li>
                                    Email:
                                    <i class="ace-icon fa fa-caret-right blue"></i>{{ $data['orderInfo']['email'] ?? ''}}
                                </li>

                                @if(isset($data['orderInfo']['payment']))
                                    <li>
                                        Оплата:
                                        @if($data['orderInfo']['payment'] == 'self_payment')
                                            <i class="ace-icon fa fa-caret-right blue"></i> @lang('common.self_payment')
                                        @elseif($data['orderInfo']['payment'] == 'paid_delivery')
                                            <i class="ace-icon fa fa-caret-right blue"></i> @lang('common.paid_delivery')
                                        @endif
                                    </li>
                                @endif

                                @if(!empty($data['orderInfo']['comment']))
                                    <li>
                                        Комментарий:
                                        <i class="ace-icon fa fa-caret-right blue"></i>{{ $data['orderInfo']['comment'] }}
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div><!-- /.col -->

                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
                                <b>Адрес</b>
                            </div>
                        </div>

                        <div>
                            <ul class="list-unstyled spaced">
                                @if(!empty($data['orderInfo']['address']))
                                    <li>
                                        Адрес:
                                        <i class="ace-icon fa fa-caret-right blue"></i>{{ $data['orderInfo']['address'] }}
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="row">
                            <div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
                                <b>Статус</b>
                            </div>
                        </div>
                        <div class="space"></div>
                        <div class="row">
                            <div class="form-group">
                                {{ Form::label('status', 'Статус', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('status', config('custom.order_status'), $data['order']->status, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="space"></div>

                <div>
                    <table id="dynamic-table" class="table table-striped table-bordered table-responsive dataTable">
                        <thead>
                        <tr>
                            <th>Изображение</th>
                            <th>SKU</th>
                            <th>Товар</th>
                            <th>Стоимость</th>
                            <th>Количество</th>
                            <th class="all">Сумма</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if(!empty($data['products']))
                            @foreach($data['products'] as $product)
                                <tr>
                                    <td class="center align-middle">
                                        <img style="width: 50px;"
                                             src="{{ $product['photo'] }}">
                                    </td>
                                    <td class="center align-middle">
                                        {{ $product['product_number'] }}
                                    </td>
                                    <td class="center align-middle">
                                        {{ $product['name'] }}<br>
                                        @if(!empty($product['thickness']))
                                            <small>({{ $product['thickness'] }} мм)</small>
                                        @endif
                                    </td>
                                    <td class="center align-middle">{{ number_format($product['price'], 2, ".", " ") }}
                                        {{ $data['currency']['label'] }}
                                    </td>
                                    <td class="center align-middle">
                                        <span>{{ $product['quantity'] }}</span>
                                    </td>
                                    <td class="center align-middle">{{ number_format($product['price'] * $product['quantity'], 2, ".", " ") }}
                                        {{ $data['currency']['label'] }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="col-sm-5 pull-right">
                        <h4 class="pull-right">
                            Общая сумма :
                            <span class="red">
                                {{ number_format($data['order']->getTotalAmount(), 2, ".", " ") }} лей
                            </span>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('styles')
    <style>
        .btn-link:hover, .btn-link:focus {
            text-decoration: none;
            cursor: auto;
        }

        .updated-at-icon {
            margin-left: 12px;
            margin-top: 5px;
        }
    </style>

    @if(\App\Http\Controllers\Controller::isMobile())
        {{HTML::style('ace/assets/css/dataTables.responsive.css')}}
    @endif
@endsection

@section('scripts')

    @if(\App\Http\Controllers\Controller::isMobile())
        {{HTML::script('ace/assets/js/dataTables/jquery.dataTables.js')}}
        {{HTML::script('ace/assets/js/dataTables/jquery.dataTables.bootstrap.js')}}
        {{HTML::script('ace/assets/js/dataTables/extensions/TableTools/js/dataTables.tableTools.js')}}
        {{HTML::script('ace/assets/js/dataTables/extensions/ColVis/js/dataTables.colVis.js')}}
        {{HTML::script('ace/assets/js/dataTables/dataTables.responsive.js')}}

        <script>
            $("#dynamic-table").DataTable({
                searching: false,
                paging: false,
                ordering: false,
                info: false,
                responsive: true
            });
        </script>
    @endif

    @include('admin.partials.datepicker')
    @include('admin.partials.chosen')

    {!! HTML::script('ace/dist/js/dw_tooltip_c.js') !!}

    <script>
        $('.chosen-select').chosen();
    </script>
@stop
