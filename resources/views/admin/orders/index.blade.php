@extends('admin.body')
@section('title', 'Заказы')
@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Заказы</small>
        </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <a class="btn btn-success"
               href="{{ URL::to('admin/orders/settings') }}">
                <i class="ace-icon fa fa-cogs  bigger-120"></i>
                Настройки
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div class="table-header hidden-xs hidden-sm"> Список заказов</div>

            <table id="dynamic-table" class="table table-orders table-striped table-bordered table-responsive table-hover dataTable">
                <thead>
                <tr>
                    <th align="center" class="all">ID</th>
                    <th align="center" class="all">Дата создания</th>
                    <th align="center">Статус</th>
                    <th align="center" class="all">Сумма</th>
                    <th align="center" class="desktop never"><i class="menu-icon fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody bgcolor="white">
                </tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@endsection

@section('styles')
    {{HTML::style('ace/assets/css/dataTables.responsive.css')}}
@endsection

@section('scripts')

    <script>
        var data = [];

        var columns = [
            {
                data : 'id',
                "render": {
                    "display": function (data, type, full) {
                        return `<a href='admin/orders/${data}/edit'>${data}</a>`;
                    }
                },
                'className': 'center',
                'searchable': true
            },
            {
                data : 'created_at',
                "render": {
                    "display": function (data, type, full) {
                        return `<a href='admin/orders/${full.id}/edit'>${data}</a>`;
                    }
                },
                'className': 'center',
                'searchable': true
            },
            {data: 'status', 'className': 'center', 'name': 'status'},
            {data: 'amount', 'className': 'center', 'name': 'data', 'searchable': false, 'orderable': false},
            {data: 'actions', 'className': 'center', 'name': 'data', 'searchable': false}
        ];
    </script>

    @include('admin.partials.datatable-init-ajax', [
        'ajax' => 'admin/get-orders',
        'order' => ['column' => 0, 'type' => 'desc'],
        'no_state_save' => true
    ])

    <script>
        /*$('#dynamic-table').on('draw.dt', function (e, datatable) {
            datatable.aoData.forEach(function(item){
                let status_td = item.anCells[5];
                let cls_name = $(status_td).find('select')[0].className;
                if(cls_name){
                    $(status_td).addClass(cls_name);
                }
            });
        });*/

        $('#dynamic-table').on('click', '.mobile-actions .delete-btn', function (e){
            if (!confirm("Удалить?")) {
                return false;
            }
            var id      =  $(this).closest('.action-buttons').data('id');
            var model   =  $(this).closest('.action-buttons').data('model');
            var $thisdiv = $(this);

            $.get("admin/json/delete", {'id': id, 'model': model}, function(data) {
                $thisdiv.closest('tr').prev().remove();
                $thisdiv.closest('tr').remove();
            });
        });
    </script>

@endsection
