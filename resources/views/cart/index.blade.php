@extends('body')
@section('centerbox')
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a
                                href="{{ route('index') }}">@lang('common.home')</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active"
                            aria-current="page">@lang('common.cart')</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="mb-4">
            <h1 class="text-center">@lang('common.cart')</h1>
        </div>

        <cart-list
            checkout_route="{{ route('cart.checkout-page') }}"
            checkout_manager_route="{{ route('cart.checkout-manager') }}"
            currency="{{ session('currency')['label_short'] }}">
        </cart-list>

        @if($recommended->isNotEmpty())
        <!-- Recommended Products -->
        <div class="mb-6 d-none d-xl-block">
            <div class="position-relative">
                <div class="border-bottom border-color-1 mb-2">
                    <h3 class="d-inline-block section-title section-title__full mb-0 pb-2 font-size-22">@lang('common.recommended_goods')</h3>
                </div>
                <div
                    class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                    data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                    data-slides-show="5"
                    data-slides-scroll="1"
                    data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                    data-arrow-left-classes="fa fa-angle-left right-1"
                    data-arrow-right-classes="fa fa-angle-right right-0"
                    data-responsive='[{
                      "breakpoint": 1400,
                      "settings": {
                        "slidesToShow": 4
                      }
                    }, {
                        "breakpoint": 1200,
                        "settings": {
                          "slidesToShow": 4
                        }
                    }, {
                      "breakpoint": 992,
                      "settings": {
                        "slidesToShow": 3
                      }
                    }, {
                      "breakpoint": 768,
                      "settings": {
                        "slidesToShow": 2
                      }
                    }, {
                      "breakpoint": 554,
                      "settings": {
                        "slidesToShow": 2
                      }
                    }]'>

                    @foreach($recommended as $product)
                        <div class="js-slide products-group">
                            <div class="product-item">
                                @include('partials.product-box-recommended')
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- End Recommended Products -->
        @endif
    </div>

@endsection
