@extends('body')
@section('centerbox')
<div class="container">
  <div class="text-center mx-auto mw-690">
    <div class=" m-5 p-3 border borders-radius-17">
      <h5 class="mb-1 font-weight-bold">@lang('common.order_success', ['order_id' => session('order')['id']])</h5>
      <div>@lang('common.order_success_text')</div>
    </div>
  </div>
</div>
@endsection
