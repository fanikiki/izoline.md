@extends('body')
@section('centerbox')
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a
                                href="{{ route('index') }}">@lang('common.home')</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active"
                            aria-current="page">@lang('common.checkout')</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="mb-5">
            <h1 class="text-center">@lang('common.checkout')</h1>
        </div>
        <form method="post" action="{{ route('cart.checkout') }}" id="orderFrm">
            <div class="row">
                <div class="col-lg-5 order-lg-2 mb-7 mb-lg-0">
                    <div class="pl-lg-3 ">
                        <div class="bg-gray-1 rounded-lg">
                            <!-- Order Summary -->
                            <div class="p-4 mb-4 checkout-table">
                                <!-- Title -->
                                <div class="border-bottom border-color-1 mb-5">
                                    <h3 class="section-title mb-0 pb-2 font-size-25">@lang('common.your_order')</h3>
                                </div>
                                <!-- End Title -->

                                <!-- Product Content -->
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="product-name">@lang('common.product')</th>
                                        <th class="product-total">@lang('common.total')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr class="cart_item">
                                            <td>
                                                {{ $product['name'] }}&nbsp;<strong
                                                    class="product-quantity">× {{ $product['quantity'] }}</strong>

                                                @if(!empty($product['thickness']))
                                                    <br>
                                                    <small>(@lang('common.thickness'): {{ $product['thickness'] }})</small>
                                                @endif
                                            </td>
                                            <td>{{ number_format($product['price'] * $product['quantity']) }} {{ session('currency')['label_short'] }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>@lang('common.total_price')</th>
                                        <td><strong>{{ $amount }} {{ session('currency')['label_short'] }}</strong></td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <!-- End Product Content -->
                                <div class="border-top border-width-3 border-color-1 pt-3 mb-3">
                                    <!-- Basics Accordion -->
                                    <div>
                                        <div class="border-bottom border-color-1 border-dotted-bottom">
                                            <div class="p-3">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input"
                                                           id="FourstylishRadio1" name="payment" value="self_payment" required>
                                                    <label class="custom-control-label form-label"
                                                           for="FourstylishRadio1">
                                                        @lang('common.self_payment')
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="border-bottom border-color-1 border-dotted-bottom">
                                            <div class="p-3">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input"
                                                           id="FourstylishRadio1" name="payment" value="paid_delivery" required>
                                                    <label class="custom-control-label form-label"
                                                           for="FourstylishRadio1">
                                                        @lang('common.paid_delivery')
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Card -->
                                    </div>
                                    <!-- End Basics Accordion -->
                                </div>
                                <button type="submit"
                                        class="btn btn-primary-dark-w btn-block btn-pill font-size-20 mb-3 py-3 btnSubmitOrder">@lang('common.order')</button>
                            </div>
                            <!-- End Order Summary -->
                        </div>
                    </div>
                </div>

                <div class="col-lg-7 order-lg-1">
                    <div class="pb-7 mb-7">
                        <!-- Title -->
                        <div class="border-bottom border-color-1 mb-5">
                            <h3 class="section-title mb-0 pb-2 font-size-25">@lang('common.order_details')</h3>
                        </div>
                        <!-- End Title -->

                        <!-- Billing Form -->
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Input -->
                                <div class="js-form-message mb-6">
                                    <label class="form-label">
                                        @lang('common.name')
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" name="name" required autocomplete="off">
                                </div>
                                <!-- End Input -->
                            </div>

                            <div class="col-md-6">
                                <!-- Input -->
                                <div class="js-form-message mb-6">
                                    <label class="form-label">
                                        @lang('common.phone')
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" name="phone" required autocomplete="off">
                                </div>
                                <!-- End Input -->
                            </div>

                            <div class="col-md-12">
                                <!-- Input -->
                                <div class="js-form-message mb-6">
                                    <label class="form-label">
                                        @lang('common.email')
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="email" class="form-control" name="email" required>
                                </div>
                                <!-- End Input -->
                            </div>

                            <div class="col-md-12">
                                <!-- Input -->
                                <div class="js-form-message mb-6">
                                    <label class="form-label">
                                        @lang('common.form_address')
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" name="address" required>
                                </div>
                                <!-- End Input -->
                            </div>
                        </div>
                        <!-- End Billing Form -->

                        <!-- Input -->
                        <div class="js-form-message mb-6">
                            <label class="form-label">
                                @lang('common.order_comment')
                            </label>

                            <div class="input-group">
                                <textarea class="form-control p-5" rows="4" name="comment"></textarea>
                            </div>
                        </div>
                        <!-- End Input -->
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $("#orderFrm").submit(function (){
           $(this).find(".btnSubmitOrder").text("@lang('common.please_wait')");
           $(this).find(".btnSubmitOrder").attr("disabled", true);
        });
    </script>
@endsection
