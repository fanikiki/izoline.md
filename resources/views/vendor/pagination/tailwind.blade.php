@if ($paginator->hasPages())
    <nav role="navigation" aria-label="{{ __('Pagination Navigation') }}" class="flex items-center justify-between">
        <div class="flex justify-between hidden">
            @if ($paginator->onFirstPage())
                <span class="relative inline-flex items-center min-h-48px min-w-48px inline-flex items-center justify-center text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-md">
                    {!! __('pagination.previous') !!}
                </span>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" class="relative min-h-48px min-w-48px inline-flex items-center justify-center inline-flex items-center text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                    {!! __('pagination.previous') !!}
                </a>
            @endif

            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" class="relative inline-flex items-center min-h-48px min-w-48px inline-flex items-center justify-center text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                    {!! __('pagination.next') !!}
                </a>
            @else
                <span class="relative min-h-48px min-w-48px inline-flex items-center justify-center ml-3 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-md">
                    {!! __('pagination.next') !!}
                </span>
            @endif
        </div>

        <div class="flex-1 flex items-center justify-between">
            <div class="">
                <p class="text-sm text-gray-700 leading-5 hidden">
                    {!! __('Showing') !!}
                    <span class="font-medium">{{ $paginator->firstItem() }}</span>
                    {!! __('to') !!}
                    <span class="font-medium">{{ $paginator->lastItem() }}</span>
                    {!! __('of') !!}
                    <span class="font-medium">{{ $paginator->total() }}</span>
                    {!! __('results') !!}
                </p>



            </div>

            <div>

                <span class="hidden sm:flex items-center justify-center flex-wrap">
                    {{-- Previous Page Link --}}
                    @if ($paginator->onFirstPage())
                        <span aria-disabled="true" aria-label="{{ __('pagination.previous') }}" class="min-h-48px min-w-48px xl:min-w-42px xl:min-h-42px xl:text-15px sm:w-30px sm:h-30px  sm:min-h-30px sm:min-w-30px sm:h-auto sm:w-auto inline-flex items-center justify-center bg-white rounded-base border-grey-1 border-solid border mr-5px" aria-hidden="true">
                            <svg width="11" height="20" class="sm:w-7px" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10 1L1 10L10 19" stroke="url(#paint0_linear_741_17700)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M10 1L1 10L10 19" stroke="#B1B1B1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </span>
                    @else
                        <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="min-h-48px min-w-48px xl:min-w-42px xl:min-h-42px xl:text-15px  sm:min-w-30px sm:min-h-30px sm:h-auto sm:w-30px inline-flex items-center justify-center bg-white rounded-base border-grey-1 border-solid border mr-5px" aria-label="{{ __('pagination.previous') }}">
                            <svg width="11" height="20" class="sm:w-7px" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10 1L1 10L10 19" stroke="url(#paint0_linear_741_17700)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M10 1L1 10L10 19" stroke="#B1B1B1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </a>
                    @endif

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <span aria-disabled="true">
                                <span class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 cursor-default leading-5">{{ $element }}</span>
                            </span>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                            <!--  Use three dots when current page is greater than 4.  -->
                                @if ($paginator->currentPage() > 4 && $page === 2)
                                    <span class="page-link">...</span>
                                @endif
                                @if ($page == $paginator->currentPage())
                                    <span aria-current="page" class="hover:shadow-base sm:mx-2px text-sm h-30px w-30px text-grey-1 font-comfortaa inline-flex justify-center items-center bg-gradient-to-r from-corvette to-paper-brown rounded-base text-white">
                                        {{ $page }}
                                    </span>
                                @elseif ($page === $paginator->currentPage() + 1 || $page === $paginator->currentPage() + 2 || $page === $paginator->currentPage() - 1 || $page === $paginator->currentPage() - 2 || $page === $paginator->lastPage() || $page === 1)
                                    <a href="{{ $url }}" class="sm:mx-2px hover:shadow-base text-sm h-30px w-30px inline-flex justify-center items-center bg-white rounded-base border-grey-1 border-solid border mx-5px" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                                        {{ $page }}
                                    </a>
                                @endif
                                @if ($paginator->currentPage() < $paginator->lastPage() - 3 && $page === $paginator->lastPage() - 1)
                                    <span class="page-link">...</span>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="min-h-48px min-w-48px xl:min-w-42px xl:min-h-42px xl:text-15px sm:min-w-30px sm:min-h-30px sm:h-auto sm:w-30px inline-flex items-center justify-center bg-white rounded-base border-grey-1 border-solid border ml-5px" aria-label="{{ __('pagination.next') }}">
                            <svg width="11" height="20" class="sm:w-7px" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L10 10L1 19" stroke="url(#paint0_linear_741_17702)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M1 1L10 10L1 19" stroke="#B1B1B1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </a>
                    @else
                        <span aria-disabled="true" aria-label="{{ __('pagination.next') }}" class="min-h-48px min-w-48px xl:min-w-42px xl:min-h-42px xl:text-15px sm:min-w-30px sm:min-h-30px sm:h-auto sm:w-30px inline-flex items-center justify-center  bg-white rounded-base border-grey-1 border-solid border ml-5px">
                            <svg width="11" height="20" class="sm:w-7px" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L10 10L1 19" stroke="url(#paint0_linear_741_17702)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M1 1L10 10L1 19" stroke="#B1B1B1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                        </span>
                    @endif
                </span>

                <span class="sm:hidden flex items-center justify-center flex-wrap">
                    {{-- Previous Page Link --}}
                    @if ($paginator->onFirstPage())
                        <span aria-disabled="true" aria-label="{{ __('pagination.previous') }}" class="min-h-48px min-w-48px xl:min-w-42px xl:min-h-42px xl:text-15px sm:min-w-30px sm:min-h-30px sm:h-auto sm:w-30px inline-flex items-center justify-center bg-white rounded-base border-grey-1 border-solid border mr-5px" aria-hidden="true">

                            <svg width="11" height="20" class="sm:w-7px" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10 1L1 10L10 19" stroke="url(#paint0_linear_741_17700)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M10 1L1 10L10 19" stroke="#B1B1B1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>

                        </span>
                    @else
                        <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="min-h-48px min-w-48px xl:min-w-42px xl:min-h-42px xl:text-15px  sm:min-w-30px sm:min-h-30px sm:h-auto sm:w-30px inline-flex items-center justify-center bg-white rounded-base border-grey-1 border-solid border mr-5px" aria-label="{{ __('pagination.previous') }}">
                            <svg width="11" height="20" class="sm:w-7px" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10 1L1 10L10 19" stroke="url(#paint0_linear_741_17700)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M10 1L1 10L10 19" stroke="#B1B1B1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </a>
                    @endif

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <span aria-disabled="true">
                                <span class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 cursor-default leading-5">{{ $element }}</span>
                            </span>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <span aria-current="page" class="hover:shadow-base min-w-48px min-h-48px xl:min-w-42px xl:min-h-42px xl:text-15px  xl:min-w-42px xl:min-h-42px xl:text-15px text-grey-1 font-comfortaa px-1 py-1 inline-flex justify-center items-center bg-gradient-to-r from-corvette to-paper-brown rounded-base mx-5px text-white">
                                        {{ $page }}
                                    </span>
                                @else
                                    <a href="{{ $url }}" class="sm:hidden hover:shadow-base min-w-48px min-h-48px xl:min-w-42px xl:min-h-42px xl:text-15px  xl:min-w-42px xl:min-h-42px xl:text-15px text-grey-1 font-comfortaa px-1 py-1 inline-flex justify-center items-center bg-white rounded-base border-grey-1 border-solid border mx-5px" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                                        {{ $page }}
                                    </a>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    {{--{{ dd($paginator) }}--}}

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="sm:mx-0.5 min-h-48px min-w-48px xl:min-w-42px xl:min-h-42px xl:text-15px  inline-flex items-center justify-center bg-white rounded-base border-grey-1 border-solid border ml-5px" aria-label="{{ __('pagination.next') }}">
                            <svg width="11" height="20" class="sm:w-7px" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L10 10L1 19" stroke="url(#paint0_linear_741_17702)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M1 1L10 10L1 19" stroke="#B1B1B1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </a>
                    @else
                        <span aria-disabled="true" aria-label="{{ __('pagination.next') }}" class="sm:mx-0.5 min-h-48px min-w-48px xl:min-w-42px xl:min-h-42px xl:text-15px  inline-flex items-center justify-center  bg-white rounded-base border-grey-1 border-solid border ml-5px">
                            <svg width="11" height="20" class="sm:w-7px" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L10 10L1 19" stroke="url(#paint0_linear_741_17702)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M1 1L10 10L1 19" stroke="#B1B1B1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                        </span>
                    @endif
                </span>
            </div>


        </div>
    </nav>
@endif
