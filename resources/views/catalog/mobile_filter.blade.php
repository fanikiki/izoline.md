<aside id="sidebarContent1" class="u-sidebar u-sidebar--left" aria-labelledby="sidebarNavToggler1">
    <div class="u-sidebar__scroller">
        <div class="u-sidebar__container">
            <div class="">
                <!-- Toggle Button -->
                <div class="button-close__mob-menu d-flex align-items-start pt-4 px-4 mb-2 border-bottom border-color-1">
                    <div class="">
                        <h3 class="section-title section-title__sm mb-0 pb-3 font-size-18">@lang('common.filters')</h3>
                    </div>

                    <button type="button" class="close ml-auto"
                            id="buttonCloseContent"
                            aria-controls="sidebarContent1"
                            aria-haspopup="true"
                            aria-expanded="false"
                            data-unfold-event="click"
                            data-unfold-hide-on-scroll="false"
                            data-unfold-target="#sidebarContent1"
                            data-unfold-type="css-animation"
                            data-unfold-animation-in="fadeInLeft"
                            data-unfold-animation-out="fadeOutLeft"
                            data-unfold-duration="500">
                        <span aria-hidden="true"><i class="ec ec-close-remove"></i></span>
                    </button>
                </div>
                <!-- End Toggle Button -->

                <!-- Content -->
                <div class="js-scrollbar u-sidebar__body px-4 pt-4">
                    <div class="u-sidebar__content u-header-sidebar__content">
                        <div class="mb-6 border border-width-2 border-color-3 borders-radius-6">
                            <!-- List -->
                            @if($categories->isNotEmpty())
                                <ul id="sidebarNav" class="list-unstyled mb-0 sidebar-navbar view-all">
                                    <li>
                                        <div class="dropdown-title"
                                             style="background-color: #f5f5f5;">@lang('common.categories_goods')</div>
                                    </li>

                                    @foreach($categories as $parent)
                                        <li>
                                            @if($parent->children->count())
                                                <a style="padding-left: 0;" id="parent{{$parent->id}}"
                                                   class="dropdown-toggle dropdown-toggle-collapse" href="javascript:void(0);"
                                                   role="button"
                                                   data-toggle="collapse" aria-expanded="false"
                                                   data-target="#children{{ $parent->children[0]->id }}">
                                                    {{ $parent->name }}
                                                </a>
                                            @else
                                                <a style="padding: .5rem 0;"
                                                   href="{{ route('catalog.get-parent-category', $parent->slug) }}"
                                                   role="button">
                                                    {{ $parent->name }}
                                                </a>
                                            @endif

                                            @if($parent->children->count())
                                                <div id="children{{ $parent->children[0]->id }}" class="collapse"
                                                     data-parent="#parent{{$parent->id}}">
                                                    <ul id="children{{ $parent->children[0]->id }}"
                                                        class="list-unstyled dropdown-list">
                                                        @foreach($parent->children as $child)
                                                            <li>
                                                                <a class="dropdown-item"
                                                                   href="{{ route('catalog.get-child-category', [$parent->slug, $child->slug]) }}">
                                                                    {{ $child->name }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                            <!-- End List -->
                        </div>
                        <div class="mb-6">
                            <form class="filterFrm">
                                <div class="border-bottom pb-4 mb-4">
                                    @foreach($category_parameters as $parameter)
                                        @if(!$parameter->is_filter) @continue @endif

                                        <h4 class="font-size-14 font-weight-bold mt-3">{{ $parameter->name }}</h4>

                                        @if($parameter->values->isNotEmpty())
                                            <select name="parameters[{{$parameter->id}}][]" class="chosen" multiple>
                                                @foreach($parameter->values as $param_value)
                                                    <option value="{{ $param_value->id }}"
                                                            @if(isset($sel_params['parameter_' . $parameter->id]) && in_array($param_value->id, $sel_params['parameter_' . $parameter->id]))selected @endif>
                                                        {{ $param_value->value }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        @endif
                                    @endforeach
                                </div>
                                <button type="submit"
                                        class="btn px-4 btn-primary-dark-w py-2 rounded-lg">@lang('common.apply')</button>

                                <input type="hidden" name="sort" class="sort">
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Content -->
            </div>
        </div>
    </div>
</aside>
