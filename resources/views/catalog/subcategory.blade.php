@extends('body')
@include('partials.meta', ['data' => $child_category])
@section('centerbox')
    <!-- ========== MAIN CONTENT ========== -->
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1">
                            <a href="{{ route('index') }}">@lang('common.home')</a>
                        </li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1">
                            <a href="{{ route('catalog.get-parent-category', $parent_category->slug) }}">{{ $parent_category->name }}</a>
                        </li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">{{ $child_category->name }}</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="row">
            <div class="d-none d-xl-block col-xl-3 col-wd-2gdot5">
                <div class="mb-8 border border-width-2 border-color-3 borders-radius-6">
                    <!-- List -->
                    <ul id="sidebarNav" class="list-unstyled mb-0 sidebar-navbar">
                        <li>
                            <a class="dropdown-current active" href="javascript:void(0);">{{ $parent_category->name }}</a>

                            @if($categories->isNotEmpty())
                                <ul class="list-unstyled dropdown-list">
                                    @foreach($categories as $category)
                                        <li>
                                            <a class="dropdown-item @if($category->id == $child_category->id)active @endif" href="{{ route('catalog.get-child-category', [$parent_category->slug, $category->slug]) }}">
                                                {{ $category->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    </ul>
                    <!-- End List -->
                </div>
                <div class="mb-6">
                    <div class="border-bottom border-color-1 mb-5">
                        <h3 class="section-title section-title__sm mb-0 pb-2 font-size-18">@lang('common.filters')</h3>
                    </div>

                    <form class="filterFrm">
                        <div class="border-bottom pb-4 mb-4">
                            @foreach($category_parameters as $parameter)
                                @if(!$parameter->is_filter) @continue @endif

                                <h4 class="font-size-14 font-weight-bold mt-3">{{ $parameter->name }}</h4>

                                @if($parameter->values->isNotEmpty())
                                    <select name="parameters[{{$parameter->id}}][]" class="chosen" multiple>
                                        @foreach($parameter->values as $param_value)
                                            <option value="{{ $param_value->id }}"
                                                    @if(isset($sel_params['parameter_' . $parameter->id]) && in_array($param_value->id, $sel_params['parameter_' . $parameter->id]))selected @endif>
                                                {{ $param_value->value }}
                                            </option>
                                        @endforeach
                                    </select>
                                @endif
                            @endforeach
                        </div>
                        <button type="submit"
                                class="btn px-4 btn-primary-dark-w py-2 rounded-lg">@lang('common.apply')</button>

                        <input type="hidden" name="sort" class="sort">
                    </form>
                </div>
            </div>

            <div class="col-xl-9 col-wd-9gdot5">
                <!-- Shop-control-bar Title -->
                <div class="flex-center-between mb-3">
                    <h3 class="font-size-25 mb-0">{{ $parent_category->name }}</h3>
                </div>
                <!-- End shop-control-bar Title -->
                <!-- Shop-control-bar -->
                <div class="bg-gray-1 flex-center-between borders-radius-9 py-2">
                    <div class="d-xl-none px-2">
                        <!-- Account Sidebar Toggle Button -->
                        <a id="sidebarNavToggler1" class="btn btn-sm py-1 font-weight-normal color-filter" href="javascript:;"
                           role="button"
                           aria-controls="sidebarContent1"
                           aria-haspopup="true"
                           aria-expanded="false"
                           data-unfold-event="click"
                           data-unfold-hide-on-scroll="false"
                           data-unfold-target="#sidebarContent1"
                           data-unfold-type="css-animation"
                           data-unfold-animation-in="fadeInLeft"
                           data-unfold-animation-out="fadeOutLeft"
                           data-unfold-duration="500">
                            <i class="fas fa-sliders-h color-filter-icon"></i> <span class="ml-1">@lang('common.filters')</span>
                        </a>
                        <!-- End Account Sidebar Toggle Button -->
                    </div>
                    <div class="px-3 d-none d-xl-block">
                        <ul class="nav nav-tab-shop" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-two-example1-tab" data-toggle="pill"
                                   href="#pills-two-example1" role="tab" aria-controls="pills-two-example1"
                                   aria-selected="false">
                                    <div class="d-md-flex justify-content-md-center align-items-md-center">
                                        <i class="fa fa-th"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-four-example1-tab" data-toggle="pill"
                                   href="#pills-four-example1" role="tab" aria-controls="pills-four-example1"
                                   aria-selected="true">
                                    <div class="d-md-flex justify-content-md-center align-items-md-center">
                                        <i class="fa fa-th-list"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <select
                            class="select-sort js-select selectpicker dropdown-select max-width-200 max-width-160-sm right-dropdown-0 px-2 px-xl-0 text-center"
                            data-style="btn-sm bg-white font-weight-normal py-2 border text-gray-20 bg-lg-down-transparent border-lg-down-0">
                            <option value="date_desc" @if($sel_params['sort'] == '')selected @endif>@lang('common.sort_date_desc')</option>
                            <option value="price_asc" @if($sel_params['sort'] == 'price_asc')selected @endif>@lang('common.sort_price_asc')</option>
                            <option value="price_desc" @if($sel_params['sort'] == 'price_desc')selected @endif>@lang('common.sort_price_desc')</option>
                        </select>
                    </div>
                </div>
                <!-- End Shop-control-bar -->

                <!-- Shop Body -->
                <!-- Tab Content -->
                @if($products->isNotEmpty())
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade pt-2 active show" id="pills-two-example1" role="tabpanel"
                             aria-labelledby="pills-two-example1-tab" data-target-group="groups">
                            <ul class="row list-unstyled products-group no-gutters">
                                @foreach($products as $product)
                                    <li class="col-12 col-sm-6 col-md-4 product-item">
                                        @include('partials.product-box-grid')
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="tab-pane fade pt-2" id="pills-four-example1" role="tabpanel"
                             aria-labelledby="pills-four-example1-tab" data-target-group="groups">
                            <ul class="d-block list-unstyled products-group prodcut-list-view-small">
                                @foreach($products as $product)
                                    <li class="product-item remove-divider">
                                        @include('partials.product-box-row')
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <nav class="d-md-flex justify-content-center align-items-center border-top pt-3"
                         aria-label="Page navigation example">
                        {{ $products->appends(request()->input())->links() }}
                    </nav>
                @else
                    <p>@lang('common.no_products')</p>
                @endif
                <!-- End Tab Content -->
                <!-- End Shop Body -->

                @if($recommended->isNotEmpty())
                    <!-- Recommended Products -->
                    <div class="mb-6 d-none d-xl-block">
                        <div class="position-relative">
                            <div class="border-bottom border-color-1 mb-2">
                                <h3 class="d-inline-block section-title section-title__full mb-0 pb-2 font-size-22">@lang('common.recommended_goods')</h3>
                            </div>
                            <div
                                class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                                data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                                data-slides-show="5"
                                data-slides-scroll="1"
                                data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                                data-arrow-left-classes="fa fa-angle-left right-1"
                                data-arrow-right-classes="fa fa-angle-right right-0"
                                data-responsive='[{
                      "breakpoint": 1400,
                      "settings": {
                        "slidesToShow": 4
                      }
                    }, {
                        "breakpoint": 1200,
                        "settings": {
                          "slidesToShow": 4
                        }
                    }, {
                      "breakpoint": 992,
                      "settings": {
                        "slidesToShow": 3
                      }
                    }, {
                      "breakpoint": 768,
                      "settings": {
                        "slidesToShow": 2
                      }
                    }, {
                      "breakpoint": 554,
                      "settings": {
                        "slidesToShow": 2
                      }
                    }]'>

                                @foreach($recommended as $product)
                                    <div class="js-slide products-group">
                                        <div class="product-item h-100">
                                            @include('partials.product-box-recommended')
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- End Recommended Products -->
                @endif
            </div>
        </div>
    </div>

    @if(\App\Http\Controllers\Controller::isMobile())
        @include('catalog.mobile_filter')
    @endif
    <!-- End Sidebar Navigation -->
    <!-- ========== END MAIN CONTENT ========== -->
@endsection

@section('scripts')
    <script src="ace/dist/js/chosen.jquery.min.js"></script>
    <link rel="stylesheet" href="ace/dist/css/chosen.min.css">

    <script>
        $(".chosen").chosen({
            'width': '100%',
            placeholder_text_multiple: "@lang('common.all')",
        });
    </script>
@endsection
