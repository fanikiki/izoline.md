@extends('body')
@include('partials.meta')
@section('centerbox')
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{ route('index') }}">@lang('common.home')</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">{{ $data->name }}</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->
    <div class="bg-img-hero mb-14">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="h1 font-weight-bold">{{ $data->name }}</h1>
                    <p class="text-gray-39 font-size-18 text-lh-default">{!! $data->description !!}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
