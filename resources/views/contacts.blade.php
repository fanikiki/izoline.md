@extends('body')
@section('centerbox')
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{ route('index') }}">@lang('common.home')</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">@lang('common.contacts')</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->


            <div class="container">
                <div class="mb-5">
                    <h1 class="text-center">@lang('common.contacts')</h1>
                </div>
                <div class="row mb-10">
                    <div class="col-lg-7 col-xl-6 mb-8 mb-lg-0">
                        <div class="mr-xl-6">
                            <div class="border-bottom border-color-1 mb-5">
                                <h3 class="section-title mb-0 pb-2 font-size-25">@lang('common.contact_us_form')</h3>
                            </div>
                            <p class="max-width-830-xl text-gray-90">@lang('common.contact_us_form_text')</p>
                            <form id="contactFrm">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="js-form-message mb-4">
                                      <label class="form-label">
                                        @lang('common.name')
                                        <span class="text-danger">*</span>
                                      </label>
                                      <input type="text" class="form-control" name="name" aria-label="" required autocomplete="off">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="js-form-message mb-4">
                                      <label class="form-label">
                                        @lang('common.phone')
                                        <span class="text-danger">*</span>
                                      </label>
                                      <input type="text" class="form-control" name="phone" required autocomplete="off">
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="js-form-message mb-4">
                                      <label class="form-label">
                                        E-mail
                                        <span class="text-danger">*</span>
                                      </label>
                                      <input type="email" class="form-control" name="email" required autocomplete="off">
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="js-form-message mb-4">
                                      <label class="form-label">
                                        @lang('common.your_message')
                                        <span class="text-danger">*</span>
                                      </label>
                                      <textarea class="form-control p-5" rows="4" name="message" required autocomplete="off"></textarea>
                                    </div>
                                  </div>
                                </div>
                                <div class="mb-3">
                                  <button type="submit" class="btn btn-primary-dark-w px-5">@lang('common.send')</button>
                                </div>
                              </form>
                        </div>
                    </div>
                    <div class="col-lg-5 col-xl-6">
                        <div class="mb-6">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2724.2957014323874!2d28.9535736!3d46.936227099999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c977b238c521db%3A0x1e3ff5a6ffcc7b91!2sIzoline!5e0!3m2!1sru!2s!4v1687426686852!5m2!1sru!2s" width="100%" height="288" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                        <div class="border-bottom border-color-1 mb-5">
                            <h3 class="section-title mb-0 pb-2 font-size-25">@lang('common.our_address')</h3>
                        </div>
                        <address class="mb-6 text-lh-23">
                            @lang('common.store_address')
                            <div class="mb-3">@lang('common.build_route')
                              @if(\App\Http\Controllers\Controller::isMobile())
                              <a class="mr-1" href="yandexnavi://build_route_on_map?lat_to=46.936449&lon_to=28.953672">Yandex</a>
                              <a class="no-hover-green" href="https://www.google.com/maps/dir/?api=1&destination=46.936449,28.953672">Google Maps</a>
                              @else
                              <a class="mr-1" href="https://yandex.ru/maps/?rtext=~46.936449,28.953672" target="_blank">Yandex</a>
                              <a class="no-hover-green" href="https://www.google.com/maps/dir/?api=1&destination=46.936449,28.953672" target="_blank">Google Maps</a>
                              @endif
                            </div>
                            <div class="">@lang('common.phone_numbers')
                                <a class="text-black mr-1" href="tel:@lang('common.site_phone_contacts_1')">@lang('common.site_phone_contacts_1')</a>
                                <a class="text-black" href="tel:@lang('common.site_phone_contacts_2')">@lang('common.site_phone_contacts_2')</a>
                            </div>
                            <div class="">Email: <a class="text-blue text-decoration-on" href="mailto:@lang('common.site_email')">@lang('common.site_email')</a></div>
                        </address>
                        <h5 class="font-size-14 font-weight-bold mb-3">@lang('common.working_time')</h5>
                        <div>@lang('common.working_time_common_days') 08:00 - 17:00</div>
                        <div>@lang('common.working_time_saturday') 08:00 - 13:00</div>
                        <div class="mb-6">@lang('common.working_time_sunday') @lang('common.weekend')</div>
                        <h5 class="font-size-14 font-weight-bold mb-3">@lang('common.work')</h5>
                        <p class="text-gray-90">@lang('common.work_text') <a class="text-blue text-decoration-on" href="mailto:@lang('common.site_email')">@lang('common.site_email')</a></p>
                    </div>
                </div>
            </div>
@endsection

