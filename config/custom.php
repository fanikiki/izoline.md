<?php

return [
    'parameter_types' => [0 => 'Текст', 1 => 'Список'],

    'arrays_cache_minutes' => 1440, //1440 минут = сутки

    'admin_email' => env('ADMIN_EMAIL'),

    'from_address' => env('MAIL_FROM_ADDRESS'),

    'product_availability' => [
        0 => 'Нет в наличии',
        1 => 'Мало',
        2 => 'Средне',
        3 => 'В наличии'
    ],

    'order_status' => [
        -1 => 'Отменен',
        0 => 'Новый',
        1 => 'Доставляется',
        2 => 'Доставлен'
    ],

    'default_currency' => ['name' => 'MDL', 'label' => 'MDL', 'label_short' => 'lei'],

    'currencies' => [
        ['name' => 'MDL', 'label' => 'MDL', 'label_short' => 'lei'],
    ],

    'product_sort' => [
        'new',
        'expensive',
        'cheap'
    ],

    'product_per_page' => [
        15,
        20,
        30
    ],

    'watermark' => 'public/images/watermark.png',

    'week_map' => [
        0 => 'Воскресенье',
        1 => 'Понедельник',
        2 => 'Вторник',
        3 => 'Среда',
        4 => 'Четверг',
        5 => 'Пятница',
        6 => 'Суббота',
    ],

    'months' => [
        1 => 'январь',
        2 => 'февраль',
        3 => 'март',
        4 => 'апрель',
        5 => 'май',
        6 => 'июнь',
        7 => 'июль',
        8 => 'август',
        9 => 'сентябрь',
        10 => 'октябрь',
        11 => 'ноябрь',
        12 => 'декабрь'
    ]
];
